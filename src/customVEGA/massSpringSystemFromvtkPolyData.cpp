#include "stdafx.h"
#include "massSpringSystemFromvtkPolyData.h"
#include "massSpringSystemMT.h"
int MassSpringSystemFromvtkPolyData::GenerateMassSpringSystem(vtkSmartPointer<vtkPolyData> polyData , MassSpringSystem ** massSpringSystem, double surfaceDensity, double tensileStiffness, double damping, int addGravity)
{
	vtkPoints *points  = polyData->GetPoints();	
	int numParticles = points->GetNumberOfPoints();
	double * restPositions = new double[3*numParticles];
	double * masses = new double[3*numParticles];
	//copy data into restPosition
	for (int i = 0 ; i < numParticles ; i++)
	{
		points->GetPoint(i,&restPositions[3*i+0]);		
		masses[i] = surfaceDensity;
	}

	vtkSmartPointer<vtkExtractEdges> extractEdges = vtkSmartPointer<vtkExtractEdges>::New();
	extractEdges->SetInputData(polyData);
	extractEdges->Update();
	
	vtkCellArray* lines= extractEdges->GetOutput()->GetLines();
	int numMaterialGroups = 1;;
	int numEdges = lines->GetNumberOfCells();
	int *edgeGroups	= new int[numEdges];
	int *edges = new int[numEdges*2];

	double * groupStiffness = new double[numMaterialGroups];
	groupStiffness[0] = tensileStiffness;

	double * groupDamping  = new double[numMaterialGroups];
	groupDamping[0] = damping;

	for(vtkIdType i = 0; i < numEdges; i++)
    {    
	    vtkSmartPointer<vtkLine> line = vtkLine::SafeDownCast(extractEdges->GetOutput()->GetCell(i));
		edges[2*i+0] = (int)line->GetPointId(0);
		edges[2*i+1] = (int)line->GetPointId(1);
		edgeGroups[i] = 0; // tensile group	
    }
 
	/*

	vtkCellArray*cellarray = polyData->GetPolys();
	vtkIdType  maxCellSize = cellarray->GetMaxCellSize();
	int numFaces = cellarray->GetNumberOfCells();	
	int *faceCardinalities = new int[numFaces];  //how many vertices in each face.  (3 ,4 ... )	
	int *facesIndice = new int [maxCellSize*numFaces];
	int *iter_faceCardinalities = faceCardinalities;
	int *iter_facesIndice = facesIndice;
	cellarray->InitTraversal();

	
	
	vtkIdType numPts;
	vtkIdType *indices;
	
	while (cellarray->GetNextCell(numPts,indices))
	{	
		*iter_faceCardinalities++ = numPts;
		for (vtkIdType j = 0 ; j < numPts; j++)
			*iter_facesIndice++ = (int)indices[j];
	}
	*/
	
	*massSpringSystem = new MassSpringSystemMT(	numParticles, 
												masses, 
												restPositions,
												numEdges, edges, edgeGroups, 
												numMaterialGroups, 
												groupStiffness, 
												groupDamping, addGravity,4);

	
	


	delete [] edgeGroups;
	delete [] edges;

	delete [] groupStiffness;
	delete [] groupDamping;
	delete [] restPositions;
	delete [] masses;
	return 0;	
}