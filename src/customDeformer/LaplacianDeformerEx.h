#pragma once

#include "LaplacianDeformer.h"
namespace rms {
class LaplacianDeformerEx : public LaplacianDeformer
{
public:
	//LaplacianDeformerEx(void);
	//virtual ~LaplacianDeformerEx(void);
	virtual bool DeleteConstraint(IMesh::VertexID vID);
	virtual void CalMatrix();
	virtual const Wml::Vector3f * GetConstraintPos(IMesh::VertexID vID);
};


}

