#include "stdafx.h"
#include "LaplacianDeformerEx.h"

using namespace rms;
bool LaplacianDeformerEx::DeleteConstraint(IMesh::VertexID vID)
{
	bool bFound = false;
	size_t nCount = m_vConstraints.size();

	for ( unsigned int k = 0; !bFound && k < nCount; ++k ) {
		if ( m_vConstraints[k].vID == vID ) 
		{
			m_vConstraints.erase(m_vConstraints.begin() + k);
			bFound = true;
			m_bMatricesValid = false;
			break;
		}
	}
	return bFound;
}

void LaplacianDeformerEx::CalMatrix()
{
	this->UpdateMatrices();
}

const Wml::Vector3f * LaplacianDeformerEx::GetConstraintPos(IMesh::VertexID vID)
{
	bool bFound = false;
	size_t nCount = m_vConstraints.size();
	for ( unsigned int k = 0; !bFound && k < nCount; ++k ) 
	{
		if ( m_vConstraints[k].vID == vID ) 
		{
			bFound = true;
			return &(m_vConstraints[k].vPosition);
		}
	}
	return NULL;
}