

#ifndef _MYPROBLEMSOLVING_H_
#define _MYPROBLEMSOLVING_H_
#ifdef WIN32
#include <Windows.h>
#endif
#include <eo>
#include <es.h>
#include <ga.h>


class MyProblemSolving 
{
public:
	MyProblemSolving();
	virtual ~MyProblemSolving();

	void init();
	void start();
	void pause();

	
};

#endif

