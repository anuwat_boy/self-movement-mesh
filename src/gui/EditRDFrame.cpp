#include "stdafx.h"
#include "EditRDFrame.h"


//Ready RD system
#include <FormulaOpenCLImageRD.hpp>
#include <FormulaOpenCLMeshRD.hpp>
#include "vtk_pipeline.hpp"
#include "scene_items.hpp"




enum
{
ID_Hello = 1,
ID_OPEN_XML_FILE = 2,
};
wxBEGIN_EVENT_TABLE(EditRDFrame, wxFrame)
	EVT_MENU(ID_Hello, EditRDFrame::OnHello)
	EVT_MENU(wxID_EXIT, EditRDFrame::OnExit)
	EVT_MENU(wxID_ABOUT, EditRDFrame::OnAbout)
wxEND_EVENT_TABLE()




EditRDFrame::EditRDFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
: wxFrame(NULL, wxID_ANY, title, pos, size),
		system(NULL),
		pVTKWindow(NULL),
		render_settings("render_settings")
{
	wxMenu *menuFile = new wxMenu;
	menuFile->Append(ID_Hello, "&Hello...\tCtrl-H",
	"Help string shown in status bar for this menu item");
	menuFile->AppendSeparator();
	menuFile->Append(wxID_EXIT);
	wxMenu *menuHelp = new wxMenu;
	menuHelp->Append(wxID_ABOUT);
	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append( menuFile, "&File" );
	menuBar->Append( menuHelp, "&Help" );
	SetMenuBar( menuBar );
	CreateStatusBar();
	SetStatusText( "Welcome to wxWidgets!" );
}

EditRDFrame::~EditRDFrame(void)
{
	
}

void EditRDFrame::OnExit(wxCommandEvent& event)
{
	Close( true );
}
void EditRDFrame::OnAbout(wxCommandEvent& event)
{
	wxMessageBox( "This is a wxWidgets' Hello world sample",
	"About Hello World", wxOK | wxICON_INFORMATION );
}
void EditRDFrame::OnHello(wxCommandEvent& event)
{
	wxLogMessage("Hello world from wxWidgets!");
}


void EditRDFrame::SetCurrentRDSystem(AbstractRD* sys)
{
	
	this->system = (ImageRD*)sys;
	int width = 512;
	int height = 512;
	this->SetClientSize(width ,height);
	InitializeRenderPane();	
}


void EditRDFrame::InitVTKPipeline(const Properties &render_settings,bool reset_camera)
{

	vtkSmartPointer<vtkRenderer> pRenderer;
    if(pVTKWindow->GetRenderWindow()->GetRenderers()->GetNumberOfItems()>0)
    {
        pRenderer = pVTKWindow->GetRenderWindow()->GetRenderers()->GetFirstRenderer();
        pRenderer->RemoveAllViewProps();
    }
    else
    {
        pRenderer = vtkSmartPointer<vtkRenderer>::New();
        pVTKWindow->GetRenderWindow()->AddRenderer(pRenderer); // connect it to the window
        
        // set the background color
        pRenderer->GradientBackgroundOn();
        pRenderer->SetBackground(0,0.4,0.6);
        pRenderer->SetBackground2(0,0.2,0.3);
        
        // change the interactor style to a trackball
        //vtkSmartPointer<vtkInteractorStyleTrackballCamera> is = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
        //pVTKWindow->SetInteractorStyle(is);

		vtkSmartPointer<vtkParallelCoordinatesInteractorStyle> ps = vtkSmartPointer<vtkParallelCoordinatesInteractorStyle>::New();		

		//pVTKWindow->SetInteractorStyle(NULL);
		pVTKWindow->SetInteractorStyle(ps);
    }

	
	InitializeVTKPipeline_2D(pRenderer,render_settings);
	

	if(reset_camera)
    {
        vtkSmartPointer<vtkCamera> camera = vtkSmartPointer<vtkCamera>::New();        
		camera->SetPosition(0,0,10); // move slightly down, to give perspective on the displacement plots
		//camera->SetParallelProjection(1);
        pRenderer->SetActiveCamera(camera);
		
        pRenderer->ResetCamera();

    }
}

void EditRDFrame::InitializeRenderPane()
{

	// for now the VTK window goes in the center pane (always visible) - we got problems when had in a floating pane
    vtkObject::GlobalWarningDisplayOff(); // (can turn on for debugging)
    this->pVTKWindow = new wxVTKRenderWindowInteractor(this,wxID_ANY);
}





void EditRDFrame::InitializeVTKPipeline_2D(vtkRenderer* pRenderer,const Properties& render_settings)
{
    float low = render_settings.GetProperty("low").GetFloat();
    float high = render_settings.GetProperty("high").GetFloat();
    float vertical_scale_2D = render_settings.GetProperty("vertical_scale_2D").GetFloat();
    float r,g,b,low_hue,low_sat,low_val,high_hue,high_sat,high_val;
    render_settings.GetProperty("color_low").GetColor(r,g,b);
    vtkMath::RGBToHSV(r,g,b,&low_hue,&low_sat,&low_val);
    render_settings.GetProperty("color_high").GetColor(r,g,b);
    vtkMath::RGBToHSV(r,g,b,&high_hue,&high_sat,&high_val);
    bool use_image_interpolation = render_settings.GetProperty("use_image_interpolation").GetBool();
    int iActiveChemical = IndexFromChemicalName(render_settings.GetProperty("active_chemical").GetChemical());
    float contour_level = render_settings.GetProperty("contour_level").GetFloat();
    bool use_wireframe = render_settings.GetProperty("use_wireframe").GetBool();
    float surface_r,surface_g,surface_b;
    render_settings.GetProperty("surface_color").GetColor(surface_r,surface_g,surface_b);
    bool show_multiple_chemicals = render_settings.GetProperty("show_multiple_chemicals").GetBool();
    bool show_displacement_mapped_surface = render_settings.GetProperty("show_displacement_mapped_surface").GetBool();
    bool show_color_scale = render_settings.GetProperty("show_color_scale").GetBool();
    bool color_displacement_mapped_surface = render_settings.GetProperty("color_displacement_mapped_surface").GetBool();
    bool show_cell_edges = render_settings.GetProperty("show_cell_edges").GetBool();
    bool show_bounding_box = render_settings.GetProperty("show_bounding_box").GetBool();
    bool show_phase_plot = render_settings.GetProperty("show_phase_plot").GetBool();
    int iPhasePlotX = IndexFromChemicalName(render_settings.GetProperty("phase_plot_x_axis").GetChemical());
    int iPhasePlotY = IndexFromChemicalName(render_settings.GetProperty("phase_plot_y_axis").GetChemical());
    int iPhasePlotZ = IndexFromChemicalName(render_settings.GetProperty("phase_plot_z_axis").GetChemical());
    
    float scaling = vertical_scale_2D / (high-low); // vertical_scale gives the height of the graph in worldspace units

    vtkFloatingPointType offset[3] = {0,0,0};

    //int iFirstChem=0,iLastChem=system->GetNumberOfChemicals();
	int iFirstChem=0,iLastChem=iFirstChem+1;
    if(!show_multiple_chemicals) { iFirstChem = iActiveChemical; iLastChem = iFirstChem+1; }
    
    // create a lookup table for mapping values to colors
    vtkSmartPointer<vtkLookupTable> lut = vtkSmartPointer<vtkLookupTable>::New();
    lut->SetRampToLinear();
    lut->SetScaleToLinear();
    lut->SetTableRange(low,high);
    lut->SetSaturationRange(low_sat,high_sat);
    lut->SetHueRange(low_hue,high_hue);
    lut->SetValueRange(low_val,high_val);
    lut->Build();

    for(int iChem=iFirstChem;iChem<iLastChem;iChem++)
    {
        // pass the image through the lookup table
        vtkSmartPointer<vtkImageMapToColors> image_mapper = vtkSmartPointer<vtkImageMapToColors>::New();
        image_mapper->SetLookupTable(lut);
        #if VTK_MAJOR_VERSION >= 6
		image_mapper->SetInputData(system->GetImage(iChem));
        #else
            image_mapper->SetInput(this->GetImage(iChem));
        #endif

        // will convert the x*y 2D image to a x*y grid of quads
        vtkSmartPointer<vtkPlaneSource> plane = vtkSmartPointer<vtkPlaneSource>::New();
        plane->SetXResolution(system->GetX());
        plane->SetYResolution(system->GetY());
        plane->SetOrigin(0,0,0);
        plane->SetPoint1(system->GetX(),0,0);
        plane->SetPoint2(0,system->GetY(),0);

        vtkSmartPointer<vtkTexture> texture = vtkSmartPointer<vtkTexture>::New();
        texture->SetInputConnection(image_mapper->GetOutputPort());
        if(use_image_interpolation)
            texture->InterpolateOn();
        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputConnection(plane->GetOutputPort());

        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper(mapper);
        actor->SetPosition(offset);
        actor->SetTexture(texture);
        if(show_cell_edges)
            actor->GetProperty()->EdgeVisibilityOn();
        actor->GetProperty()->SetEdgeColor(0,0,0);
        actor->GetProperty()->LightingOff();
        pRenderer->AddActor(actor);

        //if(show_displacement_mapped_surface)
		if (0)
        {
            vtkSmartPointer<vtkImageDataGeometryFilter> plane = vtkSmartPointer<vtkImageDataGeometryFilter>::New();
            #if VTK_MAJOR_VERSION >= 6
				plane->SetInputData(system->GetImage(iChem));
            #else
                plane->SetInput(this->GetImage(iChem));
            #endif
            vtkSmartPointer<vtkWarpScalar> warp = vtkSmartPointer<vtkWarpScalar>::New();
            warp->SetInputConnection(plane->GetOutputPort());
            warp->SetScaleFactor(scaling);
            vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
            normals->SetInputConnection(warp->GetOutputPort());
            normals->SplittingOff();
            vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
            if(color_displacement_mapped_surface)
            {
                vtkSmartPointer<vtkTextureMapToPlane> tmap = vtkSmartPointer<vtkTextureMapToPlane>::New();
                tmap->SetOrigin(0,0,0);
                tmap->SetPoint1(system->GetX(),0,0);
                tmap->SetPoint2(0,system->GetY(),0);
                tmap->SetInputConnection(normals->GetOutputPort());
                mapper->SetInputConnection(tmap->GetOutputPort());
            }
            else
                mapper->SetInputConnection(normals->GetOutputPort());
            mapper->ScalarVisibilityOff();
            mapper->ImmediateModeRenderingOn();
            vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
            actor->SetMapper(mapper);
            actor->GetProperty()->SetColor(surface_r,surface_g,surface_b);
            actor->GetProperty()->SetAmbient(0.1);
            actor->GetProperty()->SetDiffuse(0.7);
            actor->GetProperty()->SetSpecular(0.2);
            actor->GetProperty()->SetSpecularPower(3);
            if(use_wireframe)
                actor->GetProperty()->SetRepresentationToWireframe();
            actor->SetPosition(offset[0]+0.5,offset[1]+0.5+system->GetY()+system->ygap,offset[2]);
            actor->PickableOff();
            pRenderer->AddActor(actor);

            // add the color image as the texture of the displacement-mapped surface
            if(color_displacement_mapped_surface)
            {
                vtkSmartPointer<vtkTexture> texture = vtkSmartPointer<vtkTexture>::New();
                texture->SetInputConnection(image_mapper->GetOutputPort());
                if(use_image_interpolation)
                    texture->InterpolateOn();
                else
                    texture->InterpolateOn();
                actor->SetTexture(texture);
            }

            // add the bounding box
            if(show_bounding_box)
            {
                vtkSmartPointer<vtkCubeSource> box = vtkSmartPointer<vtkCubeSource>::New();
                box->SetBounds(0,system->GetX()-1,0,system->GetY()-1,low*scaling,high*scaling);

                vtkSmartPointer<vtkExtractEdges> edges = vtkSmartPointer<vtkExtractEdges>::New();
                edges->SetInputConnection(box->GetOutputPort());

                vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                mapper->SetInputConnection(edges->GetOutputPort());
                mapper->ImmediateModeRenderingOn();

                vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
                actor->SetMapper(mapper);
                actor->GetProperty()->SetColor(0,0,0);  
                actor->GetProperty()->SetAmbient(1);
                actor->SetPosition(offset[0]+0.5,offset[1]+0.5+system->GetY()+system->ygap,offset[2]);
                actor->PickableOff();

                pRenderer->AddActor(actor);
            }
        }

        // add a text label
        if(system->GetNumberOfChemicals()>1)
        {
            vtkSmartPointer<vtkTextActor3D> label = vtkSmartPointer<vtkTextActor3D>::New();
            label->SetInput(GetChemicalName(iChem).c_str());
            const float text_label_offset = 20.0f;
            label->SetPosition(offset[0]+system->GetX()/2,offset[1]-text_label_offset,offset[2]);
            label->PickableOff();
            pRenderer->AddActor(label);
        }

        offset[0] += system->GetX()+system->xgap; // the next chemical should appear further to the right
    }

    //if(show_displacement_mapped_surface)
	if (0)
    {
        // add an axis
        vtkSmartPointer<vtkCubeAxesActor2D> axis = vtkSmartPointer<vtkCubeAxesActor2D>::New();
        axis->SetCamera(pRenderer->GetActiveCamera());
        axis->SetBounds(0.5,0.5,system->GetY()-0.5+system->GetY()+system->ygap,system->GetY()-0.5+system->GetY()+system->ygap,low*scaling,high*scaling);
        axis->SetRanges(0,0,0,0,low,high);
        axis->UseRangesOn();
        axis->XAxisVisibilityOff();
        axis->YAxisVisibilityOff();
        axis->SetZLabel("");
        axis->SetLabelFormat("%.2f");
        axis->SetInertia(10000);
        axis->SetCornerOffset(0);
        axis->SetNumberOfLabels(5);
        axis->PickableOff();
        pRenderer->AddActor(axis);
    }

    // add a scalar bar to show how the colors correspond to values
    if(show_color_scale)
    {
        AddScalarBar(pRenderer,lut);
    }

    // add a phase plot
    if(show_phase_plot && system->GetNumberOfChemicals()>=2)
    {
        float posY = system->ygap*2 + system->GetY();
        if(show_displacement_mapped_surface)
            posY += system->ygap + system->GetY();

        system->AddPhasePlot(pRenderer,system->GetX()/(high-low),low,high,0.0f,posY,0.0f,iPhasePlotX,iPhasePlotY,iPhasePlotZ);
    }
}