#pragma once
///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  5 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __SIMULATIONDIALOG_H__
#define __SIMULATIONDIALOG_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/spinctrl.h>
#include <wx/grid.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/aui/aui.h>
#ifdef WIN32
#include <Windows.h>
#endif
#include "GlobalVar.h"
///////////////////////////////////////////////////////////////////////////
class SimulationThread;

///////////////////////////////////////////////////////////////////////////////
/// Class SimulationDialog
///////////////////////////////////////////////////////////////////////////////
class SimulationDialog : public wxDialog 
{
	private:
		
	protected:
		wxGridCellNumberEditor *editor;
		
		int GetCellData(std::vector<double> &data);
		int GetCellData(std::vector<std::vector<double>> &data);
	protected:
		wxStaticText* m_staticText1;
		wxStaticText* m_staticText2;
		wxStaticText* m_staticText3;
		wxSpinCtrl* m_spinCtrlChromosomeSize;
		wxSpinCtrl* m_spinCtrlMeshFrame;
		wxSpinCtrl* m_spinCtrlEvalTime;
		wxGrid* m_gridChromosome;
		wxButton* m_buttonRun;
		wxButton* m_buttonClose;
		wxRadioBox* m_radioBoxInputType;
		wxTextCtrl* m_textCtrlChromosome;
		wxSpinCtrl* m_spinCtrlPropagateIntervalTime;
		wxStaticText* m_staticTextPropagate;
		wxRadioBox* m_radioBoxBehaviorMode;
		

		// Virtual event handlers, overide them in your derived class
		virtual void m_spinCtrlChromosomeSizeOnSpinCtrl( wxSpinEvent& event );
		virtual void m_gridChromosomeOnKeyDown( wxKeyEvent& event );
		virtual void m_buttonRunOnButtonClick( wxCommandEvent& event );
		virtual void m_buttonCloseOnButtonClick( wxCommandEvent& event );
		virtual void m_radioBoxInputTypeOnRadioBox( wxCommandEvent& event );
		virtual void m_radioBoxBehaviorModeOnRadioBox( wxCommandEvent& event );
		virtual void OnKeyDown( wxKeyEvent &e); 
	public:
		
		SimulationDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 672,500 ), long style = wxDEFAULT_DIALOG_STYLE ); wxAuiManager m_mgr;
		
		virtual ~SimulationDialog();
		static int GetCellData(wxString str ,  std::vector<std::vector<double>> &data);
	
};

class SimulationThread : public wxThread
{	
public:
	SimulationThread(std::vector<MyIndivi>& _indiv ,int num_meshframe)
    {
		m_num_meshframe = num_meshframe;
		m_indiv = _indiv;
    }
 
    virtual ExitCode Entry();	
	std::vector<MyIndivi> m_indiv;
	int m_num_meshframe;
};

#endif //__SIMULATIONDIALOG_H__
