#pragma once

#ifdef WIN32
#include <Windows.h>
#endif
#include <eo>
#include <es.h>
#include <ga.h>




//#include "implicitNewmarkSparse.h"
//#include "tetMesh.h"
//#include "corotationalLinearFEMMT.h"
//#include "graph.h"
#include "PhysicsEngine.h"

#include "LaplacianDeformerEx.h"
#include "MeshObject.h"



typedef eoBit<double> eoIndi;     // A bitstring with fitness double<double> eoIndi;
/*
struct control_vertex
{
	unsigned int vid;
	vtkIdType    display_vid;
	double		 pos[3];
};
*/




class ImageRD;
class Properties;
class SelfMovementMesh 
{
public:
	SelfMovementMesh();
	virtual ~SelfMovementMesh();
	
	void InitialMassSpringDeformSystem(std::vector<int> &fixVertices);
	void InitialLaplacianDeformSystem(std::vector<int> &fixVertices);
	void InitialGAsystem();
	void InitialPhysicsEngine();
	void ProcessObjectiveFunction();
	void ProcessObjectiveFunction2();
	void UpdateMesh();
	void UpdateMesh2();
	void AddExternalForce(int vertexID, double *force_vector);
	void SetExternalForce(int vertexID, double *force_vector);
	void ResetExternalForces();
	void SaveOriginalMesh(vtkSmartPointer<vtkPolyData> poly);
	//vtkWeakPointer<vtkPolyDataAlgorithm>	pmesh ;
	//vtkWeakPointer<vtkDoubleArray>			ptexCoord ;
	ImageRD*								pimageRD;
	ImageRD*								pimageRDs[100];	
	Properties *							p_render_settings;

	//spring system
	/*
	MassSpringSystem *						p_massSpringSystem;
	SparseMatrix*							p_massMatrix;
	MassSpringSystemForceModel *			p_massSpringForceSys;
	ImplicitNewmarkSparse *					p_implicitNewmarkSparse;
	Graph*									p_meshGraph;
	SparseMatrix*							p_LaplacianDampingMatrix;
	*/

	
	
	double									CurrentTotalParticlesEnergy();

	//std::vector<control_vertex> &			ControlPoints();
	vtkSmartPointer<vtkPolyData>	original_mesh;


protected :

	
	int    m_substepsPerTimeStep;
	double* m_external_force;
	size_t m_external_force_bytesize;
	std::vector<int> m_fixVertices;

	//EO system (Genetic Algorithm)
	eoPop<eoIndi> 	 m_population;		
		
	
	
private:
	void InitialFEMDeformSystem(std::vector<int> &fixVertices); //not support
	

}  ;