#include "stdafx.h"
#include "SelfMovementMesh.h"
#include <FormulaOpenCLImageRD.hpp>
#include <FormulaOpenCLMeshRD.hpp>
#include "Properties.hpp"
#include <algorithm>    // std::sort
SelfMovementMesh::SelfMovementMesh(void):
	pimageRD(NULL),p_render_settings(NULL)
{
	ZeroMemory(pimageRDs,sizeof(ImageRD*)*100);	
	m_external_force = NULL;
	m_substepsPerTimeStep = 10;
	original_mesh = vtkSmartPointer<vtkPolyData>::New();
	//p_PhysicsEngine = NULL;
}


SelfMovementMesh::~SelfMovementMesh(void)
{
	/*
	if (pmesh)
		pmesh->Delete();
	if (ptexCoord)
		ptexCoord->Delete();
		*/
	

	if (m_external_force)
	{
		delete [] m_external_force;
		m_external_force = NULL;
	}
	/*
	if (p_PhysicsEngine)
	{
		p_PhysicsEngine->exitPhysics();
		delete p_PhysicsEngine;
		p_PhysicsEngine = NULL;
	}*/
}


void SelfMovementMesh::InitialPhysicsEngine()
{
	//p_PhysicsEngine = new PhysicsEngine();
	//p_PhysicsEngine->initPhysics();
}

void SelfMovementMesh::InitialGAsystem()
{
	if (!pimageRD)
		return; 

	unsigned int POP_SIZE  = 2;
	unsigned int VEC_SIZE  = (unsigned int)(pimageRD->GetX()* pimageRD->GetY());
	/*
	eoUniformGenerator<double> uGen(-1.0, 1.0);
    eoInitFixedLength<eoIndi> random(VEC_SIZE, uGen);
	m_population  = eoPop<eoIndi>(2, random);
	*/

	// fill it!
	for (unsigned int igeno=0; igeno<POP_SIZE; igeno++)
	{
		eoIndi v;                  // void individual, to be filled
		for (unsigned ivar=0; ivar<VEC_SIZE; ivar++)
		{
			double r = 0;
			v.push_back(r);            // append that random value to v
		}
        
        m_population.push_back(v);                // and put it in the population
     }
	
}

void SelfMovementMesh::InitialLaplacianDeformSystem(std::vector<int> &fixVertices)
{
	/*
	vtkSmartPointer<vtkPolyData> polydata =  pmesh->GetOutput();
	
	original_mesh->DeepCopy(polydata);
	double normVector[3] = {0}; //same as normal
	double posVector[3] = {0};
	vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(polydata->GetPointData()->GetNormals());
			
	rms::VFTriangleMesh _mesh;
	

	for (int vid = 0 ; vid < polydata->GetNumberOfPoints(); vid++)
	{
		pointNormals->GetTuple(vid, normVector);
		polydata->GetPoint(vid,posVector);
		Wm4::Vector3f N(normVector[0],normVector[1],normVector[2]);
		Wm4::Vector3f V(posVector[0],posVector[1],posVector[2]);
		rms::IMesh::VertexID vID = _mesh.AppendVertex(V,&N);
	}

	
	polydata->GetPolys()->InitTraversal();
	for (int fid = 0 ; fid < polydata->GetNumberOfPolys(); fid++)
	{
		vtkIdType npts;
		vtkIdType *pts;
		polydata->GetPolys()->GetNextCell(npts,pts);
		
		rms::IMesh::TriangleID tID = _mesh.AppendTriangle(pts[0],pts[1],pts[2]);
	}
	m_vDeformedMesh.Clear(false);
	m_vDeformedMesh.Copy(_mesh,m_VMap);
	m_deformer.SetMesh(&m_vDeformedMesh);
	//m_deformer.AddBoundaryConstraints(10.0f);
	*/

}

void SelfMovementMesh::InitialMassSpringDeformSystem(std::vector<int> &fixVertices)
{
	/*
	MassSpringSystemFromvtkPolyData::GenerateMassSpringSystem(this->pmesh->GetOutput(), &this->p_massSpringSystem, 1.0, 10000.0,   0.01);
	int n = this->p_massSpringSystem->GetNumParticles();
	this->p_massSpringSystem->GenerateMassMatrix(&this->p_massMatrix);
	this->p_massSpringForceSys = new MassSpringSystemForceModel(this->p_massSpringSystem);

	// create the mesh graph (used only for the distribution of user forces over neighboring vertices)
    this->p_meshGraph = new Graph(this->p_massSpringSystem->GetNumParticles(), this->p_massSpringSystem->GetNumEdges(), this->p_massSpringSystem->GetEdges());
	int scaleRows = 1;
	
	
	this->p_meshGraph->GetLaplacian(&this->p_LaplacianDampingMatrix, scaleRows);
	double dampingLaplacianCoef = 0;
	this->p_LaplacianDampingMatrix->ScalarMultiply(dampingLaplacianCoef);
	
	//let border vertex be fixedDOF;	
	int numBorderVertices = (int)fixVertices.size();
	int * fixedDOFs = new int[numBorderVertices * 3];
	int *iter_fixedDOFs = fixedDOFs;
	for (std::vector<int>::iterator it = fixVertices.begin() ; it != fixVertices.end(); it++)
	{
		*iter_fixedDOFs++ = (int)(3*(*it)+0);
		*iter_fixedDOFs++ = (int)(3*(*it)+1);
		*iter_fixedDOFs++ = (int)(3*(*it)+2);
	}
	
	SAFE_DELETE_ARRAY(m_external_force);
	m_external_force_bytesize = sizeof(double)*3*n;
	m_external_force = new double[n*3];
	memset(m_external_force,0,m_external_force_bytesize);
	
	
    
    
	this->p_implicitNewmarkSparse = new ImplicitNewmarkSparse(3*n,
															0.01,this->p_massMatrix,this->p_massSpringForceSys,0,
															numBorderVertices * 3,fixedDOFs);	
	
	
	this->p_implicitNewmarkSparse->UseStaticSolver(0);
	this->p_implicitNewmarkSparse->SetDampingMatrix(this->p_LaplacianDampingMatrix);	
	
	this->p_implicitNewmarkSparse->ResetToRest();		
	this->p_implicitNewmarkSparse->SetState(m_external_force, NULL);
	this->p_implicitNewmarkSparse->SetTimestep(0.01);
	

	//delete LaplacianDampingMatrix;
	m_fixVertices = fixVertices;
	std::sort(m_fixVertices.begin(), m_fixVertices.end());
	delete [] fixedDOFs;


	*/
	
	

}





void SelfMovementMesh::InitialFEMDeformSystem(std::vector<int> &fixVertices)
{
	/*
	MassSpringSystem *massSpringSystem = NULL; //temp  , just borrow to create mass matrix
	
	MassSpringSystemFromvtkPolyData::GenerateMassSpringSystem(this->pmesh->GetOutput(), &massSpringSystem, 1.0, 10000.0,   0.01);
	int n = massSpringSystem->GetNumParticles();
	massSpringSystem->GenerateMassMatrix(&this->p_massMatrix);
	
	this->p_meshGraph = new Graph(massSpringSystem->GetNumParticles(), massSpringSystem->GetNumEdges(), massSpringSystem->GetEdges());
	
	int scaleRows = 1;
	
	
	this->p_meshGraph->GetLaplacian(&this->p_LaplacianDampingMatrix, scaleRows);
	double dampingLaplacianCoef = 0;
	this->p_LaplacianDampingMatrix->ScalarMultiply(dampingLaplacianCoef);


	//let border vertex be fixedDOF;	
	int numBorderVertices = (int)fixVertices.size();
	int * fixedDOFs = new int[numBorderVertices * 3];
	int *iter_fixedDOFs = fixedDOFs;
	for (std::vector<int>::iterator it = fixVertices.begin() ; it != fixVertices.end(); it++)
	{
		*iter_fixedDOFs++ = (int)(3*(*it)+0);
		*iter_fixedDOFs++ = (int)(3*(*it)+1);
		*iter_fixedDOFs++ = (int)(3*(*it)+2);
	}
	
	
	//double *uInitial = new double[3*n];
 //   
 //   for(int i=0; i<n; i++)
 //   {
 //     uInitial[3*i+0] = 0.0;
 //     uInitial[3*i+1] = 0.0;
 //     uInitial[3*i+2] = 0.0;
 //   }
	
	

	SAFE_DELETE(massSpringSystem);
	*/
	
}

void SelfMovementMesh::UpdateMesh()
{
	/*
	p_implicitNewmarkSparse->SetExternalForces(m_external_force);
	for(int i=0; i<m_substepsPerTimeStep; i++)
    {
		int code = p_implicitNewmarkSparse->DoTimestep(); 
		if (code != 0)
		{
			printf("The integrator went unstable. Reduce the timestep, or increase the number of substeps per timestep.\n");
			p_implicitNewmarkSparse->ResetToRest();	
			memset(m_external_force,0,m_external_force_bytesize);   
			p_implicitNewmarkSparse->SetExternalForcesToZero();
			break;
		}      
    }


	int numParticles = p_massSpringSystem->GetNumParticles();
	double *u = new double [3 * numParticles];	
	memcpy(u, p_implicitNewmarkSparse->Getq(), sizeof(double) * 3 * numParticles);	
	
	int  resX  = (int )pimageRD->GetX();
	int  resY  = (int )pimageRD->GetY();


	double *iter_u = u;
	vtkSmartPointer<vtkPolyData> polydata =  pmesh->GetOutput();
	vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(polydata->GetPointData()->GetNormals());
	
	double *restPosition = p_massSpringSystem->GetRestPositions();
	for (int i = 0; i < numParticles ; i++)
	{		
		
		if (std::binary_search (m_fixVertices.begin(), m_fixVertices.end(), i))
			continue;
		polydata->GetPoints()->SetPoint(i,	restPosition[i*3+0] + u[i*3+0],
											restPosition[i*3+1] + u[i*3+1],
											restPosition[i*3+2] + u[i*3+2]);
	}
	vtkSmartPointer<vtkPolyDataNormals> normal = vtkSmartPointer<vtkPolyDataNormals>::New();
	normal->SetInputData(polydata);
	normal->SplittingOff();
	normal->Update();
	polydata->ShallowCopy(normal->GetOutput());
	
	
	polydata->GetPoints()->Modified();
#if 0
	pimageRD->LockMutex();
	int iActChem = p_render_settings->GetActiveChem();
	//vtkImageData* imagedata = smm.pimageRD->GetImage(iActChem);
	//float* data = static_cast<float*>(imagedata->GetScalarPointer());

	vtkSmartPointer<vtkPolyData> polydata =  pmesh->GetOutput();
	vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(polydata->GetPointData()->GetNormals());
	
	double *restPosition = p_massSpringSystem->GetRestPositions();
	for (int i = 0; i < numParticles ; i++)
	{		
		
		if (std::binary_search (m_fixVertices.begin(), m_fixVertices.end(), i))
			continue;
		polydata->GetPoints()->SetPoint(i,	restPosition[i*3+0] + u[i*3+0],
											restPosition[i*3+1] + u[i*3+1],
											restPosition[i*3+2] + u[i*3+2]);
		
		float norm = vtkMath::Norm(&u[i*3]);

		



		

		const double *meshTexcoordS = ptexCoord->GetTuple2(i);
		const double *meshTexcoordT = meshTexcoordS + 1;
		//int  x = (int)((resX -1.0)*(*meshTexcoordS) + 0.5);
		//int  y = (int)((resY -1.0)*(*meshTexcoordT) + 0.5);
		float x = (resX -1.0)*(*meshTexcoordS);
		float y = (resY -1.0)*(*meshTexcoordT);
		float z = 0.0f;
		if (norm > 0.0f)
		{
			double N[3] = {0}; 
			pointNormals->GetTuple(i , N);
			float dir = vtkMath::Dot(N,&u[i*3]);
			if (dir<0.0f)
				norm *=  -1.0f;
			//pimageRD->AddValuesInRadius(x,y,z,0.1,norm,iActChem);
			pimageRD->SetValuesInRadius(x,y,z,0.1,norm,iActChem);
		}
		//data[y*resX + x] += norm;
		
	}
	CurrentTotalParticlesEnergy();
	vtkSmartPointer<vtkPolyDataNormals> normal = vtkSmartPointer<vtkPolyDataNormals>::New();
	normal->SetInputData(polydata);
	normal->SplittingOff();
	normal->Update();
	polydata->ShallowCopy(normal->GetOutput());
	
	
	polydata->GetPoints()->Modified();	

	//imagedata->Modified();
	pimageRD->SetModified(true);
	pimageRD->UnLockMutex();
#endif
	memset(m_external_force,0,m_external_force_bytesize);   
	delete [] u;
	*/

}


void SelfMovementMesh::UpdateMesh2()
{
	/*
	if (m_controlVertices.size() > 0)
	{
		//if (m_deformer.ClearConstraints
		
		m_deformer.Solve();
		vtkSmartPointer<vtkPolyData> polydata =  pmesh->GetOutput();
	
		rms::VFTriangleMesh::vertex_iterator curv(m_vDeformedMesh.BeginVertices()), endv(m_vDeformedMesh.EndVertices());
		while ( curv != endv ) {
			rms::IMesh::VertexID vID = *curv++;
			rms::IMesh::VertexID vOldID = m_VMap.GetOld(vID);
			Wml::Vector3f vVertex;
			m_vDeformedMesh.GetVertex(vID, vVertex);
			polydata->GetPoints()->SetPoint(vOldID,vVertex[0],vVertex[1],vVertex[2]);
			
			physicsCO.vertices[vOldID * 3 + 0] = vVertex[0];
			physicsCO.vertices[vOldID * 3 + 1] = vVertex[1];
			physicsCO.vertices[vOldID * 3 + 2] = vVertex[2];
		
		}
		polydata->Modified();
	}
	*/
}

void SelfMovementMesh::ProcessObjectiveFunction()
{
	
	//if (!pimageRD || !p_implicitNewmarkSparse)
	//	return;


	////create value same dimesion
	//int iActChem = p_render_settings->GetActiveChem();
	//int iNumChem = pimageRD->GetNumberOfChemicals();
	// 
	//int  resX  = (int )pimageRD->GetX();
	//int  resY  = (int )pimageRD->GetY();
	//int numParticles = p_massSpringSystem->GetNumParticles();
	//vtkSmartPointer<vtkPolyData> polydata =  pmesh->GetOutput();
	//
	////find maximum position
	//float maxValue = 0; 
	//int maxParticle = -1;

	////to do :  parallel
	//for (int i = 0; i < numParticles ; i++)
	//{	
	//	const double *meshTexcoordS = ptexCoord->GetTuple2(i);
	//	const double *meshTexcoordT = meshTexcoordS + 1;		
	//	float x = (resX -1.0)*(*meshTexcoordS);
	//	float y = (resY -1.0)*(*meshTexcoordT);
	//	float z = 0.0f;
	//	
	//	
	//	float value = pimageRD->GetValue(x,y,z, (iActChem)%iNumChem);
	//	/*
	//	if (fabs(maxValue) < fabs(value))
	//	{
	//		maxValue = value;
	//		maxParticle = i;
	//	}
	//	*/
	//	if (value  > 0.0f)
	//	{
	//		double forceVector[3] = {0}; //same as normal
	//		vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(polydata->GetPointData()->GetNormals());
	//		pointNormals->GetTuple(i, forceVector);
	//		vtkMath::MultiplyScalar(forceVector, 0.1*(value)); 	
	//		this->AddExternalForce(i,forceVector);
	//	}
	//}

	///*
	//if (maxParticle >=0 && maxValue > 0)
	//{	
	//	//apply maximum value particle 
	//	double forceVector[3] = {0}; //same as normal
	//	vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(polydata->GetPointData()->GetNormals());
	//	pointNormals->GetTuple(maxParticle, forceVector);
	//	vtkMath::MultiplyScalar(forceVector, -(maxValue)); 
	//
	//	this->AddExternalForce(maxParticle,forceVector);

	//}
	//*/

}


void SelfMovementMesh::ProcessObjectiveFunction2()
{
	/*
	if (!pimageRD || !pmesh)
		return;


	//create value same dimesion
	int iActChem = p_render_settings->GetActiveChem();
	int iNumChem = pimageRD->GetNumberOfChemicals();
	 
	int  resX  = (int )pimageRD->GetX();
	int  resY  = (int )pimageRD->GetY();
	
	
	
	for (int i = 0 ; i < m_controlVertices.size(); i++)
	{
		control_vertex &cv = m_controlVertices[i];
		vtkIdType cvid = cv.vid;
		const double *meshTexcoordS = ptexCoord->GetTuple2(cvid);
		const double *meshTexcoordT = meshTexcoordS + 1;		
		float x = (resX -1.0)*(*meshTexcoordS);
		float y = (resY -1.0)*(*meshTexcoordT);
		float z = 0.0f;
		float value = pimageRD->GetValue(x,y,z, (iActChem)%iNumChem);
		double oriPos[3];
		original_mesh->GetPoint(cvid,oriPos);

		double NormalVector[3] = {0}; //same as normal
		vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(original_mesh->GetPointData()->GetNormals());
		pointNormals->GetTuple(cvid, NormalVector);

		double currentpos[3];
		pmesh->GetOutput()->GetPoint(cvid,currentpos);
		double distance  = vtkMath::Distance2BetweenPoints(oriPos,cv.pos);

		if (value  > 0.0f)
		{			
			if (distance < 0.2*(value))
			{
				vtkMath::MultiplyScalar(NormalVector, 0.05); 	
				vtkMath::MultiplyScalar(NormalVector, -1.0);
				double newPos[3] = {0};		
				vtkMath::Add(cv.pos,NormalVector, newPos);
				m_deformer.UpdateConstraint( cvid, Wm4::Vector3f(newPos[0],newPos[1],newPos[2]),1.0);
				cv.pos[0] = newPos[0];
				cv.pos[1] = newPos[1];
				cv.pos[2] = newPos[2];
			}
		}
		else		
		{					
			
			if (distance > 0.05)
			{
				double newPos[3] = {0};		
				vtkMath::MultiplyScalar(NormalVector, -0.05);
				vtkMath::MultiplyScalar(NormalVector, -1.0);
				vtkMath::Add(cv.pos,NormalVector, newPos);
				m_deformer.UpdateConstraint( cvid, Wm4::Vector3f(newPos[0],newPos[1],newPos[2]),1.0);
				cv.pos[0] = newPos[0];
				cv.pos[1] = newPos[1];
				cv.pos[2] = newPos[2];
			}
			else
			{
				m_deformer.UpdateConstraint( cvid, Wm4::Vector3f(oriPos[0],oriPos[1],oriPos[2]),1.0);
				cv.pos[0] = oriPos[0];
				cv.pos[1] = oriPos[1];
				cv.pos[2] = oriPos[2];
			}			
			
		}


	}

				*/
	
}

void SelfMovementMesh::AddExternalForce(int vertexID, double *force_vector)
{
	__m256d a = _mm256_loadu_pd( &m_external_force[vertexID*3]);
	__m256d b = _mm256_setr_pd(force_vector[0],force_vector[1],force_vector[2],0);
	__m256d c = _mm256_add_pd(a,b);
	_mm256_storeu_pd(&m_external_force[vertexID*3], c);
}
void SelfMovementMesh::SetExternalForce(int vertexID, double *force_vector)
{
	memcpy_s(&m_external_force[vertexID*3], m_external_force_bytesize, force_vector, sizeof(double)*3);
}
void SelfMovementMesh::ResetExternalForces()
{
	/*
	memset(m_external_force,0,m_external_force_bytesize);   
	if (p_implicitNewmarkSparse)
	{
		p_implicitNewmarkSparse->ResetToRest();	
		p_implicitNewmarkSparse->SetState(m_external_force, NULL);	
	}
	*/
	
}

double SelfMovementMesh::CurrentTotalParticlesEnergy()
{
	double totalEnergy = 0;
	/*
	int numParticles = p_massSpringSystem->GetNumParticles();
	double *u = new double [3 * numParticles];	
	memcpy(u, p_implicitNewmarkSparse->Getq(), sizeof(double) * 3 * numParticles);	
	double *restPosition = p_massSpringSystem->GetRestPositions();


	for (int i = 0; i < numParticles ; i++)
	{		
		
		__m256d x = _mm256_loadu_pd( &u[i*3+0]);		
		x.m256d_f64[3] = 0;
		__m256d xx = _mm256_mul_pd( x, x );				
		__m256d dxx = _mm256_hadd_pd(xx,xx);
		totalEnergy += ((double*)&dxx)[0] + ((double*)&dxx)[2];




		
	}
		



	delete [] u;
	*/
	return totalEnergy;
	
}

double eval_function(const eoIndi & _indi)
{
	double sum = 0;
	for (unsigned i = 0; i < _indi.size(); i++)
		sum += _indi[i]*_indi[i];
	return (sum);            // maximizing only
}

/*
std::vector<control_vertex> &		SelfMovementMesh::ControlPoints()
{
	return m_controlVertices; 
}
*/


void SelfMovementMesh::SaveOriginalMesh(vtkSmartPointer<vtkPolyData> poly)
{
	
	original_mesh->DeepCopy(poly);
	
	
}