// Copyright Ryan Schmidt 2011.
// Distributed under the Boost Software License, Version 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)
#include "StdAfx.h"
#include "rmsdebug.h"
#include "meshobject.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include <VFMeshRenderer.h>
#include <string>
#include <Triangulator2D.h>
#include <VFMeshMask.h>
#include <PlanarParameterization.h>
#include <MeshUtils.h>

extern float g_scale;

MeshObject::MeshObject(void)
{
	m_bExpMapValid = false;
	m_bComputeExpMap = false;
	m_fDecalRadius = 0.3f;

	m_expmapgen.SetUseUpwindAveraging(true);
	m_expmapgen.SetUseNeighbourNormalSmoothing(true);

	// create checker texture...
	std::vector<unsigned char> vTexBuf;
	Texture::MakeCheckerImage(512, 64, vTexBuf, true);
	m_texture.InitializeBuffer(512, 512, vTexBuf, GL_RGBA);
	m_texture.SetTextureInfo( GL_TEXTURE_2D, GL_LINEAR, GL_REPEAT );
	m_texture.TextureEnvMode() = GL_DECAL;
	//m_texture.SetEdgeAlpha( 0 );

	m_renderer.EnableGLDisplayList(false);

	m_fTranslate[0] = 	m_fTranslate[1] = 	m_fTranslate[2] = 0.0f;

	m_fColor[0] = 0.8f;	m_fColor[1] = 0.8f;	m_fColor[2]	= 1.0f;
}

MeshObject::~MeshObject(void)
{
}


void MeshObject::SetSurface( rms::GSurface & surface )
{
	m_surface = surface;

	m_renderer.SetMesh( &m_surface.Mesh(), &m_surface.Polygons() );
	m_bvTree.SetMesh( & GetMesh() );
	m_expmapgen.SetSurface(& GetMesh(), &m_bvTree);
	m_bExpMapValid = false;

	if ( ! GetMesh().HasUVSet(0) )
		GetMesh().AppendUVSet();
	GetMesh().InitializeUVSet(0);


	float fMin = 0, fMax = 0, fAverage = 0;
	GetMesh().GetEdgeLengthStats(fMin, fMax, fAverage);
//	m_fBoundaryWidth =  fMax * 1.0f;
	m_fBoundaryWidth =  fAverage * 1.5f;


	// set initial expmap
	Wml::Vector3f v,n;
	GetMesh().GetVertex(0, v, &n);
	m_vSeedFrame.Origin() = v;
	m_vSeedFrame.AlignZAxis( n );
	m_fDecalRadius = 10.0f * fAverage;
	m_bExpMapValid = false;

}

void MeshObject::NotifyMeshModified()
{
	m_bvTree.SetMesh(&GetMesh());
	m_expmapgen.SetSurface(&GetMesh(), &m_bvTree);
	m_bExpMapValid = false;

	m_renderer.InvalidateGLDisplayList();
}



bool MeshObject::FindIntersection( Wml::Ray3f & vRay, Wml::Vector3f & vHit, Wml::Vector3f & vHitNormal )
{
	rms::IMesh::TriangleID tID;
	if ( ! m_bvTree.FindRayIntersection( vRay.Origin, vRay.Direction, vHit, tID ) )
		return false;
	Wml::Vector3f vVerts[3], vNorms[3];
	GetMesh().GetTriangle(tID, vVerts, vNorms);

	float fBary[3];
	rms::BarycentricCoords( vVerts[0], vVerts[1], vVerts[2], vHit, fBary[0], fBary[1], fBary[2] );
	vHitNormal = 
		fBary[0]*vNorms[0] + fBary[1]*vNorms[1] + fBary[2]*vNorms[2];
	vHitNormal.Normalize();

	return true;
}

bool MeshObject::FindHitFrame( Wml::Ray3f & vRay, rms::Frame3f & vFrame )
{
	Wml::Vector3f vHit, vHitNormal;
	if ( ! FindIntersection(vRay, vHit, vHitNormal ) ) 
		return false;
	vFrame.Origin() = vHit;
	vFrame.AlignZAxis(vHitNormal);	
	return true;
}

bool MeshObject::FindNearestFrame( const Wml::Vector3f & vPoint, rms::Frame3f & vNearestFrame )
{
	rms::IMesh::TriangleID tID;
	if ( ! m_bvTree.FindNearest( vPoint, vNearestFrame.Origin(), tID ) )
		return false;
	Wml::Vector3f vVerts[3], vNorms[3];
	GetMesh().GetTriangle(tID, vVerts, vNorms);

	float fBary[3];
	rms::BarycentricCoords( vVerts[0], vVerts[1], vVerts[2], vNearestFrame.Origin(), fBary[0], fBary[1], fBary[2] );
	Wml::Vector3f vNormal =
		fBary[0]*vNorms[0] + fBary[1]*vNorms[1] + fBary[2]*vNorms[2];
	vNormal.Normalize();

	vNearestFrame.AlignZAxis( vNormal );
	return true;
}



void MeshObject::SetComputeExpMap(bool bEnable)
{
	m_bComputeExpMap = bEnable;
	m_bExpMapValid = false;
}

void MeshObject::MoveExpMap( rms::Frame3f & vFrame )
{
	m_vSeedFrame.Origin() = vFrame.Origin();
	m_vSeedFrame.AlignZAxis( vFrame.Z() );
	m_bExpMapValid = false;
}

void MeshObject::ScaleExpMap( float fScale )
{
	if ( fScale <= 0 )
		return;
	m_fDecalRadius *= fScale;
	if ( m_fDecalRadius < 0.01f )
		m_fDecalRadius = 0.01f;
	m_bExpMapValid = false;
}

void MeshObject::SetDecalRadius( float fRadius )
{
	m_fDecalRadius = fRadius;
	m_bExpMapValid = false;
}

void MeshObject::RotateExpMap( float fRotate )
{
	Wml::Matrix3f matRotate;
	matRotate.FromAxisAngle(m_vSeedFrame.Z(), fRotate);
	m_vSeedFrame.Rotate(matRotate);
	m_bExpMapValid = false;
}



bool MeshObject::ValidateExpMap(bool bForce, rms::Polygon2f * pClipPoly)
{
	if ( ! bForce ) {
		if ( ! m_bComputeExpMap )
			return false;
		if ( m_bExpMapValid )
			return false;
	}
	float fParamRadius = m_fDecalRadius + m_fBoundaryWidth;

	rms::Frame3f vNearestFrame(m_vSeedFrame);
	if ( ! FindNearestFrame( m_vSeedFrame.Origin(), vNearestFrame ) )
		DebugBreak();
	if ( (m_vSeedFrame.Origin() - vNearestFrame.Origin()).Length() > 0.001f )
		m_vSeedFrame = vNearestFrame;

	bool bClipPolyEnabled = false;
	if ( pClipPoly ) {
		m_expmapgen.EnableClipPoly(true);
		m_expmapgen.SetClipPoly( *pClipPoly );
		bClipPolyEnabled = true;
	}

	m_expmapgen.SetSurfaceDistances(m_vSeedFrame.Origin(), 0.0f, 
		fParamRadius, &m_vSeedFrame);
	m_expmapgen.CopyVertexUVs(&GetMesh(), 0);

	if ( bClipPolyEnabled ) 
		m_expmapgen.EnableClipPoly(false);

	m_bExpMapValid = true;
	return true;
}



void MeshObject::ParameterizeMesh()
{
	rms::PlanarParameterization param;
	param.SetMesh(&m_surface.Mesh(), NULL);
	param.SetEmbeddingType(rms::PlanarParameterization::DiscreteNaturalConformal);
	//param.SetEmbeddingType(rms::PlanarParameterization::Optimal2DOneRing);
	//param.SetBoundaryMapType(rms::PlanarParameterization::CircleBoundary);
	bool bOK = param.Compute();
	//int nCoordMap[2] = {0,2};
	//rms::MeshUtils::SetMeshXYZtoUV(m_surface.Mesh(), Wml::Vector3f::UNIT_Y, nCoordMap);
}




void MeshObject::Render(bool bWireframe, bool bFlatShading, bool bUseScalarColors,
						rms::VFMeshRenderer::ColorTransferMode eTransferMode, int nScalarSet )
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT | GL_LINE_BIT | GL_POINT_BIT | GL_DEPTH_BUFFER_BIT );

	// render base mesh
	m_renderer.SetNormalMode( (bFlatShading) ? rms::VFMeshRenderer::FaceNormals : rms::VFMeshRenderer::VertexNormals);
	m_renderer.SetEnableScalarColor( bUseScalarColors );
	m_renderer.SetColorTransferMode( eTransferMode );
	m_renderer.SetScalarColorSet( nScalarSet );
	m_renderer.SetDrawNormals(false);
	m_renderer.SetDrawNonManifoldEdges(true);
	m_renderer.SetEdgeMode( (bWireframe) ? rms::VFMeshRenderer::PolygonEdges : rms::VFMeshRenderer::NoEdges );

//	glColor3f(0.6f, 1.0f, 0.6f);
//	glColor3f(1.0f, 1.0f, 1.0f);
	glColor3fv(m_fColor);
	//glLineWidth(3.0f);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(m_fTranslate[0],m_fTranslate[1],m_fTranslate[2]);
	m_renderer.Render();
	glPopMatrix();
	glDepthFunc(GL_LEQUAL);

	//// render support region of decal parameterization
	//renderer.SetTexture2DMode( rms::VFMeshRenderer::VertexTexture2D_Required );
	//glColor4f(0.5f, 0.5f, 1.0f, 1.0f);
	//renderer.Render();

	// do texture rendering
	if ( m_bExpMapValid ) {
		// enable transparency and alpha test
		glDepthFunc(GL_LEQUAL);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		glAlphaFunc( GL_NOTEQUAL, 0.0f );
		glEnable( GL_ALPHA_TEST );

		// enable texture
		m_texture.Enable();
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		// set texture matrix transformation so that decal is in the right spot
		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		//glTranslatef( 0.5f, 0.5f, 0.0f );	
		//float fScale = 1.0f / (m_fDecalRadius * (float)sqrt(2.0f));
		//glScalef( fScale, fScale, 1.0f );
		float fTexScale = 0.5f / m_fDecalRadius;
		glScalef( fTexScale, fTexScale, fTexScale );
		glMatrixMode(GL_MODELVIEW);
		
		m_renderer.SetTexture2DMode( rms::VFMeshRenderer::VertexTexture2D_Required );
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		
		m_renderer.Render();
		m_renderer.SetTexture2DMode( rms::VFMeshRenderer::NoTexture2D );
		
		glMatrixMode(GL_TEXTURE);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);

		m_texture.Disable();
	}


	// draw seed point frame
#if 0
	glDisable(GL_LIGHTING);

	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f,0.0f,0.0f);
	glVertex3fv(m_vSeedFrame.Origin());
	glVertex3fv(m_vSeedFrame.Origin() + 0.025f * m_vSeedFrame.X());
	glColor3f(0.0f,1.0f,0.0f);
	glVertex3fv(m_vSeedFrame.Origin());
	glVertex3fv(m_vSeedFrame.Origin() + 0.025f * m_vSeedFrame.Y());
	glColor3f(0.0f,0.0f,1.0f);
	glVertex3fv(m_vSeedFrame.Origin());
	glVertex3fv(m_vSeedFrame.Origin() + 0.025f * m_vSeedFrame.Z());
	glEnd();
#endif


	glPopAttrib();
}



void MeshObject::RenderParamMesh()
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT | GL_LINE_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glLineWidth(2.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glColor3f(0.0f, 0.0f, 0.0f);

	// set transformation so that decal is in the right spot
	glPushMatrix();
	glLoadIdentity();
	//glTranslatef( 0.5f, 0.5f, 0.0f );	
	//float fScale = 1.0f / (m_fDecalRadius * (float)sqrt(2.0f));
	float fParamRadius = m_fDecalRadius + m_fBoundaryWidth;
	float fScale = 1.5f / (fParamRadius * (float)sqrt(2.0f));
	glScalef( fScale, fScale, 1.0f );

	rms::VFMeshRenderer renderer( & GetMesh());
	renderer.Render_UV();	

	glColor3f(1.0f, 0.0f, 0.0f);
	glLineWidth(3.0f);
	glBegin(GL_LINE_LOOP);
	float fSize = m_fDecalRadius / (float)sqrt(2.0f);
	glVertex2f(-fSize, -fSize);
	glVertex2f(fSize, -fSize);
	glVertex2f(fSize, fSize);
	glVertex2f(-fSize, fSize);
	glEnd();

	glPopMatrix();

	glPopAttrib();
}



void MeshObject::SetTranslate(const float *xyz)
{
	m_fTranslate[0]  = xyz[0];
	m_fTranslate[1]  = xyz[1];
	m_fTranslate[2]  = xyz[2];
}

void MeshObject::SetTranslate(const float x ,const float y ,const float z)
{
	m_fTranslate[0]  = x;
	m_fTranslate[1]  = y;
	m_fTranslate[2]  = z;
}

void MeshObject::SetColor(const float *rgb)
{
	m_fColor[0] = rgb[0];
	m_fColor[1] = rgb[1];
	m_fColor[2] = rgb[2];
}
void MeshObject::SetColor(const float r ,const float g ,const float b)
{
	m_fColor[0] = r;
	m_fColor[1] = g;
	m_fColor[2] = b;
}