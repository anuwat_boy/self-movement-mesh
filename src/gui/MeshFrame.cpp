#include "stdafx.h"
#include "MeshFrame.h"
#include "GlobalVar.h"

//Ready RD system
#include <FormulaOpenCLImageRD.hpp>
#include <FormulaOpenCLMeshRD.hpp>
#include "vtk_pipeline.hpp"
#include "scene_items.hpp"

#include <sstream>
#include "vtkFeatureEdgesEx.h"
#include "sqp_reader.h"
#define COPY3DARRAY(source,dest) {dest[0]=source[0];dest[1]=source[1];dest[2]=source[2];}

#include "frame.hpp"
#include "prefs.hpp"
#define CONTROLPOINT_WEIGHT 2.5

std::vector<bool> MeshFrame::usingMeshFrame;
std::vector<bool> MeshFrame::readyMeshFrame;
wxMutex MeshFrame::s_MeshFrameMutex;
wxMutex MeshFrame::s_UpdateMutex;
enum
{
ID_OPENMESH = 1,
ID_OPENSQP = 2,
ID_RESET0 = 10,
ID_RESET1 = 11,
};
wxBEGIN_EVENT_TABLE(MeshFrame, wxFrame)
	EVT_IDLE(MeshFrame::OnIdle)
	EVT_MENU(ID_OPENMESH, MeshFrame::OnOpenMesh)
	EVT_MENU(ID_OPENSQP, MeshFrame::OnOpenSqp)
	EVT_MENU(ID_RESET0, MeshFrame::OnResetToInit0)
	EVT_MENU(ID_RESET1, MeshFrame::OnResetToInit1)
wxEND_EVENT_TABLE()




MeshFrame::MeshFrame(wxWindow *parent,const wxString& title, const wxPoint& pos, const wxSize& size , unsigned int numObj)
: wxFrame(parent, wxID_ANY, title, pos, size),
		system(NULL),
		pVTKWindow(NULL),		
		render_settings("render_settings"),
		m_allInitialed(false),
		m_numObj(numObj),recordDebug(false)
{
	m_bPickingPoint = false;
	m_diffTimeFromLastFrame = 0;

	
#ifdef BULLET_PHYSICS
	m_original_smm_vertices = NULL;
	ZeroMemory(m_PhysicsEngine,sizeof(PhysicsEngine*)*100);
#endif
	//ZeroMemory(m_PhysXEngine,sizeof(PhysXEngine*)*100);
#ifdef ODE_PHYSICS
	m_original_smm_ode_vertices = NULL;
	ZeroMemory(m_ODEEngine,sizeof(ODEEngine*)*100);
#endif
	ZeroMemory(m_revisionUpdate,sizeof(int)*100);
	


	wxMenu *menuFile = new wxMenu;
	menuFile->Append(ID_OPENMESH, "&Open Mesh file...\tCtrl-H",
	"Open mesh file");
	menuFile->Append(ID_OPENSQP, "&Open Sqp file...",
	"Open sqp file");
	menuFile->AppendSeparator();
	menuFile->Append(wxID_EXIT);

	wxMenu *resetMenu= new wxMenu;
	resetMenu->Append(ID_RESET0, "Reset to Initial without delete CVs",
	"Reset to Initial without delete CVs");
	resetMenu->Append(ID_RESET1, "Reset to Initial with delete CVs",
	"Reset to Initial with delete CVs");

	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append( menuFile, "&File" );
	menuBar->Append( resetMenu, "&Reset" );
	SetMenuBar( menuBar );


	m_timeStep = 1.0f/60.0f;
	m_initialObjectivePos[0] = -2.0f;
	m_initialObjectivePos[1] = 0.0f;
	m_initialObjectivePos[2] = 0.0f;


	InitializeRenderPane();
	
	InitialMassSpringSystem();
	InitialDeformSystem();
	InitialPhysicsSystem();
	InitialCollisionObject();
	menuFile->Enable(ID_OPENSQP, false);
	m_allInitialed = true;
}

MeshFrame::~MeshFrame(void)
{
	//wxMutexLocker lock(m_SolverMutex);
	//wxMutexLocker lock2(this->pVTKWindow->renderMutex);
	ClearMassSpringSystem();


	//tell parent frame that this frame is destroyed....
	MyFrame *parentFrame = static_cast<MyFrame *>(this->GetParent());
	if (parentFrame)
	{
		parentFrame->ChildFrameDestroy(this);
	}
#ifdef BULLET_PHYSICS
	if (m_original_smm_vertices)
	{
		delete [] m_original_smm_vertices;
		m_original_smm_vertices = NULL;
	}
#endif
#ifdef ODE_PHYSICS
	if (m_original_smm_ode_vertices)
	{
		delete [] m_original_smm_ode_vertices;
		m_original_smm_ode_vertices = NULL;
	}
#endif

	//to do: clear all obj
	for (int i = 0 ; i < m_numObj; i++)
	{
#ifdef BULLET_PHYSICS
		delete [] m_smmCO[i].indices;
		delete [] m_smmCO[i].vertices;
		delete [] m_objectiveCO[i].indices;
		delete [] m_objectiveCO[i].vertices;
		m_PhysicsEngine[i]->exitPhysics();
		delete m_PhysicsEngine[i];
		m_PhysicsEngine[i] = NULL;
#endif

#ifdef ODE_PHYSICS
		delete [] m_smmCO[i].odeVertices;
		delete [] m_smmCO[i].odeIndices;
		delete [] m_objectiveCO[i].odeIndices;
		delete [] m_objectiveCO[i].odeVertices;
		
		delete m_ODEEngine[i];
		m_ODEEngine[i] = NULL;
#endif
	}
	
}

void MeshFrame::UpdateMeshSystem(int obj_id ,bool isRunning ,double time_since_last_frame)
{
	wxMutexLocker lock(m_SolverMutex[obj_id]);

	//to do update multi obj
	if (time_since_last_frame <= 0.0)
		m_diffTimeFromLastFrame = 1000.0/60.0;
	else
		m_diffTimeFromLastFrame  = time_since_last_frame*1000.0;



	if (!m_allInitialed)
		return;
	if (m_bPickingPoint)
		return;
	if (!m_smmPolydata[obj_id])
		return;
	if (!m_smmTexCoord)
	{
		m_textActor->SetInput( "No text coord information , please load sqp file" );
		m_textActor->Modified();
		this->Refresh(false);
		return;
	}
	
	if (isRunning)
	{
		
		
		if (m_controlVertices[obj_id].size() > 0)
		{			
			UpdateDeformMesh(obj_id);
		}
		
		#ifdef BULLET_PHYSICS
		m_PhysicsEngine[obj_id]->Tick(time_since_last_frame );
		#endif
		#ifdef ODE_PHYSICS
		#pragma omp critical
		m_ODEEngine[obj_id]->Tick(time_since_last_frame );
		#endif
		//m_PhysXEngine[obj_id]->ResetFilter(static_cast<PxRigidActor*>(m_smmCO[obj_id].pxActor));
		//m_PhysXEngine[obj_id]->ResetFilter(static_cast<PxRigidActor*>(m_objectiveCO[obj_id].pxActor));
		//m_PhysXEngine[obj_id]->GetScene()->forceDynamicTreeRebuild(true,true);
		//m_PhysXEngine[obj_id]->Tick(time_since_last_frame );
		UpdatePhysicsObjects(obj_id);
		m_revisionUpdate[obj_id]++;
	}
	else
	{
		g_pOptimizationFrame->AppendLog(wxString("is_running = false"),true);
	}
	
}

void MeshFrame::Clone(int source_obj_id,int destination_obj_id)
{
	wxMutexLocker lock(m_SolverMutex[source_obj_id]);
	//wxMutexLocker lock2(m_SolverMutex[destination_obj_id]);
	//clone smm mesh
	m_smmPolydata[destination_obj_id]->DeepCopy(m_smmPolydata[source_obj_id]);
		

	//clone ball position
	this->m_objectiveCO[destination_obj_id].actor->SetUserMatrix(m_objectiveCO[source_obj_id].actor->GetUserMatrix());
	this->m_objectiveCO[destination_obj_id].actor->Modified();
#ifdef BULLET_PHYSICS	
	int memsize = m_smmCO[source_obj_id].numVertices * sizeof(btScalar)*3;
	memcpy_s(m_smmCO[destination_obj_id].vertices,memsize, m_smmCO[source_obj_id].vertices,memsize); 

	//clone physic
	btCollisionObject *dest_colObj =  m_PhysicsEngine[destination_obj_id]->GetCollisionObject(m_objectiveCO[destination_obj_id].btID);
	btRigidBody*		dest_body= btRigidBody::upcast(dest_colObj);
	btCollisionObject *src_colObj =  m_PhysicsEngine[source_obj_id]->GetCollisionObject(m_objectiveCO[source_obj_id].btID);
	btRigidBody*		src_body= btRigidBody::upcast(src_colObj);

	

	//clone physic
	btCollisionObject *dest_smmObj =  m_PhysicsEngine[destination_obj_id]->GetCollisionObject(m_smmCO[destination_obj_id].btID);
	btRigidBody*		dest_smmbody= btRigidBody::upcast(dest_colObj);
	btCollisionObject *src_smmObj =  m_PhysicsEngine[source_obj_id]->GetCollisionObject(m_smmCO[source_obj_id].btID);
	btRigidBody*		src_smmbody= btRigidBody::upcast(src_colObj);



	if (dest_smmbody != NULL && src_smmbody != NULL )
	{
		if (dest_smmbody->getMotionState() != NULL &&  src_smmbody->getMotionState() != NULL)
		{
			btDefaultMotionState* dest_myMotionState = (btDefaultMotionState*)dest_smmbody->getMotionState();
			btDefaultMotionState* src_myMotionState = (btDefaultMotionState*)src_smmbody->getMotionState();
			btTransform trans;
			src_myMotionState->getWorldTransform(trans);
			dest_myMotionState->setWorldTransform(trans);
			//dest_myMotionState->m_graphicsWorldTrans = src_myMotionState->m_graphicsWorldTrans;
		}

		dest_smmbody->setCenterOfMassTransform(src_smmbody->getCenterOfMassTransform());						
		dest_smmbody->setInterpolationWorldTransform( src_smmbody->getInterpolationWorldTransform());
		dest_smmbody->setWorldTransform( src_smmbody->getWorldTransform());
		dest_smmbody->forceActivationState( src_smmbody->getActivationState());
		dest_smmbody->setDeactivationTime(src_smmbody->getDeactivationTime());	

		dest_smmbody->setInterpolationAngularVelocity(src_smmbody->getInterpolationAngularVelocity());
		dest_smmbody->setInterpolationLinearVelocity(src_smmbody->getInterpolationLinearVelocity());
		dest_smmbody->getBroadphaseProxy()->m_aabbMax = src_smmbody->getBroadphaseProxy()->m_aabbMax;
		dest_smmbody->getBroadphaseProxy()->m_aabbMin = src_smmbody->getBroadphaseProxy()->m_aabbMin;
		
		dest_smmbody->setLinearVelocity(src_smmbody->getLinearVelocity());
		dest_smmbody->setAngularVelocity(src_smmbody->getAngularVelocity());
		
		dest_smmbody->updateInertiaTensor();

		btDefaultSerializer *src_serializer = new btDefaultSerializer();
		src_serializer->startSerialization();
		src_smmbody->serializeSingleObject(src_serializer);
		src_serializer->finishSerialization();


		btDefaultSerializer *dest_serializer = new btDefaultSerializer();
		dest_serializer->startSerialization();
		dest_smmbody->serializeSingleObject(dest_serializer);
		dest_serializer->finishSerialization();
			
		if (dest_serializer->getCurrentBufferSize() == src_serializer->getCurrentBufferSize()){
			
			int resultcmp = memcmp(dest_serializer->getBufferPointer(),src_serializer->getBufferPointer(), dest_serializer->getCurrentBufferSize());
			if (resultcmp != 0)
				throw;
		}
		else
			throw;

	}



	int sd = sizeof(*dest_body);
	int ss = sizeof(*src_body);

	if (dest_body != NULL && src_body != NULL )
	{
		if (dest_body->getMotionState())
		{
			btDefaultMotionState* dest_myMotionState = (btDefaultMotionState*)dest_body->getMotionState();
			btDefaultMotionState* src_myMotionState = (btDefaultMotionState*)src_body->getMotionState();
			dest_myMotionState->m_graphicsWorldTrans = src_myMotionState->m_graphicsWorldTrans;
			
			dest_body->setCenterOfMassTransform(src_body->getCenterOfMassTransform());						
			dest_body->setInterpolationWorldTransform( src_body->getInterpolationWorldTransform());
			dest_body->setWorldTransform( src_body->getWorldTransform());
			dest_body->forceActivationState( src_body->getActivationState());
			dest_body->setDeactivationTime(src_body->getDeactivationTime());	

			dest_body->setInterpolationAngularVelocity(src_body->getInterpolationAngularVelocity());
			dest_body->setInterpolationLinearVelocity(src_body->getInterpolationLinearVelocity());
			dest_body->getBroadphaseProxy()->m_aabbMax = src_body->getBroadphaseProxy()->m_aabbMax;
			dest_body->getBroadphaseProxy()->m_aabbMin = src_body->getBroadphaseProxy()->m_aabbMin;
			dest_body->setLinearVelocity(src_body->getLinearVelocity());
			dest_body->setAngularVelocity(src_body->getAngularVelocity());
			
			dest_body->updateInertiaTensor();


			btDefaultSerializer *src_serializer = new btDefaultSerializer();
			src_serializer->startSerialization();
			src_body->serializeSingleObject(src_serializer);
			src_serializer->finishSerialization();


			btDefaultSerializer *dest_serializer = new btDefaultSerializer();
			dest_serializer->startSerialization();
			dest_body->serializeSingleObject(dest_serializer);
			dest_serializer->finishSerialization();
			
			if (dest_serializer->getCurrentBufferSize() == src_serializer->getCurrentBufferSize()){
			
				int resultcmp = memcmp(dest_serializer->getBufferPointer(),src_serializer->getBufferPointer(), dest_serializer->getCurrentBufferSize());
				if (resultcmp != 0)
					throw;
			}
			else
				throw;
			
		}
		//removed cached contact points (this is not necessary if all objects have been removed from the dynamics world)
		{
			
			//btDynamicsWorld *dynamic_world = m_PhysicsEngine[destination_obj_id]->getDynamicsWorld(); 
			//if (dynamic_world->getBroadphase()->getOverlappingPairCache())
			//	dynamic_world->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(dest_colObj->getBroadphaseHandle(),dynamic_world->getDispatcher());

			
		}
		/* //comment out (2015-08-12)  why I copy dest to source ???
		if (src_body && !src_body->isStaticObject())
		{
			src_body->setLinearVelocity(dest_body->getLinearVelocity());
			src_body->setAngularVelocity(dest_body->getAngularVelocity());		
			
		}
		*/
		
	}
	
	btDynamicsWorld *dest_dynamic_world = m_PhysicsEngine[destination_obj_id]->getDynamicsWorld(); 
	btDynamicsWorld *src_dynamic_world = m_PhysicsEngine[destination_obj_id]->getDynamicsWorld(); 
	


	if (dest_dynamic_world->getBroadphase())//->getOverlappingPairCache())
	{
	
		dest_dynamic_world->performDiscreteCollisionDetection();
		

		//dynamic_world->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(dest_body->getBroadphaseProxy(),dynamic_world->getDispatcher());
		//dynamic_world->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(dest_smmbody->getBroadphaseProxy(),dynamic_world->getDispatcher());
	}
	

	btDefaultSerializer *dw_serializer = new btDefaultSerializer();
	//dw_serializer->startSerialization();
	dest_dynamic_world->serialize(dw_serializer);
	//dw_serializer->finishSerialization();

	btDefaultSerializer *sw_serializer = new btDefaultSerializer();
	//sw_serializer->startSerialization();
	src_dynamic_world->serialize(sw_serializer);
	//sw_serializer->finishSerialization();
	
	if (dw_serializer->getCurrentBufferSize() == sw_serializer->getCurrentBufferSize()){
			
		int resultcmp = memcmp(dw_serializer->getBufferPointer(),sw_serializer->getBufferPointer(), dw_serializer->getCurrentBufferSize());
		if (resultcmp != 0)
			g_pOptimizationFrame->AppendLog("a");
	}
	else
		g_pOptimizationFrame->AppendLog("b");;
		

#endif
#ifdef ODE_PHYSICS
	{
		// dContact contact[64];   // up to MAX_CONTACTS contacts per box-box
		//  for (int i=0; i<64; i++) {
		//	contact[i].surface.mode = dContactBounce;
		//	contact[i].surface.mu = dInfinity;
	
		//	//contact[i].surface.mu2 = 0;
		//	contact[i].surface.bounce = 0.0; //0.1
		//	contact[i].surface.bounce_vel = 0.0; //0.1
		//	contact[i].surface.soft_cfm = 0.0;
		//  }
		//if (int numc = dCollide (m_smmCO[source_obj_id].odeID,m_objectiveCO[source_obj_id].odeID,64,&contact[0].geom,
		//			   sizeof(dContact))) 
		//  {
		//	  g_pOptimizationFrame->AppendLog(wxString("clone at collide"),true);
		//  }
		if (source_obj_id != destination_obj_id)
		{

			//Ball part
			dGeomID srcGeom = m_objectiveCO[source_obj_id].odeID;
			dGeomID dstGeom = m_objectiveCO[destination_obj_id].odeID;
			dBodyID srcBody = dGeomGetBody(srcGeom);
			dBodyID dstBody = dGeomGetBody(dstGeom);
#if 0		
			{
				dBodySetRotation(dstBody, dBodyGetRotation(srcBody));
				//const dReal *quat = dBodyGetQuaternion(srcBody);
				//g_pOptimizationFrame->AppendLog(wxString::Format("clone at rot %f %f %f %f", quat[0],quat[1],quat[2],quat[3] ));
			}
			{
				const dReal *src_pos = dBodyGetPosition(srcBody);
				dBodySetPosition(dstBody, src_pos[0],src_pos[1], src_pos[2]);
				//g_pOptimizationFrame->AppendLog(wxString::Format("clone at pos %f %f %f" , src_pos[0],src_pos[1],src_pos[2] ));
			}
			{
				const dReal *src_vel = dBodyGetLinearVel(srcBody);
				dBodySetLinearVel(dstBody, src_vel[0], src_vel[1],src_vel[2]);
				//g_pOptimizationFrame->AppendLog(wxString::Format("clone at linear vel %f %f %f", src_vel[0],src_vel[1],src_vel[2] ));
			}
			{
				const dReal *src_vel = dBodyGetAngularVel(srcBody);
				dBodySetAngularVel(dstBody, src_vel[0], src_vel[1],src_vel[2]);
				//g_pOptimizationFrame->AppendLog(wxString::Format("clone at angular vel %f %f %f", src_vel[0],src_vel[1],src_vel[2] ));
			}
			{
				const dReal *src_force = dBodyGetForce(srcBody);
				dBodySetForce(dstBody, src_force[0], src_force[1],src_force[2]);
				//g_pOptimizationFrame->AppendLog(wxString::Format("clone at force %f %f %f", src_force[0],src_force[1],src_force[2] ));
			}
			{
				const dReal *src_torque = dBodyGetTorque(srcBody);
				dBodySetTorque(dstBody, src_torque[0], src_torque[1],src_torque[2]);
				//g_pOptimizationFrame->AppendLog(wxString::Format("clone at torgue %f %f %f", src_torque[0],src_torque[1],src_torque[2] ));
			}
			{
				dGeomSetRotation(dstGeom, dGeomGetRotation(srcGeom));
			
				const dReal *src_pos = dGeomGetPosition(srcGeom);
				dGeomSetPosition(dstGeom, src_pos[0],src_pos[1],src_pos[2]);
			}		
			{
				dMass srcMass;
				dBodyGetMass(srcBody, &srcMass);
				dMass newmass;
				dMassSetZero(&newmass);
				dMassAdd(&newmass,&srcMass);
				dBodySetMass(dstBody, &newmass);
		
			}
#endif
			dBodyClone(srcBody,dstBody);
			
		}
		else
		{
			//Ball part
			dGeomID srcGeom = m_objectiveCO[source_obj_id].odeID;
			dGeomID dstGeom = m_objectiveCO[destination_obj_id].odeID;
			dBodyID srcBody = dGeomGetBody(srcGeom);
			dBodyID dstBody = dGeomGetBody(dstGeom);
		
			{
				dBodySetRotation(dstBody, dBodyGetRotation(srcBody));
				//const dReal *quat = dBodyGetQuaternion(srcBody);
				//g_pOptimizationFrame->AppendLog(wxString::Format("clone at rot %f %f %f %f", quat[0],quat[1],quat[2],quat[3] ));
			}
		}

		{
			//deform tube part
			dGeomID srcGeom = m_smmCO[source_obj_id].odeID;
			dGeomID dstGeom = m_smmCO[destination_obj_id].odeID;

			int memsize = m_smmCO[source_obj_id].odeNumVertices * sizeof(double) * 3;
			memcpy_s(m_smmCO[destination_obj_id].odeVertices,memsize, m_smmCO[source_obj_id].odeVertices,memsize); 	
			dGeomTriMeshDataUpdate(dGeomTriMeshGetTriMeshDataID(dstGeom));
		}


		 
		  

	}
#endif
	m_controlVertices[destination_obj_id] = m_controlVertices[source_obj_id];
	m_vDeformedMesh[destination_obj_id].CopyVertInfo(m_vDeformedMesh[source_obj_id]);
	
	for (int i = 0 ; i < m_controlVertices[destination_obj_id].size(); i++)
	{
		control_vertex &cv = m_controlVertices[destination_obj_id][i];
		const Wml::Vector3f *source_pos = m_deformer[source_obj_id].GetConstraintPos(cv.vid);
		m_deformer[destination_obj_id].UpdateConstraint( cv.vid, *source_pos,CONTROLPOINT_WEIGHT);
	}
	

	//if (cloneControlVertices)
	//{
	////
	//	m_controlVertices.clear();
	//	m_controlVertices = source.m_controlVertices;
	//	//std::copy(source.m_controlVertices.begin(),source.m_controlVertices.end(), m_controlVertices.begin());
	//	vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();
	//	for (int i = 0 ; i < m_controlVertices.size(); i++)
	//	{
	//		control_vertex &cv  = m_controlVertices[i];
	//		cv.display_vid = _sPoints->InsertNextPoint(m_smmPolydata->GetPoint(cv.vid));

	//		//calculate plane position
	//		//double *texcoord = m_smmTexCoord->GetTuple2(cv.vid);			
	//		//smm.pimageRD->AddControlPoints((float )texcoord[0],(float )texcoord[1],0.0f,true);
	//	}
	//	m_selectedPoints->ShallowCopy(_sPoints);	


	//}
	
}
std::string MeshFrame::ReportPhysicsProperties(int obj_id, bool toLog )
{
	wxMutexLocker lock(m_SolverMutex[obj_id]);
	std::stringstream ss;
	ss.precision(10);
	#ifdef ODE_PHYSICS
	{
		{
			//Ball part
			if (toLog)
				g_pOptimizationFrame->AppendLog(wxString::Format("total step %f", m_ODEEngine[obj_id]->GetTotalStep()));
			ss << "total step " <<  m_ODEEngine[obj_id]->GetTotalStep() << endl ;
			dGeomID srcGeom = m_objectiveCO[obj_id].odeID;			
			dBodyID srcBody = dGeomGetBody(srcGeom);
			
			{
				
				const dReal *quat = dBodyGetQuaternion(srcBody);
				if (toLog)
					g_pOptimizationFrame->AppendLog(wxString::Format("rot %f %f %f %f", quat[0],quat[1],quat[2],quat[3] ));
				ss << "rot" << " " << quat[0] << " " <<  quat[1] << " " <<  quat[2] << " "  << quat[3] << endl;
			}
			{
				const dReal *src_pos = dBodyGetPosition(srcBody);				
				if (toLog)
					g_pOptimizationFrame->AppendLog(wxString::Format("pos %f %f %f" , src_pos[0],src_pos[1],src_pos[2] ));
				ss << "pos" << " " << src_pos[0] << " " <<  src_pos[1] << " " <<  src_pos[2] << endl;
			}
			{
				const dReal *src_vel = dBodyGetLinearVel(srcBody);
				if (toLog)
					g_pOptimizationFrame->AppendLog(wxString::Format("linear vel %f %f %f", src_vel[0],src_vel[1],src_vel[2] ));
				ss << "linear vel" << " " << src_vel[0] << " " <<  src_vel[1] << " " <<  src_vel[2] << endl;
			}
			{
				const dReal *src_vel = dBodyGetAngularVel(srcBody);
				if (toLog)
					g_pOptimizationFrame->AppendLog(wxString::Format("angular vel %f %f %f", src_vel[0],src_vel[1],src_vel[2] ));
				ss << "angular vel" << " " << src_vel[0] << " " <<  src_vel[1] << " " <<  src_vel[2] << endl;
			}
			{
				const dReal *src_force = dBodyGetForce(srcBody);
				if (toLog)
					g_pOptimizationFrame->AppendLog(wxString::Format("force %f %f %f", src_force[0],src_force[1],src_force[2] ));
				ss << "force" << " " << src_force[0] << " " <<  src_force[1] << " " <<  src_force[2] << endl;
			}
			{
				const dReal *src_torque = dBodyGetTorque(srcBody);
				if (toLog)
					g_pOptimizationFrame->AppendLog(wxString::Format("torgue %f %f %f", src_torque[0],src_torque[1],src_torque[2] ));
				ss << "torgue" << " " << src_torque[0] << " " <<  src_torque[1] << " " <<  src_torque[2] << endl;
			}
			
		}
		


		 
		  

	}
#endif

	return ss.str();
}
void MeshFrame::OnOpenSqp(wxCommandEvent& event)
{
	wxString sqp_filename = wxFileSelector(_("Open a sqp:"),wxEmptyString,wxEmptyString,wxEmptyString,
        _("Supported sqp format (*.sqp)|*.sqp"),wxFD_OPEN);
    if(sqp_filename.empty()) 
		return; // user cancelled
	
	std::vector<double> texCoord ;
	
	const int numPoints = m_smmPolydata[0]->GetNumberOfPoints();
	if (!sqp_reader(sqp_filename, texCoord, numPoints))
		return;

	if (m_smmTexCoord)
	{
		m_smmTexCoord->Delete();
	}

	m_smmTexCoord = vtkDoubleArray::New();
	m_smmTexCoord->SetNumberOfComponents( 3 );
	m_smmTexCoord->SetNumberOfTuples(numPoints);
	
	for (int i = 0 ; i < numPoints ; i++)
	{		
		m_smmTexCoord->SetTuple3(i, texCoord[i*2+0]  ,texCoord[i*2 + 1],0);
	}

	
	if (m_textActor)
	{
		m_textActor->SetInput(" ");
		m_textActor->Modified();
	}
}



void MeshFrame::OnOpenMesh(wxCommandEvent& event)
{
#if 0
	vtkSmartPointer<vtkPolyDataAlgorithm> mesh ;
	wxString mesh_filename = wxFileSelector(_("Open a mesh:"),wxEmptyString,wxEmptyString,wxEmptyString,
        _("Supported mesh formats (*.obj;*.ply;*.plyex)|*.obj;*.ply;*.plyex"),wxFD_OPEN);
    if(mesh_filename.empty()) 
		return; // user cancelled


	
    if(mesh_filename.EndsWith(_T("obj")))
    {   
        vtkSmartPointer<vtkOBJReader> obj_reader = vtkSmartPointer<vtkOBJReader>::New();
        obj_reader->SetFileName(mesh_filename.mb_str());
        obj_reader->Update();
		mesh = obj_reader;
    }
	else if (mesh_filename.EndsWith(_T("ply")) || mesh_filename.EndsWith(_T("plyex")))
	{            
            
        vtkSmartPointer<vtkPLYReader> ply_reader = vtkSmartPointer<vtkPLYReader>::New();
        ply_reader->SetFileName(mesh_filename.mb_str());
        ply_reader->Update();
		mesh = ply_reader;
	}
    else
    {
        wxMessageBox(_("Unsupported file type")); 
        return; 
    }
	


	vtkSmartPointer<vtkTriangleFilter> triangleFilter =
	vtkSmartPointer<vtkTriangleFilter>::New();
	triangleFilter->SetInputConnection(mesh->GetOutputPort());
	triangleFilter->Update();
	m_smmPolydata[0] = triangleFilter->GetOutput();

	m_polydatamapper->RemoveAllInputs();	
	m_polydatamapper->SetInputData(m_smmPolydata);
	m_polydatamapper->Update();

	
	
	
	


	//find border points
	vtkSmartPointer<vtkFeatureEdgesEx> borderEdges = vtkSmartPointer<vtkFeatureEdgesEx>::New();
	borderEdges->SetInputConnection(triangleFilter->GetOutputPort());
	borderEdges->FeatureEdgesOff();
	borderEdges->BoundaryEdgesOn();
	borderEdges->ManifoldEdgesOff();
	borderEdges->NonManifoldEdgesOff();
	borderEdges->Update();	
	
	
	vtkSmartPointer<vtkCellArray> lines= borderEdges->GetOutput()->GetLines();	
	vtkSmartPointer<vtkPoints> points = borderEdges->GetOutput()->GetPoints();
	{
		int numBorderEdges = lines->GetNumberOfCells();
	
	
		std::set<int> _tmpBPoints;	
		for(vtkIdType i = 0; i < numBorderEdges; i++)
		{			
			vtkLine* line = vtkLine::SafeDownCast(borderEdges->GetOutput()->GetCell(i));		
			int numID = line->GetPointIds()->GetNumberOfIds();
			int id0 = line->GetPointIds()->GetId(0);
			int id1 = line->GetPointIds()->GetId(1);
			int oid0 = borderEdges->GetOldIdFromCurrentID(id0);
			int oid1 = borderEdges->GetOldIdFromCurrentID(id1);
			_tmpBPoints.insert(oid0);
			_tmpBPoints.insert(oid1);		
		}
		m_borderVertices.clear();
		std::copy(_tmpBPoints.begin(), _tmpBPoints.end(), std::back_inserter(m_borderVertices));
		vtkSmartPointer<vtkPoints> _borderPoints = vtkSmartPointer<vtkPoints>::New();
		for (std::vector<int>::iterator iter = m_borderVertices.begin(); iter != m_borderVertices.end(); iter++)
			_borderPoints->InsertNextPoint(m_smmPolydata->GetPoint(*iter));
		m_borderPoints->ShallowCopy(_borderPoints);
	
	}	


	// Create a mapper and actor of b-edge
  vtkSmartPointer<vtkPolyDataMapper> edge_mapper =   vtkSmartPointer<vtkPolyDataMapper>::New();
  edge_mapper->SetInputConnection(borderEdges->GetOutputPort());
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(edge_mapper);
  actor->GetProperty()->SetColor(1,1,0);
  m_pRenderer->AddActor(actor);

	//m_borderPoints->Modified();
	//polydata->Modified();
    if (m_smmTexCoord)
	{
		m_smmTexCoord->Delete();
	}

	m_pRenderer->ResetCamera();
	ClearMassSpringSystem();
	InitialMassSpringSystem();
	

	this->GetMenuBar()->Enable(ID_OPENSQP, true);	
	this->Refresh(false);
#endif
}


void MeshFrame::OnResetToInit0(wxCommandEvent& event)
{	
	ResetToInitialState(0,false);
	this->Refresh(false);
}

void MeshFrame::OnResetToInit1(wxCommandEvent& event)
{	
	ResetToInitialState(0,true);
	this->Refresh(false);
}
void MeshFrame::SetCurrentRDSystem(AbstractRD* sys)
{
	
	
}


void MeshFrame::InitVTKPipeline()
{

	
    if(pVTKWindow->GetRenderWindow()->GetRenderers()->GetNumberOfItems()>0)
    {
        vtkSmartPointer<vtkRenderer> _pRenderer = pVTKWindow->GetRenderWindow()->GetRenderers()->GetFirstRenderer();
        _pRenderer->RemoveAllViewProps();
    }
    else
    {
		/*
        vtkSmartPointer<vtkRenderer> _pRenderer = vtkSmartPointer<vtkRenderer>::New();
        pVTKWindow->GetRenderWindow()->AddRenderer(_pRenderer); // connect it to the window
        
        // set the background color
        _pRenderer->GradientBackgroundOn();
        _pRenderer->SetBackground(0,0.4,0.6);
        _pRenderer->SetBackground2(0,0.2,0.3);
		m_mouseInteractor = vtkSmartPointer<MouseInteractorStylePP>::New();		
		//vtkSmartPointer<vtkInteractorStyleTrackballCamera> ts = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();		
		
		pVTKWindow->SetInteractorStyle(m_mouseInteractor);



		vtkSmartPointer<vtkRenderer> _pAxeRenderer = vtkSmartPointer<vtkRenderer>::New(); 
		_pAxeRenderer->SetViewport(0.0,0.0,0.1,0.1);
		vtkSmartPointer<vtkAxesActor> axes =vtkSmartPointer<vtkAxesActor>::New();			
		_pAxeRenderer->AddActor ( axes );
		 vtkSmartPointer<vtkTransform> transform =  vtkSmartPointer<vtkTransform>::New();
		 transform->Scale(0.5, 0.5, 0.5);
		 transform->Translate(-0.5, -0.5, 0);
		 axes->SetUserTransform(transform);
		_pAxeRenderer->SetActiveCamera(_pRenderer->GetActiveCamera()); 
		pVTKWindow->GetRenderWindow()->AddRenderer(_pAxeRenderer); // connect it to the window	
		_pAxeRenderer->ResetCamera();
		*/
    }

	//depth peeling
	pVTKWindow->GetRenderWindow()->SetAlphaBitPlanes(1);
	pVTKWindow->GetRenderWindow()->SetMultiSamples(0);
	//InitializeVTKPipelineSphere(m_pRenderer);
	InitializeVTKPipelineCylinder(pVTKWindow->GetRenderWindow());

	

	m_pointPicker =  vtkSmartPointer<vtkPointPicker>::New();
	double tol = m_pointPicker->GetTolerance();
	m_pointPicker->SetTolerance(tol*0.25*1.0);
	m_pointPicker->AddObserver(vtkCommand::EndPickEvent,this,&MeshFrame::pickCallbackFunc2);
	/*
	m_pickCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	m_pickCallback->SetCallback ( this->pickCallbackFunc );
	*/
	
	//pointPicker->AddObserver(vtkCommand::EndPickEvent,m_pickCallback);
	//pVTKWindow->AddObserver(vtkCommand::EndPickEvent,this,&MeshFrame::pickCallbackFunc);
	this->pVTKWindow->SetPicker(m_pointPicker);
	
	
	
	

	

}

void MeshFrame::mouseMoveAfterPickPointCallbackFunc(vtkObject* caller, unsigned long eid, void *calldata)
{
#if 0
	if (m_bPickingPoint)
	{
		/*
		double diffmouseX = (double )(pVTKWindow->GetEventPosition()[0] - pVTKWindow->GetLastEventPosition()[0]);
		double diffmouseY = (double )(pVTKWindow->GetEventPosition()[1] - pVTKWindow->GetLastEventPosition()[1]);
		if (diffmouseX != 0.0 || diffmouseY != 0.0)
		{
			double diffmouseZ = 0;
			vtkSmartPointer<vtkCamera> camera = m_pRenderer->GetActiveCamera();
			vtkMatrix4x4 *mvMatrix = camera->GetModelViewTransformMatrix();
			double *axisX,*axisY,*axisZ;
			axisX = (*mvMatrix)[0];
			axisY = axisX+4;
			axisZ = axisY+4;		
			double camera2WorldScalingFactor = 10.0;
			double extForce[3];
			extForce[0] = (diffmouseX * axisX[0] + diffmouseY * axisY[0] + diffmouseZ * axisZ[0]) * camera2WorldScalingFactor;
			extForce[1] = (diffmouseX * axisX[1] + diffmouseY * axisY[1] + diffmouseZ * axisZ[1]) * camera2WorldScalingFactor;
			extForce[2] = (diffmouseX * axisX[2] + diffmouseY * axisY[2] + diffmouseZ * axisZ[2]) * camera2WorldScalingFactor;
			
			smm.SetExternalForce( m_pickID,extForce);
			
			smm.UpdateMesh2();
			vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();
			_sPoints->InsertNextPoint(smm.pmesh->GetOutput()->GetPoint(m_pickID));
			m_selectedPoints->ShallowCopy(_sPoints);
			m_selectedPoints->Modified();

			this->Refresh(false);
		}
		*/
	}
	else
	{
		
	}

	if (this->pVTKWindow->GetControlKey() == 0)
	{
		m_selectedPoints->Resize(0);
		m_selectedPoints->Modified();
		m_bPickingPoint = false;
		m_mouseInteractor->RemoveObserver(m_c);
	}
#endif
}

void	MeshFrame::UpdateControlPoints()
{
	//to do  multi obj scheme
	//smm.pimageRD->ClearControlPoints();
	for (int i = 0 ; i < m_numObj ;i++)
	{
		vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();
		for (int j = 0 ; j < m_controlVertices[i].size(); j++)
		{
			control_vertex &cv  = m_controlVertices[i][j];
			cv.display_vid = _sPoints->InsertNextPoint(m_smmPolydata[i]->GetPoint(cv.vid));

			//calculate plane position
			//double *texcoord = m_smmTexCoord->GetTuple3(cv.vid);			
			//smm.pimageRD->AddControlPoints((float )texcoord[0],(float )texcoord[1],0.0f,true);
		}
		m_selectedPoints[i]->ShallowCopy(_sPoints);	
	}

}

void MeshFrame::pickCallbackFunc2(vtkObject* caller, unsigned long eid, void *calldata)
{
#if 0
	if ((m_pickID  =  m_pointPicker->GetPointId()) >= 0)
	{		
		
		std::vector<control_vertex>::iterator it = m_controlVertices.begin();
		while (it != m_controlVertices.end())
		{
			if (it->vid == m_pickID)
			{
				break;
			}
			it++;
		}
		if (it != m_controlVertices.end())
		{
			//remove
			
			m_controlVertices.erase(it);
			UpdateControlPoints();
			
		}
		else
		{
			//add 
			control_vertex cv;
			cv.vid = m_pickID;			
			smm.original_mesh->GetPoint(m_pickID,cv.pos);
			cv.display_vid = m_selectedPoints[0]->InsertNextPoint(cv.pos);

			//calculate plane position
			double *texcoord = m_smmTexCoord->GetTuple2(cv.vid);			
			smm.pimageRD->AddControlPoints((float )texcoord[0],(float )texcoord[1],0.0f,true);


			m_controlVertices.push_back(cv);

			
			
		}
		m_selectedPoints[0]->Modified();
		this->Refresh(false);
		//vtkOutputWindowDisplayText(stringStream.str().c_str());
		


		/*
		vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();
		_sPoints->InsertNextPoint(smm.pmesh->GetOutput()->GetPoint(m_pickID));
		m_selectedPoints->ShallowCopy(_sPoints);
		*/
		
		
	}
#endif
}
void MeshFrame::pickCallbackFunc(vtkObject* caller, unsigned long eid, void *calldata)
{
#if 0
	//vtkRenderWindowInteractor *iren = static_cast<vtkRenderWindowInteractor*>(caller);
	if ((m_pickID  =  m_pointPicker->GetPointId()) >= 0)
	{
		std::ostringstream stringStream;
		double picked[3] = {0};
		m_pointPicker->GetPickPosition(picked);
		stringStream << "Picked POINT"<<m_pointPicker->GetPointId() << ",value: " << picked[0] << " " << picked[1] << " " << picked[2] << std::endl;
		
		//vtkOutputWindowDisplayText(stringStream.str().c_str());

		m_bPickingPoint = true;
		vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();
		_sPoints->InsertNextPoint(m_smmPolydata[0]->GetPoint(m_pickID));
		m_selectedPoints[0]->ShallowCopy(_sPoints);
		m_selectedPoints[0]->Modified();
		this->Refresh(false);
		//m_c = m_mouseInteractor->AddObserver(vtkCommand::MouseMoveEvent,this,&MeshFrame::mouseMoveAfterPickPointCallbackFunc);
	}
#endif	
}
void MeshFrame::InitializeRenderPane()
{

	// for now the VTK window goes in the center pane (always visible) - we got problems when had in a floating pane
    vtkObject::GlobalWarningDisplayOff(); // (can turn on for debugging)
    this->pVTKWindow = new wxVTKRenderWindowInteractor(this,wxID_ANY);

	InitVTKPipeline();
}



/*void MeshFrame::InitializeVTKPipelineSphere(vtkRenderer* pRenderer)
{
	wxString mesh_filename = "../testing_data/sphere.ply";
	vtkSmartPointer<vtkPLYReader> ply_reader = vtkSmartPointer<vtkPLYReader>::New();
	ply_reader->SetFileName(mesh_filename.mb_str());
	ply_reader->Update();

	vtkSmartPointer<vtkPolyDataNormals> normal = vtkSmartPointer<vtkPolyDataNormals>::New();
	normal->SetInputConnection(ply_reader->GetOutputPort());
	normal->SplittingOff();
	normal->Update();
	
	m_smmPolydata =  normal->GetOutput();

	wxString sqp_filename = "../testing_data/sphere.sqp";
	
	std::vector<double> texCoord ;	
	const int numPoints = m_smmPolydata->GetNumberOfPoints();
	if (!sqp_reader(sqp_filename, texCoord, numPoints))
		return;

	if (m_smmTexCoord)
	{
		m_smmTexCoord->Delete();
	}

	m_smmTexCoord = vtkDoubleArray::New();
	m_smmTexCoord->SetNumberOfComponents( 2 );
	m_smmTexCoord->SetNumberOfTuples(numPoints);
	
	for (int i = 0 ; i < numPoints ; i++)
	{		
		m_smmTexCoord->SetTuple2(i, texCoord[i*2+0]  ,texCoord[i*2 + 1]);
	}
	
	
	m_polydatamapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    m_polydatamapper->SetInputConnection(normal->GetOutputPort());

    m_actor = vtkSmartPointer<vtkActor>::New();
    m_actor->SetMapper(m_polydatamapper);
	m_actor->GetProperty()->EdgeVisibilityOn();


	// Setup point border and selected colors
	m_borderPoints = vtkSmartPointer<vtkPoints>::New();
	m_selectedPoints = vtkSmartPointer<vtkPoints>::New();

	//find border points
	vtkSmartPointer<vtkFeatureEdgesEx> borderEdges = vtkSmartPointer<vtkFeatureEdgesEx>::New();
	borderEdges->SetInputData(m_smmPolydata);
	borderEdges->FeatureEdgesOff();
	borderEdges->BoundaryEdgesOn();
	borderEdges->ManifoldEdgesOff();
	borderEdges->NonManifoldEdgesOff();
	borderEdges->Update();	
	
	
	vtkSmartPointer<vtkCellArray> lines= borderEdges->GetOutput()->GetLines();	
	vtkSmartPointer<vtkPoints> points = borderEdges->GetOutput()->GetPoints();
	{
		int numBorderEdges = lines->GetNumberOfCells();
	
	
		std::set<int> _tmpBPoints;	
		for(vtkIdType i = 0; i < numBorderEdges; i++)
		{			
			vtkLine* line = vtkLine::SafeDownCast(borderEdges->GetOutput()->GetCell(i));		
			int numID = line->GetPointIds()->GetNumberOfIds();
			int id0 = line->GetPointIds()->GetId(0);
			int id1 = line->GetPointIds()->GetId(1);
			int oid0 = borderEdges->GetOldIdFromCurrentID(id0);
			int oid1 = borderEdges->GetOldIdFromCurrentID(id1);
			_tmpBPoints.insert(oid0);
			_tmpBPoints.insert(oid1);		
		}
		m_borderVertices.clear();
		std::copy(_tmpBPoints.begin(), _tmpBPoints.end(), std::back_inserter(m_borderVertices));
		//m_borderVertices.push_back(1);
		vtkSmartPointer<vtkPoints> _borderPoints = vtkSmartPointer<vtkPoints>::New();
		for (std::vector<int>::iterator iter = m_borderVertices.begin(); iter != m_borderVertices.end(); iter++)
			_borderPoints->InsertNextPoint(m_smmPolydata->GetPoint(*iter));
		m_borderPoints->ShallowCopy(_borderPoints);
	
	}	
	vtkSmartPointer<vtkPolyData> bpointsPolydata =  vtkSmartPointer<vtkPolyData>::New();
	bpointsPolydata->SetPoints(m_borderPoints);

	vtkSmartPointer<vtkVertexGlyphFilter> bvertexGlyphFilter =  vtkSmartPointer<vtkVertexGlyphFilter>::New();
	bvertexGlyphFilter->AddInputData(bpointsPolydata);
	bvertexGlyphFilter->Update();
 
	vtkSmartPointer<vtkPolyDataMapper> bpointsMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	bpointsMapper->SetInputConnection(bvertexGlyphFilter->GetOutputPort());

	vtkSmartPointer<vtkActor> bpointsActor =  vtkSmartPointer<vtkActor>::New();
	bpointsActor->SetMapper(bpointsMapper);
	bpointsActor->GetProperty()->SetPointSize(5);
	bpointsActor->GetProperty()->SetColor(1.0,0.0,0.0);	


	//selected points	
	vtkSmartPointer<vtkPolyData> selpointsPolydata =  vtkSmartPointer<vtkPolyData>::New();
	selpointsPolydata->SetPoints(m_selectedPoints);
	vtkSmartPointer<vtkVertexGlyphFilter> svertexGlyphFilter =  vtkSmartPointer<vtkVertexGlyphFilter>::New();
	svertexGlyphFilter->AddInputData(selpointsPolydata);
	svertexGlyphFilter->Update();
 
	vtkSmartPointer<vtkPolyDataMapper> spointsMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	spointsMapper->SetInputConnection(svertexGlyphFilter->GetOutputPort());

	vtkSmartPointer<vtkActor> spointsActor =  vtkSmartPointer<vtkActor>::New();
	spointsActor->SetMapper(spointsMapper);
	spointsActor->GetProperty()->SetPointSize(5);
	spointsActor->GetProperty()->SetColor(0,1.0,0.0);	


	//label control vertices
	vtkSmartPointer<vtkIdFilter> ids = vtkSmartPointer<vtkIdFilter>::New();
	ids->SetInputData(selpointsPolydata);
	ids->PointIdsOn();
	ids->CellIdsOff();
	ids->FieldDataOn();

	vtkSmartPointer<vtkLabeledDataMapper> ldm = vtkSmartPointer<vtkLabeledDataMapper>::New();
	ldm->SetInputConnection( ids->GetOutputPort() );
	ldm->SetLabelModeToLabelFieldData();
	ldm->GetLabelTextProperty()->SetColor( 255.0/255.0, 201.0/255.0, 41.0/255.0 );
	ldm->GetLabelTextProperty()->SetFontSize(10);
	vtkSmartPointer<vtkActor2D> pointLabels =  vtkSmartPointer<vtkActor2D>::New();
	pointLabels->SetMapper( ldm );




	//Text info
	m_textActor = vtkSmartPointer<vtkTextActor>::New();
	m_textActor->GetTextProperty()->SetFontSize ( 16 );
	m_textActor->SetPosition2 ( 10, 40 );
	
	//m_textActor->SetInput ( "Hello world" );
	m_textActor->GetTextProperty()->SetColor ( 1.0,0.0,0.0 );
		
	pRenderer->AddActor(m_actor);
	pRenderer->AddActor(bpointsActor);
    pRenderer->AddActor(spointsActor);
	pRenderer->AddActor(pointLabels);
	pRenderer->AddActor2D ( m_textActor );


}*/


void MeshFrame::InitializeVTKPipelineCylinder(vtkRenderWindow* pRenderWindow)
{
	//wxString mesh_filename = "../testing_data/cylinder_reverse_face_disk.ply";
	wxString mesh_filename = "../testing_data/cylinderHD_disk.ply";
	vtkSmartPointer<vtkPLYReader> ply_reader = vtkSmartPointer<vtkPLYReader>::New();
	ply_reader->SetFileName(mesh_filename.mb_str());
	ply_reader->Update();

	vtkSmartPointer<vtkPolyDataNormals> normal = vtkSmartPointer<vtkPolyDataNormals>::New();
	normal->SetInputConnection(ply_reader->GetOutputPort());
	normal->SplittingOff();
	normal->Update();
	
	

	//wxString sqp_filename = "../testing_data/cylinder_reverse_face_disk.sqp";
	wxString sqp_filename = "../testing_data/cylinderHD_disk.sqp";
	
	std::vector<double> texCoord ;	
	const int numPoints = normal->GetOutput()->GetNumberOfPoints();
	if (!sqp_reader(sqp_filename, texCoord, numPoints))
		return;

	if (m_smmTexCoord)
	{
		m_smmTexCoord->Delete();
	}

	m_smmTexCoord = vtkDoubleArray::New();
	m_smmTexCoord->SetNumberOfComponents( 3 );
	m_smmTexCoord->SetNumberOfTuples(numPoints);
	
	for (int i = 0 ; i < numPoints ; i++)
	{		
		m_smmTexCoord->SetTuple3(i, texCoord[i*2+0]  ,texCoord[i*2 + 1],0);
	}
	
	int numColumn = (m_numObj/4) + 1;
	double viewport_interval_x = 1.0/numColumn;


	double viewport_interval_y ;
	if (m_numObj < 4)
		viewport_interval_y = 1.0/m_numObj;
	else
		viewport_interval_y = 1.0/4.0;

	for (unsigned int i = 0; i < m_numObj; i++)
	{
		int col = i/4;
		int row = i%4;
		
		m_smmPolydata[i] = vtkSmartPointer<vtkPolyData>::New();
		m_smmPolydata[i]->DeepCopy(normal->GetOutput());
		vtkSmartPointer<vtkPolyDataMapper> _polydatamapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		_polydatamapper->SetInputData(m_smmPolydata[i]);

		m_actor[i] = vtkSmartPointer<vtkActor>::New();
		m_actor[i]->SetMapper(_polydatamapper);
		m_actor[i]->GetProperty()->EdgeVisibilityOff();
		m_actor[i]->GetProperty()->SetOpacity(0.5);

		// Setup point border and selected colors
		m_borderPoints[i] = vtkSmartPointer<vtkPoints>::New();
		m_selectedPoints[i] = vtkSmartPointer<vtkPoints>::New();

		//find border points
		vtkSmartPointer<vtkFeatureEdgesEx> borderEdges = vtkSmartPointer<vtkFeatureEdgesEx>::New();
		borderEdges->SetInputData(m_smmPolydata[i]);
		borderEdges->FeatureEdgesOff();
		borderEdges->BoundaryEdgesOn();
		borderEdges->ManifoldEdgesOff();
		borderEdges->NonManifoldEdgesOff();
		borderEdges->Update();	
	
	
		vtkSmartPointer<vtkCellArray> lines= borderEdges->GetOutput()->GetLines();	
		vtkSmartPointer<vtkPoints> points = borderEdges->GetOutput()->GetPoints();
		{
			int numBorderEdges = lines->GetNumberOfCells();
	
	
			std::set<int> _tmpBPoints;	
			for(vtkIdType j = 0; j < numBorderEdges; j++)
			{			
				vtkLine* line = vtkLine::SafeDownCast(borderEdges->GetOutput()->GetCell(j));		
				int numID = line->GetPointIds()->GetNumberOfIds();
				int id0 = line->GetPointIds()->GetId(0);
				int id1 = line->GetPointIds()->GetId(1);
				int oid0 = borderEdges->GetOldIdFromCurrentID(id0);
				int oid1 = borderEdges->GetOldIdFromCurrentID(id1);
				_tmpBPoints.insert(oid0);
				_tmpBPoints.insert(oid1);		
			}
			m_borderVertices.clear();
			std::copy(_tmpBPoints.begin(), _tmpBPoints.end(), std::back_inserter(m_borderVertices));
			//m_borderVertices.push_back(1);
			vtkSmartPointer<vtkPoints> _borderPoints = vtkSmartPointer<vtkPoints>::New();
			for (std::vector<int>::iterator iter = m_borderVertices.begin(); iter != m_borderVertices.end(); iter++)
				_borderPoints->InsertNextPoint(m_smmPolydata[i]->GetPoint(*iter));
			m_borderPoints[i]->ShallowCopy(_borderPoints);
	
		}	
		vtkSmartPointer<vtkPolyData> bpointsPolydata =  vtkSmartPointer<vtkPolyData>::New();
		bpointsPolydata->SetPoints(m_borderPoints[i]);

		vtkSmartPointer<vtkVertexGlyphFilter> bvertexGlyphFilter =  vtkSmartPointer<vtkVertexGlyphFilter>::New();
		bvertexGlyphFilter->AddInputData(bpointsPolydata);
		bvertexGlyphFilter->Update();
 
		vtkSmartPointer<vtkPolyDataMapper> bpointsMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		bpointsMapper->SetInputConnection(bvertexGlyphFilter->GetOutputPort());

		vtkSmartPointer<vtkActor> bpointsActor =  vtkSmartPointer<vtkActor>::New();
		bpointsActor->SetMapper(bpointsMapper);
		bpointsActor->GetProperty()->SetPointSize(5);
		bpointsActor->GetProperty()->SetColor(0.0,0.0,1.0);	


		//selected points	
		
		vtkSmartPointer<vtkPolyData> selpointsPolydata =  vtkSmartPointer<vtkPolyData>::New();
		selpointsPolydata->SetPoints(m_selectedPoints[i]);
		vtkSmartPointer<vtkVertexGlyphFilter> svertexGlyphFilter =  vtkSmartPointer<vtkVertexGlyphFilter>::New();
		svertexGlyphFilter->AddInputData(selpointsPolydata);
		svertexGlyphFilter->Update();
 
		vtkSmartPointer<vtkPolyDataMapper> spointsMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		spointsMapper->SetInputConnection(svertexGlyphFilter->GetOutputPort());

		vtkSmartPointer<vtkActor> spointsActor =  vtkSmartPointer<vtkActor>::New();
		spointsActor->SetMapper(spointsMapper);
		spointsActor->GetProperty()->SetPointSize(5);
		spointsActor->GetProperty()->SetColor(0,1.0,0.0);	
		

		//label control vertices
		/*
		vtkSmartPointer<vtkIdFilter> ids = vtkSmartPointer<vtkIdFilter>::New();
		ids->SetInputData(selpointsPolydata);
		ids->PointIdsOn();
		ids->CellIdsOff();
		ids->FieldDataOn();

		vtkSmartPointer<vtkLabeledDataMapper> ldm = vtkSmartPointer<vtkLabeledDataMapper>::New();
		ldm->SetInputConnection( ids->GetOutputPort() );
		ldm->SetLabelModeToLabelFieldData();
		ldm->GetLabelTextProperty()->SetColor( 255.0/255.0, 201.0/255.0, 41.0/255.0 );
		ldm->GetLabelTextProperty()->SetFontSize(10);
		vtkSmartPointer<vtkActor2D> pointLabels =  vtkSmartPointer<vtkActor2D>::New();
		pointLabels->SetMapper( ldm );
		*/
		vtkSmartPointer<vtkRenderer> _pRenderer = vtkSmartPointer<vtkRenderer>::New();

		_pRenderer->AddActor(m_actor[i]);
		_pRenderer->AddActor(bpointsActor);
		_pRenderer->AddActor(spointsActor);
		//_pRenderer->AddActor(pointLabels);
		
		_pRenderer->SetUseDepthPeeling(0);
		_pRenderer->SetMaximumNumberOfPeels(100);
		_pRenderer->SetOcclusionRatio(0.0);

		_pRenderer->SetViewport( col * viewport_interval_x, row * viewport_interval_y, 
								 (col+1) * viewport_interval_x, (row+1) * viewport_interval_y);
		pRenderWindow->AddRenderer(_pRenderer);	

		m_pRenderer[i] = _pRenderer;
	}
	//Text info
	/*
	m_textActor = vtkSmartPointer<vtkTextActor>::New();
	m_textActor->GetTextProperty()->SetFontSize ( 16 );
	m_textActor->SetPosition2 ( 140, 40 );
	
	//m_textActor->SetInput ( "Hello world" );
	m_textActor->GetTextProperty()->SetColor ( 1.0,0.0,0.0 );
	_pRenderer->AddActor2D ( m_textActor );
	*/
}
void MeshFrame::InitializeVTKPipeline(vtkRenderer* pRenderer)
{
#if 0
	 int resX  = (int)smm.pimageRD->GetX();
	 int resY  = (int)smm.pimageRD->GetY();
	 resX  = resY= 16;
	vtkSmartPointer<vtkPlaneSource> plane = vtkSmartPointer<vtkPlaneSource>::New();
	
	plane->SetXResolution(resX-1);
	plane->SetYResolution(resY-1);
	plane->SetCenter(0,0,0);
	plane->SetNormal(0.0, 0.0, 1.0);
	plane->Update();	

	vtkSmartPointer<vtkPolyDataNormals> normal = vtkSmartPointer<vtkPolyDataNormals>::New();
	normal->SetInputConnection(plane->GetOutputPort());
	normal->SplittingOff();
	normal->Update();	
	
	vtkSmartPointer<vtkTriangleFilter> triangleFilter =
	vtkSmartPointer<vtkTriangleFilter>::New();
	triangleFilter->SetInputConnection(normal->GetOutputPort());
	triangleFilter->Update();
	
	
	

	/*
	 vtkSmartPointer<vtkPLYReader> ply_reader = vtkSmartPointer<vtkPLYReader>::New();
        ply_reader->SetFileName("head.plyex");
        ply_reader->Update();
	smm.pmesh = ply_reader;
	*/
	m_smmPolydata =  triangleFilter->GetOutput();

	vtkDoubleArray* tc = vtkDoubleArray::New();
	tc->SetNumberOfComponents( 2 );
	tc->SetNumberOfTuples(m_smmPolydata->GetNumberOfPoints());
	
	for (int i = 0 ; i < m_smmPolydata->GetNumberOfPoints() ; i++)
	{
		double xyz[3] = {0};
		m_smmPolydata->GetPoint(i,xyz);	
		tc->SetTuple2(i, xyz[0] + 0.5 , xyz[1] + 0.5);
	}
	m_smmTexCoord = tc;
	
	m_polydatamapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    m_polydatamapper->SetInputConnection(triangleFilter->GetOutputPort());

    m_actor = vtkSmartPointer<vtkActor>::New();
    m_actor->SetMapper(m_polydatamapper);
	m_actor->GetProperty()->EdgeVisibilityOn();


	// Setup point border and selected colors
	m_borderPoints = vtkSmartPointer<vtkPoints>::New();
	m_selectedPoints = vtkSmartPointer<vtkPoints>::New();

	//find border points
	vtkSmartPointer<vtkFeatureEdgesEx> borderEdges = vtkSmartPointer<vtkFeatureEdgesEx>::New();
	borderEdges->SetInputData(m_smmPolydata);
	borderEdges->FeatureEdgesOff();
	borderEdges->BoundaryEdgesOn();
	borderEdges->ManifoldEdgesOff();
	borderEdges->NonManifoldEdgesOff();
	borderEdges->Update();	
	
	
	vtkSmartPointer<vtkCellArray> lines= borderEdges->GetOutput()->GetLines();	
	vtkSmartPointer<vtkPoints> points = borderEdges->GetOutput()->GetPoints();
	{
		int numBorderEdges = lines->GetNumberOfCells();
	
	
		std::set<int> _tmpBPoints;	
		for(vtkIdType i = 0; i < numBorderEdges; i++)
		{			
			vtkLine* line = vtkLine::SafeDownCast(borderEdges->GetOutput()->GetCell(i));		
			int numID = line->GetPointIds()->GetNumberOfIds();
			int id0 = line->GetPointIds()->GetId(0);
			int id1 = line->GetPointIds()->GetId(1);
			int oid0 = borderEdges->GetOldIdFromCurrentID(id0);
			int oid1 = borderEdges->GetOldIdFromCurrentID(id1);
			_tmpBPoints.insert(oid0);
			_tmpBPoints.insert(oid1);		
		}
		m_borderVertices.clear();
		std::copy(_tmpBPoints.begin(), _tmpBPoints.end(), std::back_inserter(m_borderVertices));
		//m_borderVertices.push_back(1);
		vtkSmartPointer<vtkPoints> _borderPoints = vtkSmartPointer<vtkPoints>::New();
		for (std::vector<int>::iterator iter = m_borderVertices.begin(); iter != m_borderVertices.end(); iter++)
			_borderPoints->InsertNextPoint(m_smmPolydata->GetPoint(*iter));
		m_borderPoints->ShallowCopy(_borderPoints);
	
	}	
	vtkSmartPointer<vtkPolyData> bpointsPolydata =  vtkSmartPointer<vtkPolyData>::New();
	bpointsPolydata->SetPoints(m_borderPoints);

	vtkSmartPointer<vtkVertexGlyphFilter> bvertexGlyphFilter =  vtkSmartPointer<vtkVertexGlyphFilter>::New();
	bvertexGlyphFilter->AddInputData(bpointsPolydata);
	bvertexGlyphFilter->Update();
 
	vtkSmartPointer<vtkPolyDataMapper> bpointsMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	bpointsMapper->SetInputConnection(bvertexGlyphFilter->GetOutputPort());

	vtkSmartPointer<vtkActor> bpointsActor =  vtkSmartPointer<vtkActor>::New();
	bpointsActor->SetMapper(bpointsMapper);
	bpointsActor->GetProperty()->SetPointSize(5);
	bpointsActor->GetProperty()->SetColor(1.0,0.0,0.0);	


	//selected points
	vtkSmartPointer<vtkPolyData> selpointsPolydata =  vtkSmartPointer<vtkPolyData>::New();
	selpointsPolydata->SetPoints(m_selectedPoints);
	vtkSmartPointer<vtkVertexGlyphFilter> svertexGlyphFilter =  vtkSmartPointer<vtkVertexGlyphFilter>::New();
	svertexGlyphFilter->AddInputData(selpointsPolydata);
	svertexGlyphFilter->Update();
 
	vtkSmartPointer<vtkPolyDataMapper> spointsMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	spointsMapper->SetInputConnection(svertexGlyphFilter->GetOutputPort());

	vtkSmartPointer<vtkActor> spointsActor =  vtkSmartPointer<vtkActor>::New();
	spointsActor->SetMapper(spointsMapper);
	spointsActor->GetProperty()->SetPointSize(5);
	spointsActor->GetProperty()->SetColor(0,1.0,0.0);	


	//Text info
	m_textActor = vtkSmartPointer<vtkTextActor>::New();
	m_textActor->GetTextProperty()->SetFontSize ( 16 );
	m_textActor->SetPosition2 ( 10, 40 );
	
	//m_textActor->SetInput ( "Hello world" );
	m_textActor->GetTextProperty()->SetColor ( 1.0,0.0,0.0 );



	/*
  vtkSmartPointer<vtkTransform> transform =  vtkSmartPointer<vtkTransform>::New();
  transform->Translate(1.0, 1.0, 0.0);
 
  vtkSmartPointer<vtkAxesActor> axes =
    vtkSmartPointer<vtkAxesActor>::New();
 
	  // The axes are positioned with a user transform
    
	axes->SetUserTransform(transform);
	pRenderer->AddActor ( axes );
	*/
	pRenderer->AddActor(m_actor);
	pRenderer->AddActor(bpointsActor);
    pRenderer->AddActor(spointsActor);
	pRenderer->AddActor2D ( m_textActor );
#endif	
}

void MeshFrame::ClearMassSpringSystem()
{

}

void MeshFrame::InitialMassSpringSystem()
{
	smm.InitialMassSpringDeformSystem(m_borderVertices);



}

void MeshFrame::InitialDeformSystem()
{
	//smm.InitialLaplacianDeformSystem(m_borderVertices);


	vtkSmartPointer<vtkPolyData> polydata =  m_smmPolydata[0];
	smm.SaveOriginalMesh(polydata);
	

	double normVector[3] = {0}; //same as normal
	double posVector[3] = {0};
	vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(polydata->GetPointData()->GetNormals());
			
	rms::VFTriangleMesh _mesh;
	

	for (int vid = 0 ; vid < polydata->GetNumberOfPoints(); vid++)
	{
		pointNormals->GetTuple(vid, normVector);
		polydata->GetPoint(vid,posVector);
		Wm4::Vector3f N(normVector[0],normVector[1],normVector[2]);
		Wm4::Vector3f V(posVector[0],posVector[1],posVector[2]);
		rms::IMesh::VertexID vID = _mesh.AppendVertex(V,&N);
	}

	
	polydata->GetPolys()->InitTraversal();
	for (int fid = 0 ; fid < polydata->GetNumberOfPolys(); fid++)
	{
		vtkIdType npts;
		vtkIdType *pts;
		polydata->GetPolys()->GetNextCell(npts,pts);
		
		rms::IMesh::TriangleID tID = _mesh.AppendTriangle(pts[0],pts[1],pts[2]);
	}
	for (int i = 0 ; i < m_numObj ; i++)
	{
		m_vDeformedMesh[i].Clear(false);
		m_vDeformedMesh[i].Copy(_mesh,m_VMap[i]);
		m_vDeformedMeshOriginal[i] = m_vDeformedMesh[i];
		m_deformer[i].SetMesh(&m_vDeformedMesh[i]);
		m_deformer[i].AddBoundaryConstraints(4.0f);
	}

	std::vector<float> cv;
	int divideS = 10;
	int divideT = 8;
	float intervalS = 1.f/(float)divideS;
	float intervalT = 1.f/(float)divideT;
	for (int t = 1 ; t < divideT; t++)
		for (int s = 1 ; s < divideS; s++)
		{
			float tx = intervalS*s;
			float ty = intervalT*t;
			cv.push_back(tx);	cv.push_back(ty);
		}	

#pragma omp parallel for
	for (int i = 0 ; i < m_numObj ; i++)
	{
		this->SetControlVertices(i,cv);
	}

}
void MeshFrame::InitialPhysicsSystem()
{

	//to do multi physics engines
	m_smmCO.resize(m_numObj);
	int numVertices = -1;
	for (int i = 0 ; i < m_numObj ; i++)
	{
		CollisionObject &co =  m_smmCO[i];
		co.actor = this->m_actor[i];

#ifdef BULLET_PHYSICS
		m_PhysicsEngine[i] = new PhysicsEngine();
		m_PhysicsEngine[i]->initPhysics();	
		co.btID = m_PhysicsEngine[i]->CreateFixedCollisionRigidObject(m_smmPolydata[i],co.numTriangles,co.indices,co.numVertices,co.vertices,btVector3(0,0,0));
		numVertices = co.numVertices;
#endif
		//m_PhysXEngine[i] = new PhysXEngine();
		//m_PhysXEngine[i]->initPhysics();
#ifdef ODE_PHYSICS
		m_ODEEngine[i] = new ODEEngine();
		m_ODEEngine[i]->initPhysics();
		double initpos[3] = {0};
		co.odeID = m_ODEEngine[i]->CreateFixedCollisionRigidObject(m_smmPolydata[i],initpos, co.odeNumTriangles,co.odeIndices,co.odeNumVertices,co.odeVertices);
		numVertices = co.odeNumVertices;
#endif
	}

#ifdef BULLET_PHYSICS
	if (m_original_smm_vertices)
	{
		delete [] m_original_smm_vertices;
		m_original_smm_vertices = NULL;
	}
	m_original_smm_vertices = new btScalar[numVertices * 3];
	memcpy_s(m_original_smm_vertices, sizeof(btScalar)*numVertices*3, m_smmCO[0].vertices,sizeof(btScalar)*m_smmCO[0].numVertices*3);
#endif

#ifdef ODE_PHYSICS
	if (m_original_smm_ode_vertices)
	{
		delete [] m_original_smm_ode_vertices;
		m_original_smm_ode_vertices = NULL;
	}
	m_original_smm_ode_vertices = new double[numVertices * 3];
	memcpy_s(m_original_smm_ode_vertices, sizeof(double)*numVertices*3, m_smmCO[0].odeVertices,sizeof(double)*numVertices*3);
#endif




}

void MeshFrame::UpdateDeformMesh2(int obj_id)
{
	MyFrame *parentFrame = static_cast<MyFrame *>(this->GetParent());
			
	ImageRD *rd = smm.pimageRDs[obj_id];
	Properties *render_setting = smm.p_render_settings;
	//create value same dimesion
	int iActChem = render_setting->GetActiveChem();
	int iNumChem = rd->GetNumberOfChemicals();
	vtkPolyData* originalmesh = smm.original_mesh;
	int  resX  = (int )rd->GetX();
	int  resY  = (int )rd->GetY();

	vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(originalmesh->GetPointData()->GetNormals());		
	vtkSmartPointer<vtkFloatArray> warpData =  vtkSmartPointer<vtkFloatArray>::New();
	warpData->DeepCopy(pointNormals);
	warpData->SetName("warpData");
	double neutral = parentFrame->GetNeutralValue(obj_id);
	int numPoint = m_smmTexCoord->GetNumberOfTuples();
	for (int i = 0 ; i < numPoint; i++)
	{
		float x = 0.0f;
		float y = 0.0f;		
		float z = 0.0f;
#pragma omp critical
		{
		const double *meshTexcoordS = m_smmTexCoord->GetTuple3(i);
		const double *meshTexcoordT = meshTexcoordS + 1;		
	
		x = (resX -1.0)*(*meshTexcoordS);
		y = (resY -1.0)*(*meshTexcoordT);
		}
		
		float value = rd->GetValue(x,y,z, (iActChem)%iNumChem);
		
		float diff = value - neutral;		
			
		if (diff > 0)
		{
			double warpVector[3];
			warpData->GetTuple(i  ,warpVector);
			#define MOVEMENT_SCALE 0.15   
			double mf = MOVEMENT_SCALE  * 1.0;
			vtkMath::MultiplyScalar(warpVector, diff*mf ); 	
			warpData->SetTuple3( i , warpVector[0],warpVector[1],warpVector[2]);
		}
		else
		{
			warpData->SetTuple3( i , 0,0,0);
		}
	}

	vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
	polydata->DeepCopy(originalmesh);
	polydata->GetPointData()->AddArray(warpData);
	polydata->GetPointData()->SetActiveVectors(warpData->GetName());
	
#if 0
	vtkSmartPointer<vtkWarpVector> warpVector =  vtkSmartPointer<vtkWarpVector>::New();
	warpVector->SetInputData(polydata);
	warpVector->Update();
#else
	vtkSmartPointer<vtkSMPWarpVector> warpVector =  vtkSmartPointer<vtkSMPWarpVector>::New();
	warpVector->SetInputData(polydata);
	warpVector->Update(); 
#endif
	m_smmPolydata[obj_id]->ShallowCopy(warpVector->GetOutput());
	
	for (int i = 0; i < numPoint; i++) 
	{
		
		double *vertice = m_smmPolydata[obj_id]->GetPoint(i);

#ifdef BULLET_PHYSICS
		m_smmCO[obj_id].vertices[vOldID * 3 + 0] = vVertex[0];
		m_smmCO[obj_id].vertices[vOldID * 3 + 1] = vVertex[1];
		m_smmCO[obj_id].vertices[vOldID * 3 + 2] = vVertex[2];
#endif

#ifdef ODE_PHYSICS
		m_smmCO[obj_id].odeVertices[i * 3 + 0] = vertice[0];
		m_smmCO[obj_id].odeVertices[i * 3 + 1] = vertice[1];
		m_smmCO[obj_id].odeVertices[i * 3 + 2] = vertice[2];
#endif			
	}
			
			
			

	#ifdef ODE_PHYSICS
		dGeomTriMeshDataUpdate(dGeomTriMeshGetTriMeshDataID(m_smmCO[obj_id].odeID));
	#endif
	m_smmPolydata[obj_id]->Modified();


}

void MeshFrame::UpdateDeformMesh(int obj_id)
{
	#pragma omp critical
	{
		UpdateControlVerticesPosition(obj_id);		
		m_deformer[obj_id].Solve();	
	}


	int numVertex = m_vDeformedMesh[obj_id].GetVertexCount();
			
			
//#pragma omp parallel for 
	for (int i = 0; i < numVertex; i++) 
	{
		rms::IMesh::VertexID vID = i ;
		rms::IMesh::VertexID vOldID = m_VMap[obj_id].GetOld(vID);
		Wml::Vector3f vVertex;
		m_vDeformedMesh[obj_id].GetVertex(vID, vVertex);
		m_smmPolydata[obj_id]->GetPoints()->SetPoint(vOldID,vVertex[0],vVertex[1],vVertex[2]);			

#ifdef BULLET_PHYSICS
		m_smmCO[obj_id].vertices[vOldID * 3 + 0] = vVertex[0];
		m_smmCO[obj_id].vertices[vOldID * 3 + 1] = vVertex[1];
		m_smmCO[obj_id].vertices[vOldID * 3 + 2] = vVertex[2];
#endif

#ifdef ODE_PHYSICS
		m_smmCO[obj_id].odeVertices[vOldID * 3 + 0] = vVertex[0];
		m_smmCO[obj_id].odeVertices[vOldID * 3 + 1] = vVertex[1];
		m_smmCO[obj_id].odeVertices[vOldID * 3 + 2] = vVertex[2];
#endif			
	}
			
			
			

	#ifdef ODE_PHYSICS
		dGeomTriMeshDataUpdate(dGeomTriMeshGetTriMeshDataID(m_smmCO[obj_id].odeID));
	#endif
	m_smmPolydata[obj_id]->Modified();
		
}

void MeshFrame::UpdateControlVerticesPosition(int obj_id)
{

	/*
	if (!smm.pimageRD)
		return;
	*/
		if (m_controlVertices[obj_id].size() > 0)
		{
			MyFrame *parentFrame = static_cast<MyFrame *>(this->GetParent());
			//ImageRD *rd = smm.pimageRD;
			ImageRD *rd = smm.pimageRDs[obj_id];
			Properties *render_setting = smm.p_render_settings;
			//create value same dimesion
			int iActChem = render_setting->GetActiveChem();
			int iNumChem = rd->GetNumberOfChemicals();
			vtkPolyData* originalmesh = smm.original_mesh;
			int  resX  = (int )rd->GetX();
			int  resY  = (int )rd->GetY();

		
			double _diff_ms = m_diffTimeFromLastFrame;
		
		
		
			for (int i = 0 ; i < m_controlVertices[obj_id].size(); i++)
			{
				control_vertex &cv = m_controlVertices[obj_id][i];
				vtkIdType cvid = cv.vid;
				const double *meshTexcoordS = m_smmTexCoord->GetTuple3(cvid);
				const double *meshTexcoordT = meshTexcoordS + 1;		
				float x = (resX -1.0)*(*meshTexcoordS);
				float y = (resY -1.0)*(*meshTexcoordT);
				float z = 0.0f;
				float value = rd->GetValue(x,y,z, (iActChem)%iNumChem);
				double oriPos[3];
				originalmesh->GetPoint(cvid,oriPos);

				double NormalVector[3] = {0}; //same as normal
				vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(originalmesh->GetPointData()->GetNormals());
				pointNormals->GetTuple(cvid, NormalVector);

				double currentpos[3];
				m_smmPolydata[obj_id]->GetPoint(cvid,currentpos);
				double distance  = vtkMath::Distance2BetweenPoints(oriPos,cv.pos);
		#define MOVEMENT_SCALE 0.15    
		#define SPEED_SCALE 2500.0  // frame per sec
		
		#define MOVEMENT_SPEED (1000.0/60.0)  // calculate as 60 frame per 1000 msec 
			
				if (value  > 0.0f)
				{			

					if (distance < 0.2*(value))
					{
						//double current_speed = parentFrame->Get_timesteps_per_second();
						double sf = 1.0;
						/*
						if (current_speed > 0)
						{
							sf = current_speed/SPEED_SCALE;
						}
						*/
						//double mf = (MOVEMENT_SCALE / MOVEMENT_SPEED)*(_diff_ms);  
						double mf = MOVEMENT_SCALE*2;
					
						vtkMath::MultiplyScalar(NormalVector, mf*sf); 	
						//vtkMath::MultiplyScalar(NormalVector, -1.0);
						double newPos[3] = {0};		
						vtkMath::Add(cv.pos,NormalVector, newPos);
						m_deformer[obj_id].UpdateConstraint( cvid, Wm4::Vector3f(newPos[0],newPos[1],newPos[2]),CONTROLPOINT_WEIGHT);
						cv.pos[0] = newPos[0];
						cv.pos[1] = newPos[1];
						cv.pos[2] = newPos[2];
					}
				}
				else		
				{					
			
					if (distance > 0.05)
					{
						//double current_speed = parentFrame->Get_timesteps_per_second();
						double sf = 1.0;
						/*
						if (current_speed > 0)
						{
							sf = current_speed/SPEED_SCALE;
						}
						*/
						//double mf = (MOVEMENT_SCALE / MOVEMENT_SPEED)*(_diff_ms);  
						double mf = MOVEMENT_SCALE*2;
					
						double newPos[3] = {0};		
						double moveamount = mf*sf;
						if (moveamount > distance)
						{
							m_deformer[obj_id].UpdateConstraint( cvid, Wm4::Vector3f(oriPos[0],oriPos[1],oriPos[2]),CONTROLPOINT_WEIGHT);
							cv.pos[0] = oriPos[0];
							cv.pos[1] = oriPos[1];
							cv.pos[2] = oriPos[2];
						}
						else
						{
							vtkMath::MultiplyScalar(NormalVector, -moveamount);
							//vtkMath::MultiplyScalar(NormalVector, -1.0);
							vtkMath::Add(cv.pos,NormalVector, newPos);
							m_deformer[obj_id].UpdateConstraint( cvid, Wm4::Vector3f(newPos[0],newPos[1],newPos[2]),CONTROLPOINT_WEIGHT);
							cv.pos[0] = newPos[0];
							cv.pos[1] = newPos[1];
							cv.pos[2] = newPos[2];
						}					
					
					}
					else
					{
					
					}			
			
				}

				m_selectedPoints[obj_id]->SetPoint(cv.display_vid,cv.pos);

				if (fsDebugControlPoints[obj_id].is_open())
				{
					fsDebugControlPoints[obj_id] << "vid=" << cv.vid<< ",value=" <<value<< ",pos=" << cv.pos[0] << " " << cv.pos[1] << " " << cv.pos[2]  << endl;
				}
			}
			m_selectedPoints[obj_id]->Modified();	


			if (fsDebugControlPoints[obj_id].is_open())
				fsDebugControlPoints[obj_id] << "==========="  << endl;
		}
		//end of -- if (m_controlVertices[j].size() > 0)--
	

}


void MeshFrame::InitialCollisionObject()
{
	vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
	sphere->SetRadius(0.8);
	sphere->SetThetaResolution(10);
	sphere->SetPhiResolution(10);		
	sphere->Update();
	vtkSmartPointer<vtkTriangleFilter> triangleMesh = vtkSmartPointer<vtkTriangleFilter>::New();
	triangleMesh->SetInputConnection(sphere->GetOutputPort());
	triangleMesh->Update();
	vtkSmartPointer<vtkPolyDataMapper> mesh_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mesh_mapper->SetInputConnection(triangleMesh->GetOutputPort());
	m_objectiveCO.resize(m_numObj);
	for (int i = 0 ; i < m_numObj; i++)
	{
		vtkSmartPointer<vtkActor> mesh_actor = vtkSmartPointer<vtkActor>::New();
		mesh_actor->SetMapper(mesh_mapper);
		m_objectiveCO[i].actor = mesh_actor;
#ifdef BULLET_PHYSICS
		m_objectiveCO[i].btID = m_PhysicsEngine[i]->CreateCollisionRigidObject(triangleMesh->GetOutput(),
								m_objectiveCO[i].numTriangles,
																m_objectiveCO[i].indices,
																m_objectiveCO[i].numVertices,
																m_objectiveCO[i].vertices,
																10.0f, btVector3(m_initialObjectivePos[0],m_initialObjectivePos[1],m_initialObjectivePos[2]));
#endif			
		//PxVec3 initpos(m_initialObjectivePos[0],m_initialObjectivePos[1],m_initialObjectivePos[2]);
		//m_objectiveCO[i].pxActor = m_PhysXEngine[i]->CreateCollisionRigidObject(triangleMesh->GetOutput(),10.0f,initpos);
		//m_objectiveCO[i].pxActor = m_PhysXEngine[i]->CreateCollisionRigidSphereObject(0.8,10.0f,initpos);

#ifdef ODE_PHYSICS
		double _initpos[3] = {m_initialObjectivePos[0],m_initialObjectivePos[1],m_initialObjectivePos[2]};
		m_objectiveCO[i].odeID  = m_ODEEngine[i]->CreateCollisionRigidObject(triangleMesh->GetOutput(),10.0,_initpos, m_objectiveCO[i].odeNumTriangles,m_objectiveCO[i].odeIndices,m_objectiveCO[i].odeNumVertices,m_objectiveCO[i].odeVertices);
#endif
		
		//int btID = concaveDemo->CreateFixedCollisionRigidObject(triangleMesh->GetOutput(),numTriangles,Indices,numVertices,Vectices, btVector3(10,100,0));
		vtkSmartPointer<vtkTransform> transform  = vtkSmartPointer<vtkTransform>::New();
		transform->Translate(m_initialObjectivePos[0],m_initialObjectivePos[1],m_initialObjectivePos[2]);
		mesh_actor->SetUserTransform(transform);	
		mesh_actor->GetProperty()->SetColor(0.5,0,0);
	
		
		
		
		m_pRenderer[i]->AddActor(mesh_actor);



	}
}

void MeshFrame::ResetToInitialState(int obj_id ,bool deleteControlVertices)
{
	//to do multi obj



	wxMutexLocker lock(m_SolverMutex[obj_id]);	
	vtkPolyData* originalmesh = smm.original_mesh;
//#pragma omp critical
	m_smmPolydata[obj_id]->DeepCopy(originalmesh);
#ifdef BULLET_PHYSICS
	memcpy_s(m_smmCO[obj_id].vertices, m_smmCO[obj_id].numVertices*3*sizeof(btScalar), m_original_smm_vertices,m_smmCO[obj_id].numVertices*3*sizeof(btScalar));
#endif	
#ifdef ODE_PHYSICS
	memcpy_s(m_smmCO[obj_id].odeVertices, m_smmCO[obj_id].odeNumVertices*3*sizeof(double), m_original_smm_ode_vertices,m_smmCO[obj_id].odeNumVertices*3*sizeof(double));
#endif	
	vtkSmartPointer<vtkTransform> transform  = vtkSmartPointer<vtkTransform>::New();
	transform->Translate(m_initialObjectivePos[0],m_initialObjectivePos[1],m_initialObjectivePos[2]);
	

	{
	wxMutexLocker lock2(this->pVTKWindow->renderMutex);
	m_objectiveCO[obj_id].actor->SetUserTransform(transform);	
	m_objectiveCO[obj_id].actor->Modified();
	


#ifdef BULLET_PHYSICS
	m_PhysicsEngine[obj_id]->ResetScene();
#endif
#ifdef ODE_PHYSICS
	{
		//Ball part		
		dGeomID dstGeom = m_objectiveCO[obj_id].odeID;
		dBodyID dstBody = dGeomGetBody(dstGeom);
		
		{
			dMatrix4  rotmat;
			dRSetIdentity (rotmat);
			dBodySetRotation(dstBody,rotmat);
		}
		{
			
			dBodySetPosition(dstBody, m_initialObjectivePos[0],m_initialObjectivePos[1], m_initialObjectivePos[2]);
		}
		{
			
			dBodySetLinearVel(dstBody, 0,0,0);
		}
		{
			
			dBodySetAngularVel(dstBody, 0, 0,0);
		}
		{
			
			dBodySetForce(dstBody, 0,0,0);
		}
		{
			
			dBodySetTorque(dstBody, 0, 0,0);
		}
		{
			  
			dMatrix4  rotmat;
			dRSetIdentity (rotmat);
			dGeomSetRotation(dstGeom, rotmat);			
			dGeomSetPosition(dstGeom, m_initialObjectivePos[0],m_initialObjectivePos[1],m_initialObjectivePos[2]);
		}		
		{
			//dMass srcMass;
			//dBodyGetMass(srcBody, &srcMass);
			//dMass newmass;
			//dMassSetZero(&newmass);
			//dMassAdd(&newmass,&srcMass);
			//dBodySetMass(dstBody, &newmass);
		
		}
	}
	{
		//deform tube part
		
		dGeomID dstGeom = m_smmCO[obj_id].odeID;

		int memsize = m_smmCO[obj_id].odeNumVertices * sizeof(double) * 3;
		memcpy_s(m_smmCO[obj_id].odeVertices,memsize, m_smmCO[obj_id].odeVertices,memsize); 	
		dGeomTriMeshDataUpdate(dGeomTriMeshGetTriMeshDataID(dstGeom));
	}
	
#endif
	//m_PhysicsEngine->Tick();
	}
	

	
	if (deleteControlVertices)
	{		
		m_controlVertices[obj_id].clear();
		vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();		
		m_selectedPoints[obj_id]->ShallowCopy(_sPoints);
		m_deformer[obj_id].ClearConstraints();
		m_vDeformedMesh[obj_id].Clear(false);
		m_vDeformedMesh[obj_id]  = m_vDeformedMeshOriginal[obj_id];		
		m_deformer[obj_id].SetMesh(&m_vDeformedMesh[obj_id]);
		m_deformer[obj_id].AddBoundaryConstraints(4.0f);		
		//m_deformer.AddBoundaryConstraints(4.0);
	}
	else
	{
		
		for (int i = 0 ; i < m_controlVertices[obj_id].size(); i++)
		{
			control_vertex &cv = m_controlVertices[obj_id][i];
			vtkIdType cvid = cv.vid;

			double oriPos[3];
			originalmesh->GetPoint(cvid,oriPos);
			m_deformer[obj_id].UpdateConstraint( cvid, Wm4::Vector3f(oriPos[0],oriPos[1],oriPos[2]),CONTROLPOINT_WEIGHT);
			cv.pos[0] = oriPos[0];
			cv.pos[1] = oriPos[1];
			cv.pos[2] = oriPos[2];
		}
	}
	//m_lastFrameTime = -1.0;

}


bool MeshFrame::SetControlVertices(int obj_id , std::vector<int>& controlverticesID)
{
	return true;
	std::sort( controlverticesID.begin(), controlverticesID.end() );
	controlverticesID.erase( std::unique( controlverticesID.begin(), controlverticesID.end() ), controlverticesID.end() );
	wxMutexLocker lock(m_SolverMutex[obj_id]);
	m_controlVertices[obj_id].clear();
	m_deformer[obj_id].ClearConstraints();
	m_deformer[obj_id].AddBoundaryConstraints(4.0f);
	wxMutexLocker lock2(this->pVTKWindow->renderMutex);
	vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();	
	m_selectedPoints[obj_id]->ShallowCopy(_sPoints);

	int numPoints = smm.original_mesh->GetNumberOfPoints();
	//add 

	for (int i = 0 ; i < controlverticesID.size() ; i++)
	{		
		if (controlverticesID[i] >= 0 && controlverticesID[i] <= numPoints - 1)
		{
			control_vertex cv;
			cv.vid = controlverticesID[i];
			vtkIdType cvid = cv.vid;
			smm.original_mesh->GetPoint(controlverticesID[i],cv.pos);
			cv.display_vid = m_selectedPoints[obj_id]->InsertNextPoint(cv.pos);

			double oriPos[3];
			smm.original_mesh->GetPoint(cvid,oriPos);
			m_deformer[obj_id].UpdateConstraint( cv.vid, Wm4::Vector3f(oriPos[0],oriPos[1],oriPos[2]),CONTROLPOINT_WEIGHT);
			//calculate plane position
			//double *texcoord = m_smmTexCoord->GetTuple2(cv.vid);			
			//smm.pimageRD->AddControlPoints((float )texcoord[0],(float )texcoord[1],0.0f,true);
			m_controlVertices[obj_id].push_back(cv);
		}
		else
		{
			//invalid range
			return false;
		}
	}
	
	m_deformer[obj_id].CalMatrix();
	return true;
}

bool MeshFrame::SetControlVertices(int obj_id , std::vector<float> & controlTexCoord)
{
	
	if (controlTexCoord.size() % 2 != 0)
		return false;

	wxMutexLocker lock(m_SolverMutex[obj_id]);
	m_controlVertices[obj_id].clear();
	m_deformer[obj_id].ClearConstraints();
	m_deformer[obj_id].AddBoundaryConstraints(4.0f);

	//wxMutexLocker lock2(this->pVTKWindow->renderMutex);
	vtkSmartPointer<vtkPoints> _sPoints = vtkSmartPointer<vtkPoints>::New();	
	m_selectedPoints[obj_id]->ShallowCopy(_sPoints);


	vtkSmartPointer<vtkDoubleArray> texCoord =vtkSmartPointer<vtkDoubleArray>::New();
	texCoord->DeepCopy(m_smmTexCoord);
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();	
	points->SetData( texCoord);
	vtkSmartPointer<vtkPolyData> point_poly =   vtkSmartPointer<vtkPolyData>::New();   
	point_poly->SetPoints(points);
	

	vtkSmartPointer<vtkKdTreePointLocator> kDTree = vtkSmartPointer<vtkKdTreePointLocator>::New();
	kDTree->SetDataSet(point_poly);
	kDTree->BuildLocator();
 
 
	for (int i = 0; i < controlTexCoord.size()/2; i++)
	{
		double pt[3] = {(double )controlTexCoord[i*2 + 0],(double )controlTexCoord[i*2 + 1],0};
		vtkIdType iD = kDTree->FindClosestPoint(pt);
		//std::cout << "The closest point is point " << iD << std::endl;

		control_vertex cv;
		cv.vid = iD;
		vtkIdType cvid = cv.vid;
		smm.original_mesh->GetPoint(iD,cv.pos);
		cv.display_vid = m_selectedPoints[obj_id]->InsertNextPoint(cv.pos);

		double oriPos[3];
		smm.original_mesh->GetPoint(cvid,oriPos);
		m_deformer[obj_id].UpdateConstraint( cv.vid, Wm4::Vector3f(oriPos[0],oriPos[1],oriPos[2]),CONTROLPOINT_WEIGHT);
		//calculate plane position
		//double *texcoord = m_smmTexCoord->GetTuple2(cv.vid);			
		//smm.pimageRD->AddControlPoints((float )texcoord[0],(float )texcoord[1],0.0f,true);
		m_controlVertices[obj_id].push_back(cv);
	}

//#pragma omp critical
	m_deformer[obj_id].CalMatrix();
	return true;
}
int  MeshFrame::GetObjectivePosition(int obj_id , double *op_pos)
{
	//wxMutexLocker lock1(MeshFrame::s_UpdateMutex);
	
	wxMutexLocker lock2(m_SolverMutex[obj_id]);
	
	
#ifdef BULLET_PHYSICS
	double m[16];
	int rev= m_PhysicsEngine[obj_id]->GetCollisionObjectTransformMatrix( m_objectiveCO[obj_id].btID,m);
	op_pos[0] = m[12];
	op_pos[1] = m[13];
	op_pos[2] = m[14];
#endif
#ifdef ODE_PHYSICS
	double pos[3], rot[4];
	m_ODEEngine[obj_id]->GetCollisionObjectTransform( m_objectiveCO[obj_id].odeID, pos,rot);
	
	op_pos[0] = pos[0];
	op_pos[1] = pos[1];
	op_pos[2] = pos[2];
#endif
	return 0;

}

int MeshFrame::GetRevisionUpdate(int mid) 
{
	wxMutexLocker lock2(m_SolverMutex[mid]);
	//wxMutexLocker lock1(s_MeshFrameMutex);
	
	return m_revisionUpdate[mid]; 
}
void MeshFrame::OnIdle(wxIdleEvent& evt)
{
	
	/*
	m_diffTimeFromLastFrame  = m_clock.getTimeMilliseconds();
	UpdateDeformMesh(g_pMainFrame->IsRunning());
	m_clock.reset();
	*/
	/*
	if (m_textActor)
	{
		double pos[3];
		GetObjectivePosition(pos);
		wxString ss = "ball x pos = ";
		ss << pos[0];
		m_textActor->SetInput(ss);
		m_textActor->Modified();
	}
	*/
	evt.Skip();
}



void MeshFrame::UpdatePhysicsObjects(int obj_id)
{
	m_physicsLog[obj_id] += ReportPhysicsProperties(obj_id,false);
	#pragma omp critical
	{

#ifdef BULLET_PHYSICS
		double m[16] = {0};
		int version = m_PhysicsEngine[obj_id]->GetCollisionObjectTransformMatrix(m_objectiveCO[obj_id].btID,m);
		/*
		if (fsDebugBallPos[obj_id].is_open())
		{			
			fsDebugBallPos[obj_id] << version << ":" << m[12] << " " << m[13] << " " << m[14]  << endl;			
		}
		*/
		vtkMatrix4x4::Transpose(m,m);
			
		vtkSmartPointer<vtkMatrix4x4> mat4x4 = vtkSmartPointer<vtkMatrix4x4>::New();
		mat4x4->DeepCopy(m);	
		m_objectiveCO[obj_id].actor->SetUserMatrix(mat4x4);
		m_objectiveCO[obj_id].actor->Modified();
#endif	
#ifdef ODE_PHYSICS		
		double mat[16] = {0};
		double pos[3];
		double quat[4];
		m_ODEEngine[obj_id]->GetCollisionObjectTransform(m_objectiveCO[obj_id].odeID, pos,quat);
		
		double m3x3[3][3];
		vtkMath::QuaternionToMatrix3x3(quat,m3x3);
		for (int i = 0; i < 3 ; i++)
			memcpy( &mat[i*4 ], m3x3[i], sizeof(double)*3);
		memcpy( &mat[12], pos, sizeof(double)*3);
		mat[15] = 1.0;
		
		vtkMatrix4x4::Transpose(mat,mat);
			
		vtkSmartPointer<vtkMatrix4x4> _mat4x4 = vtkSmartPointer<vtkMatrix4x4>::New();
		_mat4x4->DeepCopy(mat);	
		m_objectiveCO[obj_id].actor->SetUserMatrix(_mat4x4);
		m_objectiveCO[obj_id].actor->Modified();
#endif
	//	vtkSmartPointer<vtkMatrix4x4> mat4x4 = vtkSmartPointer<vtkMatrix4x4>::New();
	//	mat4x4 = m_objectiveCO[obj_id].actor->GetUserMatrix();
	//	mat4x4->
		
		

		//if (!(pos[0] == -2.0 &&pos[1] == 0.0 &&pos[2]== 0.0))
			//	int aaa = 0;
		/*
		//bool ret = m_PhysXEngine[obj_id]->fetchResult();
		if (m_PhysXEngine[obj_id]->fetchResult())
		{
			PxRigidDynamic *dynActor = static_cast<PxRigidDynamic*>(m_objectiveCO[obj_id].pxActor);
			PxVec3 pos = dynActor->getGlobalPose().p;

			if (!(pos.x == -2.0 &&pos.y == 0.0 &&pos.z == 0.0))
				int aaa = 0;
		}
		*/
	}
	
}




void MeshFrame::PrepareMeshFrames(int numFrames)
{
	wxMutexLocker lock(s_MeshFrameMutex);
	if (g_numMeshFrame != numFrames)
	{
		if (g_pMeshFrame)
		{
			g_pMeshFrame->Close();
			delete g_pMeshFrame; 
			g_pMeshFrame = NULL;
		}
		int screen_height = wxSystemSettings::GetMetric ( wxSYS_SCREEN_Y );
		//create new meshframes
		

		g_pMeshFrame = new MeshFrame(g_pMainFrame,"Meshes",wxPoint(0,0), wxSize(400, 300),numFrames);
		g_pMeshFrame->Show();	
		
		int column = 0;
		int row = 0;
		for (int i = 0 ; i < numFrames ; i++)
		{	
			wxString str("Mesh") ;
			str << i ;

			if ((row + 1) * 300 > screen_height)
			{
				column++;
				row = 0;
			}
			
			row++;
		}	

		g_numMeshFrame = numFrames;
		usingMeshFrame = std::vector<bool>(numFrames);
		readyMeshFrame = std::vector<bool>(numFrames);
		std::fill(usingMeshFrame.begin(),usingMeshFrame.end(), false);
		std::fill(readyMeshFrame.begin(),readyMeshFrame.end(), false);
		
	}
	else
	{
		for (int i = 0 ; i < g_numMeshFrame ; i++) 
		{
			g_pMeshFrame->ResetToInitialState(i,true);
		}
		std::fill(usingMeshFrame.begin(),usingMeshFrame.end(), false);
		std::fill(readyMeshFrame.begin(),readyMeshFrame.end(), false);
	}

	//prepare rd frames
	FormulaOpenCLImageRD** systems = new FormulaOpenCLImageRD*[g_numMeshFrame];
	for (int i = 0 ;  i < g_numMeshFrame ;i++)
	{
		//systems[i] = new FormulaOpenCLImageRD(opencl_platform,opencl_device);

		OpenCLImageRD * rd = (OpenCLImageRD *)g_pMainFrame->GetCurrentRDSystem();
		systems[i] = new FormulaOpenCLImageRD(*rd);
		systems[i]->InitializeRenderPipeline( g_pMainFrame->GetRenderer(),g_pMainFrame->GetRenderSettings());
		
	}
	
	
	
}

int MeshFrame::ReserveMeshFrameID()
{
	wxMutexLocker lock(s_MeshFrameMutex);
	for (int i = 0; i < usingMeshFrame.size(); i++)
	{		
		if (!usingMeshFrame[i])
		{
			usingMeshFrame[i] = true;
			readyMeshFrame[i] = false;
			
			return i;
		}
	}

	return -1;
}

void MeshFrame::ReleaseMeshFrameID(int id)
{
	wxMutexLocker lock(s_MeshFrameMutex);	
	usingMeshFrame[id]  = false;
	readyMeshFrame[id] = false;	
}

bool MeshFrame::IsMeshFrameUsing(int id)
{	
	//wxMutexLocker lock(s_MeshFrameMutex);
	return usingMeshFrame[id];
}

void MeshFrame::SetMeshFrameReady(int id,bool ready)
{
	wxMutexLocker lock(s_MeshFrameMutex);
	readyMeshFrame[id]=ready;
	
}


bool MeshFrame::IsAllReady()
{
	wxMutexLocker lock(s_MeshFrameMutex);
	int uMFCount = 0;
	for (int i = 0; i < g_numMeshFrame;i++)
	{
		bool uMF = usingMeshFrame[i];
		bool readyMF = readyMeshFrame[i];
		if (uMF)
		{
			//if use 
			uMFCount++;
			if (!readyMF)
			{
				// but not ready
	
				return false;
			}
		}
		
	}	
	if (uMFCount == 0)
		return false;
	else
		return true;
}


void MeshFrame::StartRecordDebug(int mid)
{
	{
		char filename[MAX_PATH];
		sprintf_s(filename,"ControlPoints%d.txt" ,mid);	
		fsDebugControlPoints[mid].open(filename);
	}

	{
		char filename[MAX_PATH];
		sprintf_s(filename,"BallPos%d.txt" ,mid);	
		fsDebugBallPos[mid].open(filename);
	}

	
	

	
}

void MeshFrame::EndRecordDebug(int mid)
{
	fsDebugControlPoints[mid].flush();
	fsDebugBallPos[mid].flush();

	fsDebugControlPoints[mid].close();	
	fsDebugBallPos[mid].close();
	
}
	