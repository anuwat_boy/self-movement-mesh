#include "stdafx.h"
#include <wx/tokenzr.h>
#include "SimulationDialog.h"
#include <sstream>      // std::stringstream


SimulationDialog::SimulationDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	m_mgr.SetManagedWindow(this);
	m_mgr.SetFlags(wxAUI_MGR_DEFAULT);
	
	m_staticText1 = new wxStaticText( this, wxID_ANY, wxT("Chromosome Size"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_staticText1->Wrap( -1 );
	m_mgr.AddPane( m_staticText1, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Number of MeshFrame "), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_staticText2->Wrap( -1 );
	m_mgr.AddPane( m_staticText2, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Fixed().DockFixed( false ).Row( 3 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Evaluate Time"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_staticText3->Wrap( -1 );
	m_mgr.AddPane( m_staticText3, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );

	m_spinCtrlChromosomeSize = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS|wxTE_PROCESS_ENTER, 1, 10000, 8 );
	m_spinCtrlChromosomeSize->Enable( false );
	
	m_mgr.AddPane( m_spinCtrlChromosomeSize, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ) );

	m_spinCtrlEvalTime = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS|wxTE_PROCESS_ENTER, 1, 100000000, 60000 );
	m_spinCtrlEvalTime->Enable( true );
	
	m_mgr.AddPane( m_spinCtrlEvalTime, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ) );
		
	m_spinCtrlMeshFrame = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS|wxTE_PROCESS_ENTER, 1, 10000, 1 );
	m_mgr.AddPane( m_spinCtrlMeshFrame, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 2 ) );
	
	//m_gridChromosome = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	//
	//// Grid
	//m_gridChromosome->CreateGrid( 1, 10 );
	//m_gridChromosome->EnableEditing( true );
	//m_gridChromosome->EnableGridLines( true );
	//m_gridChromosome->EnableDragGridSize( false );
	//m_gridChromosome->SetMargins( 0, 0 );
	//
	//// Columns
	//m_gridChromosome->EnableDragColMove( false );
	//m_gridChromosome->EnableDragColSize( false );
	//m_gridChromosome->SetColLabelSize( 30 );
	//m_gridChromosome->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	//
	//// Rows
	//m_gridChromosome->EnableDragRowSize( false );
	//m_gridChromosome->SetRowLabelSize( 30 );
	//m_gridChromosome->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	//
	//// Label Appearance
	//
	//// Cell Defaults
	//m_gridChromosome->SetDefaultCellAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	//m_gridChromosome->Enable( false );
	
	//m_mgr.AddPane( m_gridChromosome, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).CloseButton( false ).Dock().Resizable().FloatingSize( wxDefaultSize ).DockFixed( false ).Row( 1 ).MinSize( wxSize( -1,80 ) ) );
	
	m_buttonRun = new wxButton( this, wxID_ANY, wxT("Run"), wxDefaultPosition, wxDefaultSize, 0 );
	m_mgr.AddPane( m_buttonRun, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ) );
	
	m_buttonClose = new wxButton( this, wxID_ANY, wxT("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	m_mgr.AddPane( m_buttonClose, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ) );
	
	//wxString m_radioBoxInputTypeChoices[] = { wxT("Chromosome Text Style"), wxT("Table Style") };
	//int m_radioBoxInputTypeNChoices = sizeof( m_radioBoxInputTypeChoices ) / sizeof( wxString );
	//m_radioBoxInputType = new wxRadioBox( this, wxID_ANY, wxT("Input"), wxDefaultPosition, wxDefaultSize, m_radioBoxInputTypeNChoices, m_radioBoxInputTypeChoices, 1, wxRA_SPECIFY_COLS );
	//m_radioBoxInputType->SetSelection( 0 );
	//m_mgr.AddPane( m_radioBoxInputType, wxAuiPaneInfo() .Left() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Fixed().DockFixed( false ) );
	

	wxString default_chromosome = wxT("8 0.425159 0.35111 0.438976 0.701357 0.31835 0.740076 0.656817 0.739564");
	wxString default_chromosomes = wxT("8 0.482252 0.83212 0.608041 0.217713 0.481587 0.285567 0.675102 0.134536") ;

	std::stringstream sshex;
	for (int j = 0 ; j < 5 ; j++)
	{
		for (int i = 0 ; i < 8 ; i++)
		{
			double val = ((double )i + ((double )j*0.5)) / 255.0;
			sshex << wxString::Format("%llX ",val);
		}
		sshex << endl;
	}
	default_chromosomes = wxString(sshex.str());
	/*
default_chromosomes+=wxT("8 0.813033 0.639118 0.470292 0.871931 0.13099 0.587635 0.569508 0.250881\n");
default_chromosomes+=wxT("8 0.678415 0.615025 0.466058 0.652606 0.204627 0.581483 0.484296 0.357335\n");
default_chromosomes+=wxT("8 0.715306 0.428935 0.833107 0.245968 0.117873 0.406539 0.672673 0.824971\n");
default_chromosomes+=wxT("8 0.673554 0.675719 0.507838 0.701461 0.152462 0.33962 0.434605 0.495679\n");
default_chromosomes+=wxT("8 0.606776 0.889262 0.473816 0.571609 0.306289 0.206763 0.616188 0.723689\n");
default_chromosomes+=wxT("8 0.793899 0.656743 0.353061 0.495696 0.549682 0.0258497 0.803289 0.720358\n");
default_chromosomes+=wxT("8 0.402704 0.943415 0.557663 0.625653 0.0982898 0.000658865 0.610066 0.903124\n");
default_chromosomes+=wxT("8 0.387575 0.808898 0.754528 0.577603 0.313957 0.153651 0.483321 0.794171\n");
default_chromosomes+=wxT("8 0.387575 0.808898 0.754528 0.577603 0.313957 0.153651 0.483321 0.794171\n");
default_chromosomes+=wxT("8 0.544373 0.863145 0.602835 0.57102 0.25905 0.310025 0.642914 0.757782\n");
default_chromosomes+=wxT("8 0.424828 0.69274 0.301451 0.289447 0.444043 0.396238 0.433433 0.799696\n");
default_chromosomes+=wxT("8 0.525892 0.695213 0.261466 0.217241 0.230565 0.227657 0.372821 0.716865\n");
default_chromosomes+=wxT("8 0.525892 0.695213 0.261466 0.217241 0.230565 0.227657 0.372821 0.716865\n");
default_chromosomes+=wxT("8 0.525892 0.695213 0.261466 0.217241 0.230565 0.227657 0.372821 0.716865\n");
default_chromosomes+=wxT("8 0.42216 0.568956 0.159387 0.00757294 0.376869 0.390057 0.332033 0.947165\n");
default_chromosomes+=wxT("8 0.42216 0.568956 0.159387 0.00757294 0.376869 0.390057 0.332033 0.947165\n");
default_chromosomes+=wxT("8 0.42216 0.568956 0.159387 0.00757294 0.376869 0.390057 0.332033 0.947165\n");
default_chromosomes+=wxT("8 0.468439 0.50623 0.197191 0.0709384 0.176595 0.309171 0.187373 0.941211\n");
*/
	m_textCtrlChromosome = new wxTextCtrl( this, wxID_ANY, default_chromosomes, wxPoint( 50,-1 ), wxDefaultSize, wxHSCROLL|wxTE_MULTILINE|wxTE_PROCESS_ENTER );
	m_mgr.AddPane( m_textCtrlChromosome, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).PinButton( true ).Dock().Fixed().DockFixed( false ).BottomDockable( false ).TopDockable( false ).LeftDockable( false ).Floatable( false ).Row( 2 ).Position( 0 ).BestSize( wxSize( 600,150 ) ).Layer( 0 ) );
	
	m_spinCtrlPropagateIntervalTime = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 1000000, 20000 );
	m_mgr.AddPane( m_spinCtrlPropagateIntervalTime, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 0 ).BestSize( wxSize( 100,25 ) ) );
	
	m_staticTextPropagate = new wxStaticText( this, wxID_ANY, wxT("Propagate Interval Time"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextPropagate->Wrap( -1 );
	m_mgr.AddPane( m_staticTextPropagate, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).Position( 0 ).BestSize( wxSize( -1,25 ) ) );

	wxString m_radioBoxBehaviorModeChoices[] = { wxT("Restart mode"), wxT("Update mode") };
	int m_radioBoxBehaviorModeNChoices = sizeof( m_radioBoxBehaviorModeChoices ) / sizeof( wxString );
	m_radioBoxBehaviorMode = new wxRadioBox( this, wxID_ANY, wxT("behavior mode"), wxDefaultPosition, wxDefaultSize, m_radioBoxBehaviorModeNChoices, m_radioBoxBehaviorModeChoices, 1, wxRA_SPECIFY_COLS );
	m_radioBoxBehaviorMode->SetSelection( 1 );
	m_mgr.AddPane( m_radioBoxBehaviorMode, wxAuiPaneInfo() .Left() .CaptionVisible( false ).CloseButton( false ).Movable( false ).Dock().Fixed().DockFixed( false ).Floatable( false ) );

	

	m_mgr.Update();
	this->Centre( wxBOTH );
	
	// Connect Events
	m_spinCtrlChromosomeSize->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SimulationDialog::m_spinCtrlChromosomeSizeOnSpinCtrl ), NULL, this );	
	//m_gridChromosome->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( SimulationDialog::m_gridChromosomeOnKeyDown ), NULL, this );
	m_buttonRun->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SimulationDialog::m_buttonRunOnButtonClick ), NULL, this );
	m_buttonClose->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SimulationDialog::m_buttonCloseOnButtonClick ), NULL, this );
	//m_radioBoxInputType->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( SimulationDialog::m_radioBoxInputTypeOnRadioBox ), NULL, this );
	m_radioBoxBehaviorMode->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( SimulationDialog::m_radioBoxBehaviorModeOnRadioBox ), NULL, this );
	m_textCtrlChromosome->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(SimulationDialog::OnKeyDown), NULL, this);
	MeshFrame::PrepareMeshFrames(1);
	//int num_possibleControlVertice = smm.original_mesh->GetNumberOfPoints();
	//editor = new wxGridCellNumberEditor(0, num_possibleControlVertice-1);
	//m_gridChromosome->SetDefaultEditor(editor);
	/*
	m_gridChromosome->SetCellValue(0,0,"594");
	m_gridChromosome->SetCellValue(0,1,"213");
	m_gridChromosome->SetCellValue(0,2,"364");
	m_gridChromosome->SetCellValue(0,3,"199");
	m_gridChromosome->SetCellValue(0,4,"484");
	m_gridChromosome->SetCellValue(0,5,"461");
	m_gridChromosome->SetCellValue(0,6,"216");
	m_gridChromosome->SetCellValue(0,7,"256");
	*/

}

SimulationDialog::~SimulationDialog()
{
	// Disconnect Events
	m_spinCtrlChromosomeSize->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SimulationDialog::m_spinCtrlChromosomeSizeOnSpinCtrl ), NULL, this );
	m_spinCtrlMeshFrame->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( SimulationDialog::m_spinCtrlChromosomeSizeOnSpinCtrl ), NULL, this );
	//m_gridChromosome->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( SimulationDialog::m_gridChromosomeOnKeyDown ), NULL, this );
	m_buttonRun->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SimulationDialog::m_buttonRunOnButtonClick ), NULL, this );
	m_buttonClose->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SimulationDialog::m_buttonCloseOnButtonClick ), NULL, this );
	//m_radioBoxInputType->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( SimulationDialog::m_radioBoxInputTypeOnRadioBox ), NULL, this );
	m_radioBoxBehaviorMode->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( SimulationDialog::m_radioBoxBehaviorModeOnRadioBox ), NULL, this );
	m_textCtrlChromosome->Disconnect(wxEVT_KEY_DOWN, wxKeyEventHandler(SimulationDialog::OnKeyDown), NULL, this);
	m_mgr.UnInit();
	//delete editor;
	
}

int SimulationDialog::GetCellData(std::vector<std::vector<double>> &datas)
{
	datas.clear();
	std::vector<double> data;
	
	wxString str = m_textCtrlChromosome->GetValue();
	str.Replace("\r","",true);
	str.Replace(" \n", "\n",true);
	wxStringTokenizer tokenizer(str);

	while ( tokenizer.HasMoreTokens() )
	{
		wxString token = tokenizer.GetNextToken();
		wxChar deli =	tokenizer.GetLastDelimiter() ;
		// process token here
		double value = 0;
		bool covertedResult = token.ToDouble(&value);
		if (covertedResult)
		{
			data.push_back((double)value);
		}
		else
		{
			//convert hex
			if (sscanf_s(token,"%llx", &value) == -1)
			{
				//cannot convert

			}
			else
				data.push_back((double)value);
		}
		if (deli == '\n' || deli == '\0' ||deli == '\r' )
		{
			//check first item is chromosome size			
			if (data[0]== data.size()-1)			
			{
				//erase 
				data.erase(data.begin(),data.begin()+1);
			}
			datas.push_back(data);
			data.clear();

		}
	}

	if (data.size() > 0)
	{
	
		if (data[0]== data.size()-1)			
		{
			//erase 
			data.erase(data.begin(),data.begin()+1);
		}
		datas.push_back(data);
		
	}
	return datas.size();

}

void SimulationDialog::OnKeyDown( wxKeyEvent &e)
{
	switch(e.GetKeyCode())
   {
       case 'a':
       case 'A':
              if(e.ControlDown())
              {
				  if (e.GetEventObject()->GetClassInfo()->IsKindOf(wxCLASSINFO(wxTextCtrl))	)
				  {
					  wxTextCtrl* aTextCtrl = reinterpret_cast<wxTextCtrl*>( e.GetEventObject());
					  aTextCtrl->SetSelection(-1,-1);
				  }

                   
              }
              else
                  e.Skip();
              break;

       default:
              e.Skip();
              break;
   }
}
int SimulationDialog::GetCellData(wxString str ,  std::vector<std::vector<double>> &datas)
{
	datas.clear();
	std::vector<double> data;
	
	str.Replace("\r","",true);
	str.Replace(" \n", "\n",true);
	wxStringTokenizer tokenizer(str);
	while ( tokenizer.HasMoreTokens() )
	{
		wxString token = tokenizer.GetNextToken();
		wxChar deli =	tokenizer.GetLastDelimiter() ;
		// process token here
		double value = 0;
		bool covertedResult = token.ToDouble(&value);
		if (covertedResult)
		{
			data.push_back((double)value);
		}
		else
		{
			//convert hex
			if (sscanf_s(token,"%llx", &value) == -1)
			{
				//cannot convert

			}
			else
				data.push_back((double)value);
		}

		if (deli == '\n' || deli == '\0' || deli == '\r' )
		{
			//check first item is chromosome size			
			if (data[0]== data.size()-1)			
			{
				//erase 
				data.erase(data.begin(),data.begin()+1);
			}
			datas.push_back(data);
			data.clear();
		}
	}

	if (data.size() > 0)
	{
		if (data[0]== data.size()-1)			
		{
			//erase 
			data.erase(data.begin(),data.begin()+1);
			datas.push_back(data);
		}
	}
	return datas.size();
}


int SimulationDialog::GetCellData(std::vector<double> &data)
{
	
	wxString str = m_textCtrlChromosome->GetValue();
	str.Replace("\r","",true);
	str.Replace(" \n", "\n",true);
	wxStringTokenizer tokenizer(str);
	while ( tokenizer.HasMoreTokens() )
	{
		wxString token = tokenizer.GetNextToken();
		// process token here
		double value = 0;
		bool covertedResult = token.ToDouble(&value);
		if (covertedResult)
		{
			data.push_back((float)value);
		}
	}

	//check first item is chromosome size			
	if (data[0]== data.size()-1)			
	{
		//erase 
		data.erase(data.begin(),data.begin()+1);
	}

	return data.size();
	
}


void SimulationDialog::m_spinCtrlChromosomeSizeOnSpinCtrl( wxSpinEvent& event ) 
{
	int numChromosome = event.GetValue();
	int numColumn = m_gridChromosome->GetNumberCols();
	if (numColumn < numChromosome)
	{
		m_gridChromosome->AppendCols(numChromosome - numColumn);
	}
	else if (numColumn > numChromosome)
	{
		m_gridChromosome->DeleteCols(numChromosome, numColumn - numChromosome);
	}
	event.Skip();
}
void SimulationDialog::m_buttonRunOnButtonClick( wxCommandEvent& event ) 
{
	//this->m_buttonRun->Disable();
	OptimizationFrame::s_PropagateIntervalTime = this->m_spinCtrlPropagateIntervalTime->GetValue();
	OptimizationFrame::s_behaviorMode = this->m_radioBoxBehaviorMode->GetSelection();
	int num_meshframe  = m_spinCtrlMeshFrame->GetValue();
	UINT64 time = m_spinCtrlEvalTime->GetValue();
	//MeshFrame::PrepareMeshFrames(num_meshframe);	
	g_pMainFrame->InitMultiRDSystems();
	//int mid = MeshFrame::ReserveMeshFrameID();
	
	//std::vector<int> controlVertice;
	
	std::vector<std::vector<double>> texCoords;
	//int chromosize = GetCellData(texCoord);
	GetCellData(texCoords);
	if (texCoords.size() == 0)
	{
		return;
	}
	
	std::vector<MyIndivi> indi_list;

	for (int i = 0; i < texCoords.size(); i++)
	{

		//vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();
		//vtkSmartPointer<vtkImageCanvasSource2D> drawing =  vtkSmartPointer<vtkImageCanvasSource2D>::New();
		//drawing->SetScalarTypeToUnsignedChar();
		//drawing->SetDrawColor(100, 100, 100, 0);
		//drawing->SetNumberOfScalarComponents(3);
		//drawing->SetExtent(0, 256, 0, 256, 0, 0);
		//drawing->FillBox(0,256,0,256);
 
		//// Draw a red circle of radius 5 centered at (9,10)
		////drawing->SetDrawColor(255, 0, 0, 0.5);
		//drawing->SetDrawColor(255, 0, 0, 0);
		
		

		std::vector<double> &texCoord = texCoords[i];
		MyIndivi ind;
		for (int j = 0; j < texCoord.size(); j++)
		{
			ind.push_back(texCoord[j]);	
		}
		indi_list.push_back(ind);

		/*for (int j = 0; j < texCoord.size(); j+=2)
		{
			int cx = texCoord[j]*256;
			int cy = texCoord[j+1]*256;
			drawing->DrawCircle( cx,cy, 10);
			drawing->FillPixel( cx,cy);
		}
		drawing->Update();
		
		vtkSmartPointer<vtkPNGWriter> pngwriter = vtkSmartPointer<vtkPNGWriter>::New();
		pngwriter->SetInputData(drawing->GetOutput());
		std::stringstream ss;
		char currdir[MAX_PATH] = "";
		vtkDirectory::GetCurrentWorkingDirectory(currdir,MAX_PATH);
		ss << currdir << "\\check";
		vtkDirectory::MakeDirectory(ss.str().c_str());
		ss << "\\round" <<  i << ".png\0";			

		pngwriter->SetFileName(ss.str().c_str());
		pngwriter->Write();*/

	}
	
	
	g_pOptimizationFrame->AppendLog("Starting Testing",true);
	g_pMeshFrame->ResetToInitialState(0, false);
	g_pMainFrame->BackToInitialStateAndRun(time,true);

	//bool validcv = g_pMeshFrame[mid]->SetControlVertices(controlVertice);	
	SimulationThread *thread = new SimulationThread(indi_list,num_meshframe);
	thread->Create();
	thread->Run();
	
	/*
	
	*/
	event.Skip(); 
}

void SimulationDialog::m_buttonCloseOnButtonClick( wxCommandEvent& event ) 
{
	event.Skip();
}



void SimulationDialog::m_gridChromosomeOnKeyDown(wxKeyEvent &event)
{
	/*
	if(event.GetKeyCode()==WXK_RETURN)
	{
		if(m_gridChromosome->IsCellEditControlEnabled())
			m_gridChromosome->MoveCursorRight(false);
	}
	*/
	event.Skip();
} 

void SimulationDialog::m_radioBoxInputTypeOnRadioBox( wxCommandEvent& event )
{
	int sel = event.GetSelection();
	switch (sel)
	{
	case 0:
		m_gridChromosome->Enable(false);
		m_spinCtrlChromosomeSize->Enable(false);
		m_textCtrlChromosome->Enable(true);
		break;
	case 1:
		m_gridChromosome->Enable(true);
		m_spinCtrlChromosomeSize->Enable(true);
		m_textCtrlChromosome->Enable(false);
		break;
	default:
		break;
	}

	event.Skip();
}

wxThread::ExitCode SimulationThread::Entry()
{
	if (OptimizationFrame::s_behaviorMode == 0)
	{
		#pragma omp parallel for 	
		for (int i = 0; i < m_num_meshframe; ++i) 
		{
			OptimizationFrame::Evaluate(m_indiv[0]);
		}	
	}
	else if (OptimizationFrame::s_behaviorMode == 1)
	{
		int round = 0;
		for (int  i = 0; i < m_indiv.size(); ++i) 
		{
			
			
			g_pMainFrame->BackToInitialStateAndRun(0);
			wxString str =  wxString::Format("Round %d",round);
			g_pOptimizationFrame->AppendLog(str,true);

			g_pMeshFrame->StartRecordDebug(0);
			OptimizationFrame::Evaluate(m_indiv[i]);			
			g_pMeshFrame->ReportPhysicsProperties(0);
			g_pMeshFrame->EndRecordDebug(0);

		/*	vtkSmartPointer<vtkPLYWriter> writer = vtkSmartPointer<vtkPLYWriter>::New();
			writer->SetInputData(g_pMeshFrame->m_smmPolydata[0]);
			std::stringstream ss;
			char currdir[MAX_PATH] = "";
			vtkDirectory::GetCurrentWorkingDirectory(currdir,MAX_PATH);
		
			ss << currdir << "\\check";
			vtkDirectory::MakeDirectory(ss.str().c_str());
			ss << "\\round" <<  round << ".ply\0";
			writer->SetFileName(ss.str().c_str());
			writer->Write();*/
			std::stringstream ss;
			char currdir[MAX_PATH] = "";
			vtkDirectory::GetCurrentWorkingDirectory(currdir,MAX_PATH);
			ss << currdir << "\\check";
			vtkDirectory::MakeDirectory(ss.str().c_str());
			ss << "\\round" <<  round << ".txt\0";			
			
			ofstream myfile;
			myfile.open (ss.str());
			myfile << g_pMeshFrame->m_physicsLog[0];
			myfile.close();
			g_pMeshFrame->m_physicsLog[0].clear();
			round++;

		}
	}
	MeshFrame::SetMeshFrameReady(0,false);
	MeshFrame::ReleaseMeshFrameID(0);
	return 0;
}


void SimulationDialog::m_radioBoxBehaviorModeOnRadioBox( wxCommandEvent& event )
{
	int sel = event.GetSelection();
	switch (sel)
	{
	case 0:
		
		break;
	case 1:
		
		break;
	default:
		break;
	}

	event.Skip();
}
