#pragma once
#include "SelfMovementMesh.h"
#include "frame.hpp"
#include "MeshFrame.h"
#include "OptimizationFrame.h"
//extern  CollisionObject	objectCO;
#define MAX_POPULATION 1000

extern SelfMovementMesh smm;
extern int g_numMeshFrame;
extern MyFrame *g_pMainFrame; 
extern OptimizationFrame *g_pOptimizationFrame; 
extern MeshFrame*  g_pMeshFrame;
extern vtkActor*  g_pRDActor[MAX_POPULATION];

