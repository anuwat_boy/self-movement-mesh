#pragma once
class MouseInteractorStylePP :  public vtkInteractorStyleTrackballCamera
{
public:
	vtkTypeMacro(MouseInteractorStylePP, vtkInteractorStyleTrackballCamera);
	static MouseInteractorStylePP* New();
    

	virtual void OnLeftButtonDown();
protected: 
		MouseInteractorStylePP();
protected:
	bool m_bPickingPoint;
};


 
    

