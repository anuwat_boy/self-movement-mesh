#pragma once
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "wxVTKRenderWindowInteractor.h"

#include "Properties.hpp"
class ImageRD;
class AbstractRD;

class EditRDFrame :	public wxFrame
{
public:
	EditRDFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
	virtual ~EditRDFrame(void);
	 
	
	void SetCurrentRDSystem(AbstractRD* system);
	void InitVTKPipeline(const Properties &render_settings,bool reset_camera);
private:
	void OnHello(wxCommandEvent& event);
	void OnExit(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);
	wxDECLARE_EVENT_TABLE();
protected:

	void InitializeRenderPane();
	void InitializeVTKPipeline_2D(vtkRenderer* pRenderer,const Properties& render_settings);
protected:
	// current system being simulated (in future we might want more than one)
    ImageRD *system;

	// VTK does the rendering
    wxVTKRenderWindowInteractor *pVTKWindow;

public:
	Properties render_settings;
};

