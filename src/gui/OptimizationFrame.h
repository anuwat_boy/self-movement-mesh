#pragma once
///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  5 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __OPTIMIZATIONFRAME_H__
#define __OPTIMIZATIONFRAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/textctrl.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/button.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include <wx/frame.h>
#include <wx/aui/aui.h>
//#include "SimulationDialog.h"

#ifdef WIN32
#include <Windows.h>
#endif
#include <eo>
#include <utils/eoParallel.h>
#include <eoInt.h>
#include <es.h>
#include <ga.h>
#include <eoScalarFitness.h>
#include "eoInitPermutationEx.h"
///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class OptimizationFrame
///////////////////////////////////////////////////////////////////////////////
//template <class EOT>
//void apply_parallel(eoUF<EOT&, void>& _proc, std::vector<EOT>& _pop);
wxString GetCurrentTimeString();


//typedef eoInt<double> MyIndivi;     // A bitstring with fitness double<double> eoIndi;
typedef eoReal<double> MyIndivi;     // A bitstring with fitness double<double> eoIndi;
/*
class MyIndivi : public Indivi 	
{
public:
	int id;
	int sleep_time;
	MyIndivi(): Indivi() { id = -1; sleep_time = -1;}
};
*/



class OptimizationThread;
class SimulationDialog;
class OptimizationFrame : public wxFrame 
{
	protected:
		SimulationDialog *m_sim_dialog;
		bool m_bIsRunning;
		void InitOptimizationSystem();
		
		void  EvalChromosome(unsigned int input);
		wxMutex		m_textlogmutex;
		OptimizationThread * m_thread;
		void OnThreadCompletion(wxCommandEvent& evt);
		static unsigned int s_SleepSecTime;
		wxTimer* m_refreshTimer; 
		
	public:
		eoPop<MyIndivi> 	 m_population;
		eoEvalFuncPtr<MyIndivi, double, const MyIndivi& > 	 m_evaluate;
		
		static double Evaluate(const MyIndivi & _chrom);
		static UINT64		s_PropagateIntervalTime;
		static int			s_behaviorMode;
		static std::vector<MyIndivi> s_BestRecord;
		static int UpdateToBestState(eoPop<MyIndivi> *pop);
	protected:
		wxTextCtrl* m_textCtrlLog;
		wxButton* m_buttonRun;
		wxButton* m_buttonStop;
		wxButton* m_buttonSimulate;
		wxSpinCtrl* m_spinCtrlEvalTime;
		wxSpinCtrl* m_spinCtrlChromosome;
		wxSpinCtrl* m_spinCtrlPopulation;
		wxSpinCtrl* m_spinCtrlMaxGen;
		wxSpinCtrl* m_spinCtrlSteadyGen;
		wxSpinCtrl* m_spinCtrlSelection;
		wxStaticText* m_staticTextEvalTime;
		wxStaticText* m_staticTextChromosome;
		wxStaticText* m_staticTextPopulation;
		wxStaticText* m_staticTextMaxGen;
		wxStaticText* m_staticTextSteadyGen;
		wxStaticText* m_staticTextSelection;
		wxTextCtrl* m_textCtrlCrossoverProb;
		wxTextCtrl* m_textCtrlMutationProb;
		wxTextCtrl* m_textCtrlMutationEPS;
		wxTextCtrl* m_textCtrlMutationSigma;
		wxStaticText* m_staticTextCrossProb;
		wxStaticText* m_staticTextMutProb;
		wxStaticText* m_staticTextMutEps;
		wxStaticText* m_staticTextMutSigma;
		wxTextCtrl* m_textCtrlHypercuteXover;
		wxTextCtrl* m_textCtrlSegmentXover;
		wxTextCtrl* m_textCtrlUniformMut;
		wxTextCtrl* m_textCtrlDetMut;
		wxTextCtrl* m_textCtrlNormalMut;
		wxStaticText* m_staticTextHypercuteXover;
		wxStaticText* m_staticTextSegXover;
		wxStaticText* m_staticTextUniformMut;
		wxStaticText* m_staticTextDetUniformMut;
		wxStaticText* m_staticTextNormalMut;
		wxSpinCtrl* m_spinCtrlPropagateIntervalTime;
		wxStaticText* m_staticTextPropagate;
		wxRadioBox* m_radioBoxBehaviorMode;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OptimizationFrameOnClose( wxCloseEvent& event );
		virtual void OptimizationFrameOnIdle( wxIdleEvent& event );
		virtual void m_buttonRunOnButtonClick( wxCommandEvent& event ); 
		virtual void m_buttonStopOnButtonClick( wxCommandEvent& event );
		virtual void m_buttonSimulateOnButtonClick( wxCommandEvent& event );
		virtual void m_radioBoxBehaviorModeOnRadioBox( wxCommandEvent& event );
		virtual void m_timerCount( wxTimerEvent& event );
		virtual void LogWindowKeyDown( wxKeyEvent &e); 
	public:
		
		OptimizationFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 800,520 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );		wxAuiManager m_mgr;
		
		virtual ~OptimizationFrame();
		void	AppendLog(const wxString &str, bool auto_new_line = true);
	

		DECLARE_EVENT_TABLE()
};

 
const int THREAD_EXIT_ID = 0xCD00;
const int wxEVT_COMMAND_THREAD_DONE = 0xDC00;

struct EA_SETTING
{
	int   population;
	int   chromosome;
	int   min_generation; //for EA
	int   max_generation;
	int   steady_generation; //for EA
	int   selection_size;
	

	float mutation_prob;  //for EA
	float crossover_prob; //for EA

	double mutation_eps; 
	double mutation_sigma; //for EA
	double uniform_mutation_rate; //for EA
	double det_mutation_rate; //for EA
	double normal_mutation_rate; //for EA
	
	
	double segment_crossover_rate; //for EA
	double hypercube_crossover_rate; //for EA

	double bound_min;
	double bound_max;
	
	UINT64  runningTime;
	UINT64  propagation_recreate_time;
	int		behavior_mode;
	float mutation_rate;  //for GA
	float crossover_rate; //for GA
	
	
};
class OptimizationThread : public wxThread
{
    wxFrame* m_parent;
	
public:
	OptimizationThread(wxFrame* parent , eoPop<MyIndivi>  &pop, eoEvalFuncPtr<MyIndivi> &eval , EA_SETTING &setting)
    {
        m_parent = parent;
		m_pop = &pop;
		m_eval = &eval;
		m_setting = setting;
    }
 
    virtual ExitCode Entry();
	eoPop<MyIndivi> *m_pop ;
	eoEvalFuncPtr<MyIndivi, double,const MyIndivi& > *m_eval ;
	EA_SETTING m_setting;

	
};
template<class EOT> 
class eoEasyEA_para: public eoEasyEA<EOT>
{
	public:
	eoEasyEA_para(
		UINT64 _running_time,
      eoContinue<EOT>& _continuator,
      eoEvalFunc<EOT>& _eval,
      eoSelect<EOT>& _select,
      eoTransform<EOT>& _transform,
      eoReplacement<EOT>& _replace
    ) : eoEasyEA(_continuator,
				_eval,
				_select,
				_transform,
				_replace),m_running_time(_running_time) {}

        
	 /// Apply a few generation of evolution to the population.
    virtual void operator()(eoPop<EOT>& _pop);
	UINT64 m_running_time;
	/*
    {
	if (isFirstCall)
	    {
		size_t total_capacity = _pop.capacity() + offspring.capacity();
		_pop.reserve(total_capacity);
		offspring.reserve(total_capacity);
		isFirstCall = false;
	    }

      eoPop<EOT> empty_pop;

      popEval(empty_pop, _pop); // A first eval of pop.

      do
        {
          try
            {
              unsigned pSize = _pop.size();
              offspring.clear(); // new offspring

              breed(_pop, offspring);

              popEval(_pop, offspring); // eval of parents + offspring if necessary

              replace(_pop, offspring); // after replace, the new pop. is in _pop

              if (pSize > _pop.size())
                throw std::runtime_error("Population shrinking!");
              else if (pSize < _pop.size())
                throw std::runtime_error("Population growing!");

            }
          catch (std::exception& e)
            {
              std::string s = e.what();
              s.append( " in eoEasyEA");
              throw std::runtime_error( s );
            }
        }
      while ( continuator( _pop ) );
    }*/
};

template <class EOT>
class eoSGA_Para: public eoSGA<EOT>
{
public:
	eoSGA_Para(
		UINT64 _running_time,
        eoSelectOne<EOT>& _select,
        eoQuadOp<EOT>& _cross, float _crate,
        eoMonOp<EOT>& _mutate, float _mrate,
        eoEvalFunc<EOT>& _eval,
        eoContinue<EOT>& _cont)
    : eoSGA ( _select,
			  _cross, _crate,
			  _mutate, _mrate,
			  _eval, _cont),m_running_time(_running_time) {}
public:
  void operator()(eoPop<EOT>& _pop);
  UINT64 m_running_time;
};



#endif //__OPTIMIZATIONFRAME_H__
