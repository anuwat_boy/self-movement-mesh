#include "stdafx.h"
#include "MouseInteractorStylePP.h"
#include <string>
#include <sstream>

vtkStandardNewMacro(MouseInteractorStylePP);
MouseInteractorStylePP::MouseInteractorStylePP()
{
	m_bPickingPoint = false;
}
void MouseInteractorStylePP::OnLeftButtonDown() 
{
	
	if (this->Interactor->GetControlKey() != 0)
	{
		
		vtkPointPicker* pointPicker = static_cast<vtkPointPicker *>(this->Interactor->GetPicker());
	
		
		this->Interactor->GetPicker()->Pick(this->Interactor->GetEventPosition()[0], 
											this->Interactor->GetEventPosition()[1], 
											0,  // always zero.
											this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
	
	}
	else
		vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}
