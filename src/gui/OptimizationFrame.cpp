#include "stdafx.h"
//#include <stdint.h>
///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  5 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////


#include <ppl.h>
#include "OptimizationFrame.h"
#include "SimulationDialog.h"
#include "GlobalVar.h"

#include "IDs.hpp"
#include <wx/datetime.h>
#include <wx/valnum.h>
#include <time.h>
#include <sstream>      // std::stringstream
#include <map>
BEGIN_EVENT_TABLE(OptimizationFrame, wxFrame)
EVT_COMMAND  (THREAD_EXIT_ID, wxEVT_COMMAND_THREAD_DONE, OptimizationFrame::OnThreadCompletion)
END_EVENT_TABLE()

struct chromosome_mesh
{
	MyIndivi indi;
	int mid;

	chromosome_mesh(MyIndivi i, int _mid)
	{
		indi = i;
		mid = _mid;
	}
};

unsigned int OptimizationFrame::s_SleepSecTime(30);
UINT64 OptimizationFrame::s_PropagateIntervalTime(0);
int		OptimizationFrame::s_behaviorMode(0);
std::vector<MyIndivi> OptimizationFrame::s_BestRecord;
static std::vector<chromosome_mesh> chromosome_mesh_mapping;
//static std::map<const MyIndivi, int> chromosome_mesh_mapping;

OptimizationFrame::OptimizationFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style ),
	m_bIsRunning(false),
	m_thread(NULL),
	m_sim_dialog(NULL),
	m_evaluate(  &OptimizationFrame::Evaluate )
{

	wxFloatingPointValidator<float> val;
	val.SetRange(0.0, 1.0);


	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	m_mgr.SetManagedWindow(this);
	m_mgr.SetFlags(wxAUI_MGR_DEFAULT);
	
	m_textCtrlLog = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	m_mgr.AddPane( m_textCtrlLog, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Resizable().FloatingSize( wxSize( -1,-1 ) ).DockFixed( false ).Floatable( false ).Row( 1 ).MinSize( wxSize( -1,250 ) ) );
	
	m_buttonRun = new wxButton( this, wxID_ANY, wxT("Run"), wxDefaultPosition, wxDefaultSize, 0 );
	m_mgr.AddPane( m_buttonRun, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Resizable().FloatingSize( wxSize( 104,65 ) ).DockFixed( false ).Floatable( false ) );
	
	m_buttonStop = new wxButton( this, wxID_ANY, wxT("Stop"), wxDefaultPosition, wxDefaultSize, 0 );
	m_buttonStop->Enable( false );
	m_mgr.AddPane( m_buttonStop, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Resizable().FloatingSize( wxSize( 104,65 ) ).Floatable( false ) );

	m_buttonSimulate = new wxButton( this, wxID_ANY, wxT("Simulate a chromosome"), wxDefaultPosition, wxDefaultSize, 0 );	
	m_mgr.AddPane( m_buttonSimulate, wxAuiPaneInfo() .Bottom() .CaptionVisible( false ).CloseButton( false ).PinButton( true ).Dock().Resizable().FloatingSize( wxSize( 104,65 ) ).Floatable( false ) );
	
	
	m_spinCtrlEvalTime = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 1000000000, 60000);
	m_mgr.AddPane( m_spinCtrlEvalTime, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).BestSize( wxSize( 100,25 ) ) );
	
	m_spinCtrlChromosome = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 1000, 4 );
	m_mgr.AddPane( m_spinCtrlChromosome, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).BestSize( wxSize( 100,25 ) ) );
	
	m_spinCtrlPopulation = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 2, 10000, 2 );
	m_mgr.AddPane( m_spinCtrlPopulation, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).BestSize( wxSize( 100,25 ) ) );
	
	m_spinCtrlMaxGen = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 2, 100000, 400 );
	m_mgr.AddPane( m_spinCtrlMaxGen, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).BestSize( wxSize( 100,25 ) ) );
	
	m_spinCtrlSteadyGen = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 10, 10000, 50 );
	m_mgr.AddPane( m_spinCtrlSteadyGen, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).BestSize( wxSize( 100,25 ) ) );
	
	m_spinCtrlSelection = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 2, 10000, 3 );
	m_mgr.AddPane( m_spinCtrlSelection, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).BestSize( wxSize( 100,25 ) ) );
	
	m_staticTextEvalTime = new wxStaticText( this, wxID_ANY, wxT("Evaluate Time"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextEvalTime->Wrap( -1 );
	m_mgr.AddPane( m_staticTextEvalTime, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextChromosome = new wxStaticText( this, wxID_ANY, wxT("Chromosome x2 (Texture Coords)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextChromosome->Wrap( -1 );
	m_mgr.AddPane( m_staticTextChromosome, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextPopulation = new wxStaticText( this, wxID_ANY, wxT("Population"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextPopulation->Wrap( -1 );
	m_mgr.AddPane( m_staticTextPopulation, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextMaxGen = new wxStaticText( this, wxID_ANY, wxT("Max Generation"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextMaxGen->Wrap( -1 );
	m_mgr.AddPane( m_staticTextMaxGen, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextSteadyGen = new wxStaticText( this, wxID_ANY, wxT("Steady Generation"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextSteadyGen->Wrap( -1 );
	m_mgr.AddPane( m_staticTextSteadyGen, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextSelection = new wxStaticText( this, wxID_ANY, wxT("Tournament Selection"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextSelection->Wrap( -1 );
	m_mgr.AddPane( m_staticTextSelection, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).BestSize( wxSize( -1,25 ) ) );
	
	m_textCtrlCrossoverProb = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0 ,val);
	m_mgr.AddPane( m_textCtrlCrossoverProb, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 2 ).BestSize( wxSize( 100,25 ) ) );
	
	m_textCtrlMutationProb = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0 ,val);
	m_mgr.AddPane( m_textCtrlMutationProb, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 2 ).BestSize( wxSize( 100,25 ) ) );
	
	m_textCtrlMutationEPS = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0 ,val);
	m_mgr.AddPane( m_textCtrlMutationEPS, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 2 ).BestSize( wxSize( 100,25 ) ) );
	
	m_textCtrlMutationSigma = new wxTextCtrl( this, wxID_ANY, wxT("0.25"), wxDefaultPosition, wxDefaultSize, 0 ,val);
	m_textCtrlMutationSigma->Enable( false );
	m_mgr.AddPane( m_textCtrlMutationSigma, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 2 ).BestSize( wxSize( 100,25 ) ) );
	
	m_staticTextCrossProb = new wxStaticText( this, wxID_ANY, wxT("Crossover Prob"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextCrossProb->Wrap( -1 );
	m_mgr.AddPane( m_staticTextCrossProb, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 3 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextMutProb = new wxStaticText( this, wxID_ANY, wxT("Mutation Prob"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextMutProb->Wrap( -1 );
	m_mgr.AddPane( m_staticTextMutProb, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 3 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextMutEps = new wxStaticText( this, wxID_ANY, wxT("Mutation Epsilon"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextMutEps->Wrap( -1 );	
	m_mgr.AddPane( m_staticTextMutEps, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 3 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextMutSigma = new wxStaticText( this, wxID_ANY, wxT("Mutation Sigma"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextMutSigma->Wrap( -1 );
	m_staticTextMutSigma->Enable( false );
	m_mgr.AddPane( m_staticTextMutSigma, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 3 ).BestSize( wxSize( -1,25 ) ) );
	
	m_textCtrlHypercuteXover = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0 ,val);
	m_mgr.AddPane( m_textCtrlHypercuteXover, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 4 ).BestSize( wxSize( 100,25 ) ) );
	
	m_textCtrlSegmentXover = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0,val );
	m_mgr.AddPane( m_textCtrlSegmentXover, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 4 ).BestSize( wxSize( 100,25 ) ) );
	
	m_textCtrlUniformMut = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0,val );
	m_mgr.AddPane( m_textCtrlUniformMut, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 4 ).BestSize( wxSize( 100,25 ) ) );
	
	m_textCtrlDetMut = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0 ,val);
	m_mgr.AddPane( m_textCtrlDetMut, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 4 ).BestSize( wxSize( 100,25 ) ) );
	
	m_textCtrlNormalMut = new wxTextCtrl( this, wxID_ANY, wxT("0.5"), wxDefaultPosition, wxDefaultSize, 0 ,val);
	m_textCtrlNormalMut->Enable( false );
	m_mgr.AddPane( m_textCtrlNormalMut, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 4 ).BestSize( wxSize( 100,25 ) ) );
	
	m_staticTextHypercuteXover = new wxStaticText( this, wxID_ANY, wxT("Hypercute Crossover"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextHypercuteXover->Wrap( -1 );
	m_mgr.AddPane( m_staticTextHypercuteXover, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 5 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextSegXover = new wxStaticText( this, wxID_ANY, wxT("Segment Crossover"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextSegXover->Wrap( -1 );
	m_mgr.AddPane( m_staticTextSegXover, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 5 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextUniformMut = new wxStaticText( this, wxID_ANY, wxT("Uniform Mutation"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextUniformMut->Wrap( -1 );
	m_mgr.AddPane( m_staticTextUniformMut, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 5 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextDetUniformMut = new wxStaticText( this, wxID_ANY, wxT("Det-Uniform Mutation"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextDetUniformMut->Wrap( -1 );
	m_mgr.AddPane( m_staticTextDetUniformMut, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 5 ).BestSize( wxSize( -1,25 ) ) );
	
	m_staticTextNormalMut = new wxStaticText( this, wxID_ANY, wxT("Normal Mutation"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextNormalMut->Wrap( -1 );
	m_staticTextNormalMut->Enable( false );
	m_mgr.AddPane( m_staticTextNormalMut, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 5 ).BestSize( wxSize( -1,25 ) ) );
	
	m_spinCtrlPropagateIntervalTime = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 1000000, 20000 );
	m_mgr.AddPane( m_spinCtrlPropagateIntervalTime, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 0 ).BestSize( wxSize( 100,25 ) ) );
	
	m_staticTextPropagate = new wxStaticText( this, wxID_ANY, wxT("Propagate Interval Time"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextPropagate->Wrap( -1 );
	m_mgr.AddPane( m_staticTextPropagate, wxAuiPaneInfo() .Right() .CaptionVisible( false ).CloseButton( false ).Dock().Fixed().DockFixed( false ).Row( 1 ).Position( 0 ).BestSize( wxSize( -1,25 ) ) );
	
	wxString m_radioBoxBehaviorModeChoices[] = { wxT("Restart mode"), wxT("Update mode") };
	int m_radioBoxBehaviorModeNChoices = sizeof( m_radioBoxBehaviorModeChoices ) / sizeof( wxString );
	m_radioBoxBehaviorMode = new wxRadioBox( this, wxID_ANY, wxT("behavior mode"), wxDefaultPosition, wxDefaultSize, m_radioBoxBehaviorModeNChoices, m_radioBoxBehaviorModeChoices, 1, wxRA_SPECIFY_COLS );
	m_radioBoxBehaviorMode->SetSelection( 1 );
	m_mgr.AddPane( m_radioBoxBehaviorMode, wxAuiPaneInfo() .Left() .CaptionVisible( false ).CloseButton( false ).Movable( false ).Dock().Fixed().DockFixed( false ).Floatable( false ) );

	m_mgr.Update();
	this->Centre( wxBOTH );

	if (m_radioBoxBehaviorMode->GetSelection() == 1)
	{
		m_staticTextHypercuteXover->Enable(false);
		m_staticTextSegXover->Enable(false);
		m_staticTextUniformMut->Enable(false);	
		m_staticTextDetUniformMut->Enable(false);
		m_textCtrlHypercuteXover->Enable(false);
		m_textCtrlSegmentXover->Enable(false);
		m_textCtrlUniformMut->Enable(false);
		m_textCtrlDetMut->Enable(false);
	}
	m_refreshTimer = new wxTimer(this);	
	m_refreshTimer->Connect(wxEVT_TIMER,wxTimerEventHandler( OptimizationFrame::m_timerCount), NULL,this);
	m_refreshTimer->Start(500);


	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( OptimizationFrame::OptimizationFrameOnClose ) );
	this->Connect( wxEVT_IDLE, wxIdleEventHandler( OptimizationFrame::OptimizationFrameOnIdle ) );
	m_buttonRun->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OptimizationFrame::m_buttonRunOnButtonClick ), NULL, this );
	m_buttonStop->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OptimizationFrame::m_buttonStopOnButtonClick ), NULL, this );
	m_buttonSimulate->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OptimizationFrame::m_buttonSimulateOnButtonClick ), NULL, this );
	m_radioBoxBehaviorMode->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( OptimizationFrame::m_radioBoxBehaviorModeOnRadioBox ), NULL, this );
	m_textCtrlLog->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(OptimizationFrame::LogWindowKeyDown), NULL, this);
	
}

OptimizationFrame::~OptimizationFrame()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( OptimizationFrame::OptimizationFrameOnClose ) );
	this->Disconnect( wxEVT_IDLE, wxIdleEventHandler( OptimizationFrame::OptimizationFrameOnIdle ) );
	m_buttonRun->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OptimizationFrame::m_buttonRunOnButtonClick ), NULL, this );
	m_buttonStop->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OptimizationFrame::m_buttonStopOnButtonClick ), NULL, this );
	m_buttonSimulate->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OptimizationFrame::m_buttonSimulateOnButtonClick ), NULL, this );
	m_radioBoxBehaviorMode->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( OptimizationFrame::m_radioBoxBehaviorModeOnRadioBox ), NULL, this );	
	m_refreshTimer->Disconnect(wxEVT_TIMER,wxTimerEventHandler( OptimizationFrame::m_timerCount), NULL,this);
	m_textCtrlLog->Disconnect(wxEVT_KEY_DOWN, wxKeyEventHandler(OptimizationFrame::LogWindowKeyDown), NULL, this);
	if (m_sim_dialog)
	{
		m_sim_dialog->Close(true);
	}
	m_mgr.UnInit();
	
}

void OptimizationFrame::OptimizationFrameOnClose( wxCloseEvent& event )
{ 
	if (m_thread)
	{
		m_thread->Kill();
		m_thread = NULL;
	}		
	event.Skip(); 
}

void OptimizationFrame::LogWindowKeyDown( wxKeyEvent &e)
{
	switch(e.GetKeyCode())
   {
       case 'a':
       case 'A':
              if(e.ControlDown())
              {
			
                   if (e.GetEventObject()->GetClassInfo()->IsKindOf(wxCLASSINFO(wxTextCtrl)))
				  {
					  wxTextCtrl* aTextCtrl = reinterpret_cast<wxTextCtrl*>( e.GetEventObject());
					  aTextCtrl->SetSelection(-1,-1);
				  }
              }
              else
                  e.Skip();
              break;

       default:
              e.Skip();
              break;
   }
}

void OptimizationFrame::AppendLog(const wxString &str, bool auto_new_line)
{
	wxMutexLocker lock(m_textlogmutex);
	if (auto_new_line)
	{
		wxString _str(str);
		_str << "\n";
		m_textCtrlLog->AppendText(_str);
	}
	else
	{
		m_textCtrlLog->AppendText(str);
	}
	//this->Refresh(false);
}

void OptimizationFrame::m_radioBoxBehaviorModeOnRadioBox( wxCommandEvent& event )
{
	int sel = event.GetSelection();
	switch (sel)
	{
	case 0:
		//restart mode
		m_staticTextHypercuteXover->Enable(true);
		m_staticTextSegXover->Enable(true);
		m_staticTextUniformMut->Enable(true);	
		m_staticTextDetUniformMut->Enable(true);
		m_textCtrlHypercuteXover->Enable(true);	
		m_textCtrlSegmentXover->Enable(true);	
		m_textCtrlUniformMut->Enable(true);	
		m_textCtrlDetMut->Enable(true);	

		break;
	case 1:
		//update mode
		m_staticTextHypercuteXover->Enable(false);
		m_staticTextSegXover->Enable(false);
		m_staticTextUniformMut->Enable(false);	
		m_staticTextDetUniformMut->Enable(false);
		m_textCtrlHypercuteXover->Enable(false);
		m_textCtrlSegmentXover->Enable(false);
		m_textCtrlUniformMut->Enable(false);
		m_textCtrlDetMut->Enable(false);
		break;
	default:
		break;
	}

	event.Skip();
}


void OptimizationFrame::InitOptimizationSystem()
{
	unsigned int SLEEP_TIME= this->m_spinCtrlEvalTime->GetValue();
	s_SleepSecTime = SLEEP_TIME;
	unsigned int POP_SIZE  = this->m_spinCtrlPopulation->GetValue();
	unsigned int VEC_SIZE  = this->m_spinCtrlChromosome->GetValue()*2;
	int num_possibleControlVertice = smm.original_mesh->GetNumberOfPoints();
	char *argv_paraon[] = {"READY.EXE","--parallelize-loop=1","--parallelize-dynamic=1","--parallelize-nthreads=0"};
	char *argv_paraoff[] = {"READY.EXE","--parallelize-loop=0","--parallelize-dynamic=0"};
	unsigned int argc = 4; 
	eoParser parser_on(argc,argv_paraon);
	
	make_parallel(parser_on);
	bool isParallelEnable = eo::parallel.isEnabled();
	int numThread = eo::parallel.nthreads();
	bool isDynamic = eo::parallel.isDynamic();
	eoRealVectorBounds bounded(VEC_SIZE,0.0,1.0);
	eoRealInitBounded<MyIndivi>* initGene = new eoRealInitBounded<MyIndivi>(bounded);
	m_population = eoPop<MyIndivi>(POP_SIZE,*initGene);
	/*
	for (int i = 0; i < g_numMeshFrame;i++)
	{
		//0.493558 0.20645 0.215951 0.0645874 0.452747 0.617972 0.0297065 0.622048
	m_population.at(i)[0] = 0.493558;
	m_population.at(i)[1] = 0.20645;
	m_population.at(i)[2] = 0.215951;
	m_population.at(i)[3] = 0.0645874;
	m_population.at(i)[4] = 0.452747;
	m_population.at(i)[5] = 0.617972;
	m_population.at(i)[6] = 0.0297065;
	m_population.at(i)[7] = 0.622048;
	}
	*/
	
	EA_SETTING setting ;
	double val; 
	setting.runningTime = SLEEP_TIME;
	setting.propagation_recreate_time = this->m_spinCtrlPropagateIntervalTime->GetValue();
	s_PropagateIntervalTime = setting.propagation_recreate_time;
	setting.population = POP_SIZE;
	setting.chromosome = VEC_SIZE;
	setting.min_generation = 10; //for EA
	setting.max_generation = this->m_spinCtrlMaxGen->GetValue();
	setting.steady_generation = this->m_spinCtrlSteadyGen->GetValue(); //for EA
	setting.selection_size = this->m_spinCtrlSelection->GetValue();
	
	this->m_textCtrlMutationEPS->GetValue().ToDouble(&val); 
	setting.mutation_eps = (double )val; 
	this->m_textCtrlMutationSigma->GetValue().ToDouble(&val); 
	setting.mutation_sigma = (double )val;
	this->m_textCtrlUniformMut->GetValue().ToDouble(&val); 
	setting.uniform_mutation_rate = (double )val; //for EA
	this->m_textCtrlDetMut->GetValue().ToDouble(&val); 
	setting.det_mutation_rate = (double )val; //for EA
	this->m_textCtrlNormalMut->GetValue().ToDouble(&val); 
	setting.normal_mutation_rate = (double )val; //for EA 
	
	this->m_textCtrlSegmentXover->GetValue().ToDouble(&val); 
	setting.segment_crossover_rate = (double )val; //for EA

	this->m_textCtrlHypercuteXover->GetValue().ToDouble(&val); 
	setting.hypercube_crossover_rate = (double )val; //for EA
	
	this->m_textCtrlMutationProb->GetValue().ToDouble(&val); 
	setting.mutation_prob = (float )val;  //for EA

	this->m_textCtrlCrossoverProb->GetValue().ToDouble(&val); 
	setting.crossover_prob = (float )val; //for EA

	setting.bound_min = 0;
	setting.bound_max = 1.0;//num_possibleControlVertice-1;
	
	setting.behavior_mode = this->m_radioBoxBehaviorMode->GetSelection();
	

	std::stringstream ss;
	ss << "Evaluation Time:" << s_SleepSecTime << endl; 
	ss << "Propagation Interval:" << s_PropagateIntervalTime ;	
	g_pOptimizationFrame->AppendLog(ss.str(),true);

	
	if (m_thread == NULL)
	{
		m_thread = new OptimizationThread(this,m_population,m_evaluate,setting);
	}
	m_thread->Create();
	m_thread->Run();


	
	
	
}


void OptimizationFrame::OptimizationFrameOnIdle( wxIdleEvent& event ) 
{ 


	if (m_bIsRunning)
	{
		/*
		bool ismain = wxThread::IsMain();

		if (m_thread == NULL)
		{
			m_thread = new OptimizationThread(this,m_population,m_evaluate);
			m_thread->Create();
			m_thread->Run();
		}
		*/
	}
	event.Skip(); 

}

void OptimizationFrame::m_buttonRunOnButtonClick( wxCommandEvent& event ) 
{ 
	//to do ... make to single frame
	MeshFrame::PrepareMeshFrames(this->m_spinCtrlPopulation->GetValue());
	g_pMainFrame->InitMultiRDSystems();
	InitOptimizationSystem();
	
	
	
	m_bIsRunning = true;

	m_buttonRun->Enable( false );
	m_buttonStop->Enable( true );
	event.Skip(); 
}

void OptimizationFrame::m_buttonStopOnButtonClick( wxCommandEvent& event ) 
{ 
	m_bIsRunning = false;
	m_buttonRun->Enable( true );
	m_buttonStop->Enable( false );

	if (m_thread != NULL)
	{
		m_thread->Kill();
		//m_thread->Delete();
		//delete m_thread;
		m_thread = NULL;
	}
	event.Skip(); 
}


void OptimizationFrame::m_buttonSimulateOnButtonClick( wxCommandEvent& event )
{
	if (m_sim_dialog == NULL)
		m_sim_dialog = new SimulationDialog(this,-1,"Simulation");
	m_sim_dialog->Show();
	m_buttonRun->Enable(false);

	event.Skip(); 
}
double  EvalChromosome(const MyIndivi & _chrom)
{
	return 0;
}


void OptimizationFrame::m_timerCount( wxTimerEvent& _event )
{
	this->Refresh();
}


double OptimizationFrame::Evaluate(const MyIndivi & _chrom)
{
	
	//to do: support new multi mesh in one frame
	double ball_pos[3] = {-DBL_MAX};
	int revb = -1;
	int reve = -1;
	int mid = -1;

	{
		//std::stringstream ss;
		//ss << "[" << _chrom << "]" << "tried to get mid" ;
		//g_pOptimizationFrame->AppendLog(ss.str(),true);
		mid = MeshFrame::ReserveMeshFrameID();		
		#pragma omp critical 
		chromosome_mesh_mapping.push_back(chromosome_mesh(_chrom,mid));
		//chromosome_mesh_mapping.insert( std::pair<const MyIndivi, int>(_chrom,mid));
	}
	std::stringstream ss;
	ss.precision(10);
	ss << "MID:" << mid << " Evaluate [" << _chrom << "]" ;
	
	//std::vector<int> controlVertice(_chrom.begin(),_chrom.end());	
	if (OptimizationFrame::s_behaviorMode == 0)//restart mode
	{		
		g_pMeshFrame->ResetToInitialState(mid, false);

	}

	
	revb = g_pMeshFrame->GetObjectivePosition(mid,&ball_pos[0]);	
	g_pMeshFrame->ResetRevisionUpdate(mid);
	//ss << " rev diff = " << rev ;
	g_pOptimizationFrame->AppendLog(ss.str(),true);
	//bool validcv = g_pMeshFrame->SetControlVertices(mid,controlVertice);

	//create dot at rd plane
	//for (int i = 0; i < _chrom.size() ; )


	
		//g_pMainFrame
	MyIndivi::const_iterator itb = _chrom.begin();
	MyIndivi::const_iterator ite = _chrom.end();
	std::vector<double> texCoord(itb,ite);	
	UINT64 propagation_count = 0;
	if (1)
	{
		//concurrency::wait(s_SleepSecTime * 1000);
		
		MeshFrame::SetMeshFrameReady(mid,true);	
		UINT64 currenttime = 0;
		
		while (!g_pMainFrame->IsReadyForGetFitness(mid))
		{
			currenttime = g_pMainFrame->GetcurrentRunningTime(mid);
			if (currenttime >= s_PropagateIntervalTime * propagation_count)
			{
				//std::stringstream ss2;
				//ss2 << "MID:" << mid << " Create dots at time:" << g_pMainFrame->GetcurrentRunningTime(mid);
				//g_pOptimizationFrame->AppendLog(ss2.str(),true);
				g_pMainFrame->CreateDotsInRDSystems(mid,texCoord.size()/2, &texCoord[0],1.0);
				propagation_count++;

			}
			else
			{
#ifdef _OPENMP
				//#pragma omp taskyield
#else

				wxThread::Yield();
#endif
			}
			
		}
		/*
		while( g_pMainFrame->m_revisionUpdate[mid] != g_pMeshFrame->GetRevisionUpdate(mid))
		{
			wxThread::Yield();
		}
		*/
		
		reve = g_pMeshFrame->GetObjectivePosition(mid,&ball_pos[0]);		
		UINT64 time_past = g_pMainFrame->GetcurrentRunningTime(mid);
		int revv = g_pMeshFrame->GetRevisionUpdate(mid);
		MeshFrame::ReleaseMeshFrameID(mid);
					
		
		//g_pMeshFrame[mid]->ResetToInitialState(true);		
		//MeshFrame::SetMeshFrameReady(mid,false);		
		
		std::stringstream ss2;	
		ss2 << "MID:" << mid << " Fitness Result = " <<  ball_pos[0]  << "  ,time:" << time_past << " ,rev:" << revv << " ,rev main:" << g_pMainFrame->m_revisionUpdate[mid];
		g_pOptimizationFrame->AppendLog(ss2.str(),true);
		return ball_pos[0];
	}
	else
	{
		g_pMeshFrame->ResetToInitialState(mid,true);
		std::stringstream ss2;
		ss2 << "MID:" << mid << " Fitness Result = INVALID Control Points";
		g_pOptimizationFrame->AppendLog(ss2.str(),true);
		MeshFrame::ReleaseMeshFrameID(mid);
		return -DBL_MAX;
	}
	
}

void OptimizationFrame::OnThreadCompletion(wxCommandEvent& evt)
{
	m_thread = NULL;

	wxCommandEvent _event( wxEVT_MENU, ID::RunStop);        
	g_pMainFrame->GetEventHandler()->AddPendingEvent( _event );
}

wxString GetCurrentTimeString()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [256];

	time (&rawtime);
	timeinfo = localtime (&rawtime);

	strftime (buffer,256,"%Y-%m-%d %H:%M:%S",timeinfo);
	wxString str(buffer);
	return str;
}

int OptimizationFrame::UpdateToBestState(eoPop<MyIndivi> *pop)
{
	static int round = 0;

	const MyIndivi &best_Ele = pop->best_element();
	int best_index = -1;
	for (int i  = 0 ; i < chromosome_mesh_mapping.size(); i++)
	{
		if (chromosome_mesh_mapping[i].indi == best_Ele)
		{
			best_index = chromosome_mesh_mapping[i].mid;
			break;
		}
	}
	
	
	 
	if (best_index >= 0 && best_index < g_numMeshFrame)
	{
		g_pOptimizationFrame->AppendLog(wxString("Clone with stat:"),true);
		g_pMeshFrame->ReportPhysicsProperties(best_index);
		for (int i = 0 ; i < g_numMeshFrame; i++)
		{
			if (i != best_index)
			{
				g_pMeshFrame->Clone(best_index,i);
			}
		}
		//g_pMeshFrame->Clone(best_index,best_index);
		
		//g_pMeshFrame->Clone((best_index+1)%g_numMeshFrame,best_index);
		wxString logstr;
		logstr<< "Clone to mid:" << best_index;
		g_pOptimizationFrame->AppendLog(logstr,true); 	

		
		std::stringstream ss;
		char currdir[MAX_PATH] = "";
		vtkDirectory::GetCurrentWorkingDirectory(currdir,MAX_PATH);
		
		ss << currdir << "\\experiment";
		vtkDirectory::MakeDirectory(ss.str().c_str());
		ss << "\\round" <<  round << ".txt\0";

		ofstream myfile;
		myfile.open (ss.str());
		myfile << g_pMeshFrame->m_physicsLog[best_index];
		myfile.close();
		for (int i = 0 ; i < g_numMeshFrame; i++)
		{
			g_pMeshFrame->m_physicsLog[i].clear();
		}
		
		/*vtkSmartPointer<vtkPLYWriter> writer = vtkSmartPointer<vtkPLYWriter>::New();
		writer->SetInputData(g_pMeshFrame->m_smmPolydata[best_index]);
		std::stringstream ss;
		char currdir[MAX_PATH] = "";
		vtkDirectory::GetCurrentWorkingDirectory(currdir,MAX_PATH);
		
		ss << currdir << "\\experiment";
		vtkDirectory::MakeDirectory(ss.str().c_str());
		ss << "\\round" <<  round << ".ply\0";
		writer->SetFileName(ss.str().c_str());
		writer->Write();*/
		round++;
		return best_index;
	}
	
	else
		return -1;
}



wxThread::ExitCode OptimizationThread::Entry()
{

	
	const unsigned int T_SIZE = m_setting.selection_size; // size for tournament selection
	const unsigned int VEC_SIZE = m_setting.chromosome; // Number of object variables in genotypes
	const unsigned int POP_SIZE = m_setting.population; // Size of population
	const unsigned int MAX_GEN = m_setting.max_generation; // Maximum number of generation before STOP
	const unsigned int MIN_GEN = m_setting.min_generation;  // Minimum number of generation before ...
	const unsigned int STEADY_GEN = m_setting.steady_generation; // stop after STEADY_GEN gen. without improvelent
	const float P_CROSS = m_setting.crossover_prob; // Crossover probability
	const float P_MUT = m_setting.mutation_prob; // mutation probability
	const double EPSILON = m_setting.mutation_eps; // range for real uniform mutation
	double SIGMA = m_setting.mutation_sigma;        // std dev. for normal mutation
	// some parameters for chosing among different operators
	const double hypercubeRate = m_setting.hypercube_crossover_rate;    // relative weight for hypercube Xover
	const double segmentRate = m_setting.segment_crossover_rate; // relative weight for segment Xover
	const double uniformMutRate = m_setting.uniform_mutation_rate; // relative weight for uniform mutation
	const double detMutRate = m_setting.det_mutation_rate;     // relative weight for det-uniform mutation
	const double normalMutRate = m_setting.normal_mutation_rate;  // relative weight for normal mutation
	
	double runningTime = m_setting.runningTime;
	OptimizationFrame::s_behaviorMode = m_setting.behavior_mode;
	eoPop<MyIndivi> &pop = *m_pop;
	if (OptimizationFrame::s_behaviorMode == 0)
	{
		//for EA codes
		eoEvalFuncPtr<MyIndivi > &eval = *m_eval ;
		
		((OptimizationFrame *)m_parent)->AppendLog(GetCurrentTimeString(),true);
		((OptimizationFrame *)m_parent)->AppendLog("initial...start...",true);	
		g_pMainFrame->BackToInitialStateAndRun(runningTime,true);
		apply(eval,pop);
		((OptimizationFrame *)m_parent)->AppendLog("initial...end...",true); 
		/*
		if (m_setting.behavior_mode == 1)//update mode
		{
			//replace all meshes (physics and shape) with best state

			OptimizationFrame::UpdateToBestState(m_pop);
		}
		chromosome_mesh_mapping.clear();
		*/
		// and evaluate it in one loop
		// ENGINE
	  /////////////////////////////////////
	  // selection and replacement
	  ////////////////////////////////////
		// SELECT
		// The robust tournament selection
		eoRealVectorBounds bound(VEC_SIZE, m_setting.bound_min , m_setting.bound_max);


		eoDetTournamentSelect<MyIndivi> selectOne(T_SIZE);
		// is now encapsulated in a eoSelectPerc (entage)
		eoSelectPerc<MyIndivi> select(selectOne);// by default rate==1

		// REPLACE
		// And we now have the full slection/replacement - though with
		// no replacement (== generational replacement) at the moment :-)
		eoGenerationalReplacement<MyIndivi> tmp_replace;
		eoWeakElitistReplacement<MyIndivi> replace(tmp_replace);

		// OPERATORS
		//////////////////////////////////////
		// The variation operators
		//////////////////////////////////////
		// CROSSOVER
		// uniform chooce on segment made by the parents
		eoSegmentCrossover<MyIndivi> xoverS(bound);
		// uniform choice in hypercube built by the parents
		eoHypercubeCrossover<MyIndivi> xoverA(bound);
		// Combine them with relative weights
		eoPropCombinedQuadOp<MyIndivi> xover(xoverS, segmentRate);
		xover.add(xoverA, hypercubeRate);

		// MUTATION
		// offspring(i) uniformly chosen in [parent(i)-epsilon, parent(i)+epsilon]
		eoUniformMutation<MyIndivi>  mutationU(bound,EPSILON);
		// k (=1) coordinates of parents are uniformly modified
		eoDetUniformMutation<MyIndivi>  mutationD(bound,EPSILON);
		// all coordinates of parents are normally modified (stDev SIGMA)
		//eoNormalMutation<MyIndivi>  mutationN(SIGMA);
		// Combine them with relative weights
		eoPropCombinedMonOp<MyIndivi> mutation(mutationU, uniformMutRate);
		mutation.add(mutationD, detMutRate);
		//mutation.add(mutationN, normalMutRate, true);

		// STOP
		// CHECKPOINT
		//////////////////////////////////////
		// termination conditions: use more than one
		/////////////////////////////////////
		// stop after MAX_GEN generations
		eoGenContinue<MyIndivi> genCont(MAX_GEN);
		// do MIN_GEN gen., then stop after STEADY_GEN gen. without improvement
		eoSteadyFitContinue<MyIndivi> steadyCont(MIN_GEN, STEADY_GEN);
		// stop when fitness reaches a target (here VEC_SIZE)
		eoFitContinue<MyIndivi> fitCont(5.0);
		// do stop when one of the above says so
		eoCombinedContinue<MyIndivi> continuator(genCont);
		continuator.add(steadyCont);
		continuator.add(fitCont);

		// The operators are  encapsulated into an eoTRansform object
		eoSGATransform<MyIndivi> transform(xover, P_CROSS, mutation, P_MUT);

		// GENERATION
		/////////////////////////////////////////
		// the algorithm
		////////////////////////////////////////

		// Easy EA requires
		// selection, transformation, eval, replacement, and stopping criterion
		eoEasyEA_para<MyIndivi> gga(runningTime,continuator, eval, select, transform, replace);

		// Apply algo to pop - that's it!
	
		gga(pop);



		pop.sort();
		//display result
		{
			std::stringstream ss;
			ss<< "==RESULT==" << "\n";
			for (int i = 0 ; i < POP_SIZE; i++)
			{
				ss << pop[i] << std::endl;
			}
			wxString logstr = GetCurrentTimeString();
			logstr<< "\n" << ss.str();
			((OptimizationFrame *)m_parent)->AppendLog(logstr,true); 	
		}

	}
	else
	{
	
		OptimizationFrame::s_BestRecord.clear();
	
		eoEvalFuncPtr<MyIndivi > &eval = *m_eval ;
		eoPop<MyIndivi> &pop = *m_pop;
		((OptimizationFrame *)m_parent)->AppendLog(GetCurrentTimeString(),true);
		((OptimizationFrame *)m_parent)->AppendLog("initial...start...",true);	
		g_pMainFrame->BackToInitialStateAndRun(runningTime,true);

		
		//for (int i = 0 ; i < g_numMeshFrame;i++)
			//g_pMeshFrame->StartRecordDebug(i);
		apply(eval,pop);
		/*for (int i = 0 ; i < g_numMeshFrame;i++)
			g_pMeshFrame->EndRecordDebug(i);*/
		((OptimizationFrame *)m_parent)->AppendLog("initial...end...",true); 
	
		/*
		{
			std::stringstream ss;
			for (int i = 0 ; i < POP_SIZE; i++)
			{
				ss << _pop[i] << std::endl;
			}
			wxString logstr = GetCurrentTimeString();
			logstr<< "\n" << ss.str();
			((OptimizationFrame *)m_parent)->AppendLog(logstr,true); 	
		}
		*/
		// ENGINE
		/////////////////////////////////////
		// selection and replacement
		////////////////////////////////////
		// SELECT
		// The robust tournament selection
		eoDetTournamentSelect<MyIndivi> select(T_SIZE);       // T_SIZE in [2,POP_SIZE]

		// REPLACE
		// eoSGA uses generational replacement by default
		// so no replacement procedure has to be given

		eoRealVectorBounds bound(VEC_SIZE, m_setting.bound_min , m_setting.bound_max);
		// OPERATORS
		//////////////////////////////////////
		// The variation operators
		//////////////////////////////////////
		// CROSSOVER
		// offspring(i) is a linear combination of parent(i)
	
		eoSegmentCrossover<MyIndivi> xover(bound);
		// MUTATION
		// offspring(i) uniformly chosen in [parent(i)-epsilon, parent(i)+epsilon]
	
		eoUniformMutation<MyIndivi>  mutation(bound,EPSILON);

		// STOP
		// CHECKPOINT
		//////////////////////////////////////
		// termination condition
		/////////////////////////////////////
		// stop after MAX_GEN generations
		eoGenContinue<MyIndivi> genCont(MAX_GEN);
		// do MIN_GEN gen., then stop after STEADY_GEN gen. without improvement
		eoSteadyFitContinue<MyIndivi> steadyCont(MIN_GEN, STEADY_GEN);
		// stop when fitness reaches a target (here VEC_SIZE)
		eoFitContinue<MyIndivi> fitCont(5.0);
		// do stop when one of the above says so
		eoCombinedContinue<MyIndivi> continuator(genCont);
		continuator.add(fitCont);
		//continuator.add(fitCont);
	
		eoSGA_Para<MyIndivi> ga(runningTime,select, xover, P_CROSS, mutation, P_MUT,
			eval, continuator);

		ga(pop);

		OptimizationFrame::s_BestRecord.push_back(pop.best_element());
		
		//display result
		{
			std::stringstream ss;
			std::stringstream sshex;
			ss.precision(10);
			ss<< "==RESULT==" << "\n";
			for (int i = 0 ; i < OptimizationFrame::s_BestRecord.size(); i++)
			{
				ss << OptimizationFrame::s_BestRecord[i] << std::endl;
				for (int c = 0 ; c < OptimizationFrame::s_BestRecord[i].size(); c++)
					sshex << wxString::Format("%llX ",OptimizationFrame::s_BestRecord[i][c]);
				sshex << std::endl;
			}
			wxString logstr = GetCurrentTimeString();
			logstr << "\n" << ss.str();
			logstr << "\n" << sshex.str();
			
			((OptimizationFrame *)m_parent)->AppendLog(logstr,true); 	
		}

	}
	
	
	 /*
	
	concurrency::SchedulerPolicy oldpolicy = concurrency::CurrentScheduler::GetPolicy();	
	concurrency::SchedulerPolicy policy(oldpolicy);
	if (policy.GetPolicyValue(concurrency::MaxConcurrency) > 10)
		policy.SetConcurrencyLimits(1,10);	
	
	concurrency::CurrentScheduler::Create(policy);
	concurrency::parallel_for(0u, POP_SIZE, [&_eval,&_pop] (unsigned int i)  	
	{		
		_eval((MyIndivi &)_pop[i]);		
	});
	concurrency::CurrentScheduler::Detach();
	*/
	 /*
	std::stringstream ss;
	for (int i = 0 ; i < POP_SIZE; i++)
	{
		ss << _pop[i] << std::endl;
	}
	wxString logstr = GetCurrentTimeString();
	logstr<< "\n" << ss.str();
	((OptimizationFrame *)m_parent)->AppendLog(logstr,true); 	
	*/

	wxCommandEvent _event( wxEVT_COMMAND_THREAD_DONE, THREAD_EXIT_ID);        
    m_parent->GetEventHandler()->AddPendingEvent( _event );
    return 0;
}

template <class EOT>
void eoSGA_Para<EOT>::operator()(eoPop<EOT>& _pop)
{
	eoPop<EOT> offspring;
	int round = 1;
    do
      {

		
		if (OptimizationFrame::s_behaviorMode == 1)//update mode
		{
			//replace all meshes (physics and shape) with best state
			//g_pOptimizationFrame->AppendLog("aaaaa",true);	
			//wxMutexLocker lock( MeshFrame::s_UpdateMutex);
			//g_pOptimizationFrame->AppendLog("bbbbb",true);	
			OptimizationFrame::s_BestRecord.push_back(_pop.best_element());
			int best_idx = OptimizationFrame::UpdateToBestState(&_pop);
			//g_pOptimizationFrame->AppendLog("ccccc",true);	
			

		}
		chromosome_mesh_mapping.clear();

        select(_pop, offspring);
        unsigned i;

        for (i=0; i<_pop.size()/2; i++)
          {
            if ( rng.flip(crossoverRate) )
            {
                    // this crossover generates 2 offspring from two parents
                    if (cross(offspring[2*i], offspring[2*i+1]))
        {
          offspring[2*i].invalidate();
          offspring[2*i+1].invalidate();
        }
      }
          }

        for (i=0; i < offspring.size(); i++)
          {
            if (rng.flip(mutationRate) )
            {
                     if (mutate(offspring[i]))
          offspring[i].invalidate();
            }
          }

        _pop.swap(offspring);
			
		//add by Anuwat  ... to force system to test all of them 
		if (OptimizationFrame::s_behaviorMode == 1)
		{
			for (i=0; i < _pop.size(); i++)
			{
				_pop[i].invalidate();
			}
		}
		g_pMainFrame->BackToInitialStateAndRun(0);
		wxString str =  wxString::Format("Round %d",round++);
		g_pOptimizationFrame->AppendLog(str,true);

        apply<EOT>(eval, _pop);
		
      } while (cont(_pop));
}

#if 0
template <class EOT>
void apply_parallel(eoUF<EOT&, void>& _proc, std::vector<EOT>& _pop)
{
    unsigned int _pop_size = _pop.size();

#ifdef _OPENMP

    double t1 = 0;

    if ( eo::parallel.enableResults() )
    {
        t1 = omp_get_wtime();
    }

    if (!eo::parallel.isDynamic())
    {
#pragma omp parallel for if(eo::parallel.isEnabled()) //default(none) shared(_proc, _pop, size)
#ifdef _MSC_VER
        //Visual Studio supports only OpenMP version 2.0 in which
        //an index variable must be of a signed integral type
        for (long long i = 0; i < size; ++i) { _proc(_pop[i]); }
#else // _MSC_VER
        for (size_t i = 0; i < size; ++i) { _proc(_pop[i]); }
#endif
    }
    else
    {
#pragma omp parallel for schedule(dynamic) if(eo::parallel.isEnabled())
#ifdef _MSC_VER
        //Visual Studio supports only OpenMP version 2.0 in which
        //an index variable must be of a signed integral type
        for (long long i = 0; i < size; ++i) { _proc(_pop[i]); }
#else // _MSC_VER
        //doesnot work with gcc 4.1.2
        //default(none) shared(_proc, _pop, size)
        for (size_t i = 0; i < size; ++i) { _proc(_pop[i]); }
#endif
    }

    if ( eo::parallel.enableResults() )
    {
        double t2 = omp_get_wtime();
        eoLogger log;
        log << eo::file(eo::parallel.prefix()) << t2 - t1 << ' ';
    }

#else // _OPENMP
	wxString logstr = GetCurrentTimeString();
	logstr << "\nCalulate pop size " << _pop_size << " ...\n";
	g_pOptimizationFrame->AppendLog(logstr,false);
	/*
	concurrency::SchedulerPolicy oldpolicy = concurrency::CurrentScheduler::GetPolicy();	
	concurrency::SchedulerPolicy policy(oldpolicy);
	policy.SetConcurrencyLimits(1,_pop_size*4);
	//if (policy.GetPolicyValue(concurrency::MaxConcurrency) > 10)
			
	
	concurrency::CurrentScheduler::Create(policy);
	*/
	concurrency::parallel_for(0u, _pop_size, [&_proc,&_pop] (unsigned int i)  	
	{		
		_proc(_pop[i]);		
	});
	//concurrency::CurrentScheduler::Detach();
	g_pOptimizationFrame->AppendLog("done",true);

	std::stringstream ss;
	for (int i = 0 ; i < _pop_size; i++)
	{		
		ss << _pop[i] << std::endl;
	}
	g_pOptimizationFrame->AppendLog(ss.str(),true);
	/*
    for (size_t i = 0; i < size; ++i)
	{ 
		_proc(_pop[i]); 
	}
	*/

#endif // !_OPENMP
}
#endif


template <class EOT>
void eoEasyEA_para<EOT>::operator()(eoPop<EOT>& _pop)
{
	if (isFirstCall)
	{
		size_t total_capacity = _pop.capacity() + offspring.capacity();
		_pop.reserve(total_capacity);
		offspring.reserve(total_capacity);
		isFirstCall = false;
	}

    eoPop<EOT> empty_pop;
	g_pMainFrame->BackToInitialStateAndRun(m_running_time);
    popEval(empty_pop, _pop); // A first eval of pop.

	if (OptimizationFrame::s_behaviorMode == 1)//update mode
	{
		//replace all meshes (physics and shape) with best state

		OptimizationFrame::UpdateToBestState(&_pop);
	}
	chromosome_mesh_mapping.clear();

    do
    {
        try
        {
            unsigned pSize = _pop.size();
            offspring.clear(); // new offspring

            breed(_pop, offspring);
			
			
			g_pMainFrame->BackToInitialStateAndRun(m_running_time);            
			
			popEval(_pop, offspring); // eval of parents + offspring if necessary

            replace(_pop, offspring); // after replace, the new pop. is in _pop

			if (OptimizationFrame::s_behaviorMode == 1)//update mode
			{
				//replace all meshes (physics and shape) with best state

				OptimizationFrame::UpdateToBestState(&_pop);
			}

			chromosome_mesh_mapping.clear();

            if (pSize > _pop.size())
            throw std::runtime_error("Population shrinking!");
            else if (pSize < _pop.size())
            throw std::runtime_error("Population growing!");

        }
        catch (std::exception& e)
        {
            std::string s = e.what();
            s.append( " in eoEasyEA");
            throw std::runtime_error( s );
        }
    }
    while ( continuator( _pop ) );
}