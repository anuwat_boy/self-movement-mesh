#include "stdafx.h"
#include "sqp_reader.h"
#include <stdio.h>

bool sqp_reader(const char * filePath, std::vector<double> &texcoord , int numVertex)
{
	if (numVertex > 0)
		texcoord.resize(numVertex*2);

	FILE *fp =  NULL;	
	fopen_s(&fp , filePath, "rt");

	if (fp == NULL)
		return false;
	
	char tmp[256] = "";
	int i = 0;
	while (!feof(fp))
	{

		char *retgets = fgets(tmp,256,fp);
		if (retgets == NULL)
			break;
		double tx(DBL_MIN),ty(DBL_MIN);
		int ret = sscanf_s(tmp,"%lf %lf%*s",&tx,&ty);
		if (ret!= 2)
		{
			fclose(fp);
			return false;
		}
		if (numVertex > 0)
		{
			texcoord[i + 0] = tx;
			texcoord[i + 1] = ty;
		}
		else
		{
			texcoord.push_back(tx);
			texcoord.push_back(ty);
		}
		i+= 2;
	}

	
	fclose(fp);
	return true;

}