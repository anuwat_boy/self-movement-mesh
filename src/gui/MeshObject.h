// Copyright Ryan Schmidt 2011.
// Distributed under the Boost Software License, Version 1.0.
// (See copy at http://www.boost.org/LICENSE_1_0.txt)
#pragma once

#include <GSurface.h>
#include <VFMeshRenderer.h>
#include <ExpMapGenerator.h>
#include <IMeshBVTree.h>
#include <WmlPolygon2.h>
#include <VFMeshRenderer.h>
#include "Texture.h"

class MeshObject
{
public:
	MeshObject(void);
	~MeshObject(void);

	void SetSurface( rms::GSurface & surface );
	void NotifyMeshModified();

	void Render(bool bWireframe = false, bool bFlatShading = false, 
				bool bUseScalarColors = false, 
				rms::VFMeshRenderer::ColorTransferMode eTransferMode = rms::VFMeshRenderer::ScaledToUnit,
				int nScalarSet = 0);
	void RenderParamMesh();

	rms::GSurface & GetSurface() { return m_surface; }
	rms::VFTriangleMesh & GetMesh() { return m_surface.Mesh(); }
	rms::MeshPolygons & GetPolygons() { return m_surface.Polygons(); }
	rms::IMeshBVTree & GetBVTree() { return m_bvTree; }

	bool FindIntersection( Wml::Ray3f & vRay, Wml::Vector3f & vHit, Wml::Vector3f & vHitNormal );
	bool FindHitFrame( Wml::Ray3f & vRay, rms::Frame3f & vFrame );
	bool FindNearestFrame( const Wml::Vector3f & vPoint, rms::Frame3f & vNearestFrame );

	void SetComputeExpMap(bool bEnable);
	bool GetComputeExpMap() { return m_bComputeExpMap; }
	rms::Frame3f & ExpMapFrame() { return m_vSeedFrame; }
	void MoveExpMap( rms::Frame3f & vFrame );
	void ScaleExpMap( float fScale );
	void RotateExpMap( float fRotate );
	bool ValidateExpMap(bool bForce = false, rms::Polygon2f * pClipPoly = NULL );
	void SetDecalRadius( float fRadius );

	void ParameterizeMesh();

	rms::ExpMapGenerator * GetExpMapGen() { return & m_expmapgen; }


	//anuwat
	void SetTranslate(const float *xyz);
	void SetTranslate(const float x ,const float y ,const float z);

	void SetColor(const float *rgb);
	void SetColor(const float r ,const float g ,const float b);
protected:
	rms::GSurface m_surface;

	rms::VFMeshRenderer m_renderer;


	rms::IMeshBVTree m_bvTree;

	rms::Frame3f m_vSeedFrame;
	float m_fDecalRadius;
	float m_fBoundaryWidth;

	rms::ExpMapGenerator m_expmapgen;
	bool m_bExpMapValid;
	bool m_bComputeExpMap;

	Texture m_texture;


	float m_fTranslate[3];
	float m_fColor[3];
};
