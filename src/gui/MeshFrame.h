#pragma once
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif


#define ODE_PHYSICS
//#define BULLET_PHYSICS

#include "wxVTKRenderWindowInteractor.h"
#include "MouseInteractorStylePP.h"
#include "Properties.hpp"

#ifdef BULLET_PHYSICS
#include "PhysicsEngine.h"
#endif
//#include "PhysXEngine.h"
#ifdef ODE_PHYSICS
#include "ODEEngine.h"
#endif
#include "LaplacianDeformerEx.h"
#include "MeshObject.h"

#include <ostream>
class ImageRD;
class AbstractRD;

struct CollisionObject
{
	vtkActor *actor;
#ifdef BULLET_PHYSICS
	int btID;		
	int numTriangles;
	int numVertices;
	int *indices;
	btScalar *vertices;
	//PxActor* pxActor;
#endif
#ifdef ODE_PHYSICS
	dGeomID odeID;
	double *odeVertices;
	unsigned int *odeIndices; 
	int		odeNumVertices;
	int		odeNumTriangles;
#endif
	CollisionObject()
	{
		actor = NULL;	
		#ifdef BULLET_PHYSICS
		btID = -1;		
		numTriangles = 0;
		numVertices = 0;
		indices = NULL;
		vertices = NULL;
		//pxActor = NULL;
		#endif
		#ifdef ODE_PHYSICS
		odeID = NULL;
		odeVertices = NULL;
		odeIndices = NULL;
		odeNumVertices = 0;
		odeNumTriangles = 0;
		#endif
	}
};


struct control_vertex
{
	unsigned int vid;
	vtkIdType    display_vid;
	double		 pos[3];
};




class MeshFrame :	public wxFrame
{
public:
	MeshFrame(wxWindow *parent,const wxString& title, const wxPoint& pos, const wxSize& size, unsigned int numObj = 1);
	virtual ~MeshFrame(void);
	 
	
	void SetCurrentRDSystem(AbstractRD* system);
	void InitVTKPipeline();
	void UpdateMeshSystem(int obj_id ,bool isRunning = true, double time_since_last_frame = 0.0);
	
	void UpdatePhysicsObjects(int obj_id);
	void ResetToInitialState(int obj_id , bool deleteControlVertices);
	void Clone(int source_obj_id,int destination_obj_id);
	bool SetControlVertices(int obj_id , std::vector<int> & controlverticesID);
	bool SetControlVertices(int obj_id , std::vector<float> & controlTexCoord);
	int GetObjectivePosition(int obj_id , double *pos);
	std::string ReportPhysicsProperties(int obj_id , bool toLog = true);
	virtual void OnIdle(wxIdleEvent& evt);
	static int  ReserveMeshFrameID();
	static void ReleaseMeshFrameID(int);
	static void PrepareMeshFrames( int numFrames);
	static bool IsMeshFrameUsing(int id);
	static void SetMeshFrameReady(int id,bool ready);
	static bool IsAllReady();


	void StartRecordDebug(int mid);
	void EndRecordDebug(int mid);
	std::ofstream  fsDebugControlPoints[100];
	std::ofstream  fsDebugBallPos[100];
	bool recordDebug;
	int GetRevisionUpdate(int mid) ;
	void ResetRevisionUpdate(int mid) { m_revisionUpdate[mid] = 0; }
		vtkSmartPointer<vtkPolyData> m_smmPolydata[100];
private:
	void OnOpenMesh(wxCommandEvent& event);
	void OnOpenSqp(wxCommandEvent& event);
	void OnResetToInit0(wxCommandEvent& event);
	void OnResetToInit1(wxCommandEvent& event);
	void pickCallbackFunc(vtkObject*, unsigned long , void *);
	void pickCallbackFunc2(vtkObject*, unsigned long , void *);
	void mouseMoveAfterPickPointCallbackFunc(vtkObject*, unsigned long , void *);
	wxDECLARE_EVENT_TABLE();
protected:

	void InitializeRenderPane();
	void InitializeVTKPipeline(vtkRenderer* pRenderer);
	void InitializeVTKPipelineSphere(vtkRenderer* pRenderer);
	void InitializeVTKPipelineCylinder(vtkRenderWindow* pRenderWindow);
	void InitialMassSpringSystem();
	void InitialDeformSystem();
	void InitialPhysicsSystem();
	void InitialCollisionObject();
	void ClearMassSpringSystem();
	void UpdateControlVerticesPosition(int obj_id);
	void UpdateDeformMesh(int obj_id);
	void UpdateDeformMesh2(int obj_id);
protected:
	// current system being simulated (in future we might want more than one)
    ImageRD *system;

	// VTK does the rendering
    wxVTKRenderWindowInteractor *pVTKWindow;
	vtkSmartPointer<vtkRenderer> m_pRenderer[100];
	vtkSmartPointer<vtkPointPicker> m_pointPicker;
	vtkSmartPointer<vtkPointPicker> m_edgePicker;
	vtkSmartPointer<MouseInteractorStylePP> m_mouseInteractor;
	vtkSmartPointer<vtkPolyDataMapper> m_polydatamapper;
	vtkSmartPointer<vtkActor> m_actor[100];
	vtkSmartPointer<vtkTextActor> m_textActor;
	
	

	vtkSmartPointer<vtkDoubleArray> m_smmTexCoord; // tuple 3

	unsigned long m_c;
	int m_pickID;
	bool  m_bPickingPoint;
	//double* m_external_force;
	//size_t external_force_size;
	std::vector<int> m_borderVertices;

	vtkSmartPointer<vtkPoints> m_borderPoints[100];
	vtkSmartPointer<vtkPoints> m_selectedPoints[100];
	int	m_revisionUpdate[100];
	float  m_timeStep;
	void	UpdateControlPoints();


	rms::LaplacianDeformerEx m_deformer[100];
	rms::VFTriangleMesh m_vDeformedMesh[100];
	rms::VFTriangleMesh m_vDeformedMeshOriginal[100];
	rms::VertexMap m_VMap[100];
	std::vector<control_vertex> m_controlVertices[100];
	
#ifdef BULLET_PHYSICS
	PhysicsEngine *						m_PhysicsEngine[100];
#endif
	//PhysXEngine *						m_PhysXEngine[100];
#ifdef ODE_PHYSICS
	ODEEngine *							m_ODEEngine[100];
#endif
	std::vector<CollisionObject>				m_smmCO;
	std::vector<CollisionObject>				m_objectiveCO;
	float								m_initialObjectivePos[3];

	wxMutex		m_SolverMutex[100];	
	
	
	clock_t m_diffTimeFromLastFrame;
	//btClock m_clock;
	bool  m_allInitialed;

#ifdef BULLET_PHYSICS
	btScalar *m_original_smm_vertices;
#endif
	//PhysXEngine *						m_PhysXEngine[100];
#ifdef ODE_PHYSICS
	double *m_original_smm_ode_vertices;
#endif

	

	unsigned int m_numObj;
public:
	Properties render_settings;
	static std::vector<bool> usingMeshFrame;
	static std::vector<bool> readyMeshFrame;
	static wxMutex   s_MeshFrameMutex;
	static wxMutex   s_UpdateMutex;
	std::string m_physicsLog[100];
	std::string m_bestPhysicsLog;
};

