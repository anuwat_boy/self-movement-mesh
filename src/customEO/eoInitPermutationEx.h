#include <eo>
#include <eoInit.h>


template <class EOT>
class eoInitPermutationRange: public eoInit<EOT>
{
    public:

    typedef typename EOT::AtomType AtomType;

        eoInitPermutationRange(unsigned _chromSize, unsigned _randomFrom, unsigned _randomTo)
            : chromSize(_chromSize), randomFrom(_randomFrom),randomTo(_randomTo){}

        virtual void operator()(EOT& chrom)
        {
            //chrom.resize(chromSize);
			
			chrom.resize( randomTo - randomFrom + 1);
            for(unsigned idx=0;idx < chrom.size();idx++)
                  chrom[idx]=idx+randomFrom;

            std::random_shuffle(chrom.begin(), chrom.end(),gen);
			chrom.resize(chromSize);
            chrom.invalidate();
        }

    private :
        unsigned chromSize;
        unsigned randomFrom;
		unsigned randomTo;
        UF_random_generator<unsigned int> gen;
};