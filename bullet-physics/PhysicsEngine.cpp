/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "stdafx.h"
#include <windows.h>
#include <algorithm>
#include "PhysicsEngine.h"
#include "LinearMath/btIDebugDraw.h"
#include "btBulletDynamicsCommon.h"
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"

#include "BulletDynamics/ConstraintSolver/btPoint2PointConstraint.h"//picking
#include "BulletDynamics/ConstraintSolver/btGeneric6DofConstraint.h"//picking

#include "BulletCollision/CollisionShapes/btCollisionShape.h"
#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletCollision/CollisionShapes/btSphereShape.h"
#include "BulletCollision/CollisionShapes/btCompoundShape.h"
#include "BulletCollision/CollisionShapes/btUniformScalingShape.h"
#include "BulletDynamics/ConstraintSolver/btConstraintSolver.h"
#include "BulletCollision/CollisionDispatch/btSimulationIslandManager.h"
//#include "GL_ShapeDrawer.h"
#include "LinearMath/btQuickprof.h"
#include "LinearMath/btDefaultMotionState.h"
#include "LinearMath/btSerializer.h"
//#include "GLDebugFont.h"


#define USE_PARALLEL_SOLVER 1 //experimental parallel solver
#define USE_PARALLEL_DISPATCHER 1

#ifdef USE_PARALLEL_DISPATCHER
#include "BulletMultiThreaded/SpuGatheringCollisionDispatcher.h"
#include "BulletMultiThreaded/PlatformDefinitions.h"

#ifdef USE_LIBSPE2
#include "BulletMultiThreaded/SpuLibspe2Support.h"
#elif defined (_WIN32)
#include "BulletMultiThreaded/Win32ThreadSupport.h"
#include "BulletMultiThreaded/SpuNarrowPhaseCollisionTask/SpuGatheringCollisionTask.h"

#elif defined (USE_PTHREADS)

#include "BulletMultiThreaded/PosixThreadSupport.h"
#include "BulletMultiThreaded/SpuNarrowPhaseCollisionTask/SpuGatheringCollisionTask.h"

#else
//other platforms run the parallel code sequentially (until pthread support or other parallel implementation is added)

#include "BulletMultiThreaded/SpuNarrowPhaseCollisionTask/SpuGatheringCollisionTask.h"
#endif //USE_LIBSPE2

#ifdef USE_PARALLEL_SOLVER
#include "BulletMultiThreaded/btParallelConstraintSolver.h"
#include "BulletMultiThreaded/SequentialThreadSupport.h"

static int	engineCreatedCount;
btThreadSupportInterface* createSolverThreadSupport(int maxNumThreads)
{
//#define SEQUENTIAL
#ifdef SEQUENTIAL
	SequentialThreadSupport::SequentialThreadConstructionInfo tci("solverThreads",SolverThreadFunc,SolverlsMemoryFunc);
	SequentialThreadSupport* threadSupport = new SequentialThreadSupport(tci);
	threadSupport->startSPU();
#else

#ifdef _WIN32
	char uniqueName[64];
	sprintf_s(uniqueName,64,"solverThreads%02d", engineCreatedCount);
	Win32ThreadSupport::Win32ThreadConstructionInfo threadConstructionInfo(uniqueName,SolverThreadFunc,SolverlsMemoryFunc,maxNumThreads);
	Win32ThreadSupport* threadSupport = new Win32ThreadSupport(threadConstructionInfo);
	threadSupport->startSPU();
#elif defined (USE_PTHREADS)
	PosixThreadSupport::ThreadConstructionInfo solverConstructionInfo("solver", SolverThreadFunc,
																	  SolverlsMemoryFunc, maxNumThreads);
	
	PosixThreadSupport* threadSupport = new PosixThreadSupport(solverConstructionInfo);
	
#else
	SequentialThreadSupport::SequentialThreadConstructionInfo tci("solverThreads",SolverThreadFunc,SolverlsMemoryFunc);
	SequentialThreadSupport* threadSupport = new SequentialThreadSupport(tci);
	threadSupport->startSPU();
#endif
	
#endif

	return threadSupport;
}

#endif //USE_PARALLEL_SOLVER

#endif//USE_PARALLEL_DISPATCHER




extern bool gDisableDeactivation;
int numObjects = 0;
const int maxNumObjects = 16384;
btTransform startTransforms[maxNumObjects];
btCollisionShape* gShapePtr[maxNumObjects];//1 rigidbody has 1 shape (no re-use of shapes)
#define SHOW_NUM_DEEP_PENETRATIONS 1

extern int gNumClampedCcdMotions;

#ifdef SHOW_NUM_DEEP_PENETRATIONS 
extern int gNumDeepPenetrationChecks;
extern int gNumSplitImpulseRecoveries;
extern int gNumGjkChecks;
extern int gNumAlignedAllocs;
extern int gNumAlignedFree;
extern int gTotalBytesAlignedAllocs;
#endif //


const int maxProxies = 32766;
const int maxOverlap = 65535;


PhysicsEngine::PhysicsEngine()
//see btIDebugDraw.h for modes
:
m_dynamicsWorld(0),
m_pickConstraint(0),
m_debugMode(0),
m_idle(false),
m_defaultContactProcessingThreshold(BT_LARGE_FLOAT),
m_indexVertexArrays(NULL),
m_broadphase(NULL),
m_dispatcher(NULL),
m_solver(NULL),
m_collisionConfiguration(NULL)
{	
	#ifdef USE_PARALLEL_DISPATCHER
	engineCreatedCount++;
	#endif

}



PhysicsEngine::~PhysicsEngine()
{
	//if (m_shapeDrawer)
	//	delete m_shapeDrawer;
}



void PhysicsEngine::initPhysics()
{

	SYSTEM_INFO sysinfo;
	GetSystemInfo( &sysinfo );
	int numCPU = sysinfo.dwNumberOfProcessors;

	#ifdef USE_PARALLEL_DISPATCHER
	m_threadSupportSolver = NULL;
	m_threadSupportCollision = NULL;
	#endif

	m_collisionConfiguration = new btDefaultCollisionConfiguration();

#ifdef USE_PARALLEL_DISPATCHER
	int maxNumOutstandingTasks = std::max(1, (int)(numCPU * 0.2));

#ifdef USE_WIN32_THREADING
	
	char uniqueName[64];
	sprintf_s(uniqueName,64,"collision%02d", engineCreatedCount);
	
m_threadSupportCollision = new Win32ThreadSupport(Win32ThreadSupport::Win32ThreadConstructionInfo(
								uniqueName,
								processCollisionTask,
								createCollisionLocalStoreMemory,
								maxNumOutstandingTasks));
#else

#ifdef USE_LIBSPE2

   spe_program_handle_t * program_handle;
#ifndef USE_CESOF
                        program_handle = spe_image_open ("./spuCollision.elf");
                        if (program_handle == NULL)
                    {
                                perror( "SPU OPEN IMAGE ERROR\n");
                    }
                        else
                        {
                                printf( "IMAGE OPENED\n");
                        }
#else
                        extern spe_program_handle_t spu_program;
                        program_handle = &spu_program;
#endif
        SpuLibspe2Support* threadSupportCollision  = new SpuLibspe2Support( program_handle, maxNumOutstandingTasks);
#elif defined (USE_PTHREADS)
    PosixThreadSupport::ThreadConstructionInfo constructionInfo("collision",
								processCollisionTask,
								createCollisionLocalStoreMemory,
								maxNumOutstandingTasks);
    m_threadSupportCollision = new PosixThreadSupport(constructionInfo);
#else

	SequentialThreadSupport::SequentialThreadConstructionInfo colCI("collision",processCollisionTask,createCollisionLocalStoreMemory);
	SequentialThreadSupport* m_threadSupportCollision = new SequentialThreadSupport(colCI);
		
#endif //USE_LIBSPE2

///Playstation 3 SPU (SPURS)  version is available through PS3 Devnet
/// For Unix/Mac someone could implement a pthreads version of btThreadSupportInterface?
///you can hook it up to your custom task scheduler by deriving from btThreadSupportInterface
#endif


	m_dispatcher = new	SpuGatheringCollisionDispatcher(m_threadSupportCollision,maxNumOutstandingTasks,m_collisionConfiguration);
//	m_dispatcher = new	btCollisionDispatcher(m_collisionConfiguration);
#else
	
	m_dispatcher = new	btCollisionDispatcher(m_collisionConfiguration);
#endif //USE_PARALLEL_DISPATCHER
	
	
	
	btVector3 worldMin(-1000,-1000,-1000);
	btVector3 worldMax(1000,1000,1000);
	m_broadphase = new btAxisSweep3(worldMin,worldMax,maxProxies);

		
#ifdef USE_PARALLEL_SOLVER
	m_threadSupportSolver = createSolverThreadSupport(maxNumOutstandingTasks);
	m_solver = new btParallelConstraintSolver(m_threadSupportSolver);
	//this solver requires the contacts to be in a contiguous pool, so avoid dynamic allocation
	m_dispatcher->setDispatcherFlags(btCollisionDispatcher::CD_DISABLE_CONTACTPOOL_DYNAMIC_ALLOCATION);
#else

	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver();
	m_solver = solver;
	//default solverMode is SOLVER_RANDMIZE_ORDER. Warmstarting seems not to improve convergence, see 
	//solver->setSolverMode(0);//btSequentialImpulseConstraintSolver::SOLVER_USE_WARMSTARTING | btSequentialImpulseConstraintSolver::SOLVER_RANDMIZE_ORDER);
#endif //USE_PARALLEL_SOLVER

	
	
	btDiscreteDynamicsWorld* world = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfiguration);
	m_dynamicsWorld = world;
	m_dynamicsWorld->setGravity( btVector3(0.0,0.0,0));
	
	
	world->getSimulationIslandManager()->setSplitIslands(false);
	m_dynamicsWorld->getSolverInfo().m_numIterations = 4;
	m_dynamicsWorld->getSolverInfo().m_solverMode = SOLVER_SIMD+SOLVER_USE_WARMSTARTING;//+SOLVER_RANDMIZE_ORDER;
	m_dynamicsWorld->getDispatchInfo().m_enableSPU = true;
}

void	PhysicsEngine::exitPhysics()
{



	//cleanup in the reverse order of creation/initialization

	//remove the rigidbodies from the dynamics world and delete them
	int i;
	for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
	{
		btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		m_dynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}

	//delete collision shapes
	for (int j=0;j<m_collisionShapes.size();j++)
	{
		btCollisionShape* shape = m_collisionShapes[j];
		delete shape;
	}

	//delete dynamics world
	delete m_dynamicsWorld;
	m_dynamicsWorld = NULL;

	//delete solver
	delete m_solver;
#ifdef USE_PARALLEL_DISPATCHER
	if (m_threadSupportSolver)
	{
		#ifdef _WIN32
		((Win32ThreadSupport* )m_threadSupportSolver)->stopSPU();
		#endif
		delete m_threadSupportSolver;
	}
#endif

	//delete broadphase
	delete m_broadphase;

	//delete dispatcher
	delete m_dispatcher;

#ifdef USE_PARALLEL_DISPATCHER
	deleteCollisionLocalStoreMemory();
	if (m_threadSupportCollision)
	{
		#ifdef _WIN32
		((Win32ThreadSupport *)m_threadSupportCollision)->stopSPU();
		#endif
		delete m_threadSupportCollision;
	}
#endif

	delete m_collisionConfiguration;

	m_collisionShapes.clear();
	
}

void PhysicsEngine::toggleIdle() {
	if (m_idle) {
		m_idle = false;
	}
	else {
		m_idle = true;
	}
}



const float STEPSIZE = 5;


void	PhysicsEngine::setDebugMode(int mode)
{
	m_debugMode = mode;
	if (getDynamicsWorld() && getDynamicsWorld()->getDebugDrawer())
		getDynamicsWorld()->getDebugDrawer()->setDebugMode(mode);
}



//#define NUM_SPHERES_ON_DIAGONAL 9
//
//
//int gPickingConstraintId = 0;
//btVector3 gOldPickingPos;
//btVector3 gHitPos(-1,-1,-1);
//btScalar gOldPickingDist  = 0.f;
btRigidBody* pickedBody = 0;//for deactivation state



void PhysicsEngine::removePickingConstraint()
{
	if (m_pickConstraint && m_dynamicsWorld)
	{
		m_dynamicsWorld->removeConstraint(m_pickConstraint);
		delete m_pickConstraint;
		//printf("removed constraint %i",gPickingConstraintId);
		m_pickConstraint = 0;
		pickedBody->forceActivationState(ACTIVE_TAG);
		pickedBody->setDeactivationTime( 0.f );
		pickedBody = 0;
	}
}
/*
void	PhysicsEngine::mouseMotionFunc(int x,int y)
{

	if (m_pickConstraint)
	{
		//move the constraint pivot

		if (m_pickConstraint->getConstraintType() == D6_CONSTRAINT_TYPE)
		{
			btGeneric6DofConstraint* pickCon = static_cast<btGeneric6DofConstraint*>(m_pickConstraint);
			if (pickCon)
			{
				//keep it at the same picking distance

				btVector3 newRayTo = getRayTo(x,y);
				btVector3 rayFrom;
				btVector3 oldPivotInB = pickCon->getFrameOffsetA().getOrigin();

				btVector3 newPivotB;
				if (m_ortho)
				{
					newPivotB = oldPivotInB;
					newPivotB.setX(newRayTo.getX());
					newPivotB.setY(newRayTo.getY());
				} else
				{
					rayFrom = m_cameraPosition;
					btVector3 dir = newRayTo-rayFrom;
					dir.normalize();
					dir *= gOldPickingDist;

					newPivotB = rayFrom + dir;
				}
				pickCon->getFrameOffsetA().setOrigin(newPivotB);
			}

		} else
		{
			btPoint2PointConstraint* pickCon = static_cast<btPoint2PointConstraint*>(m_pickConstraint);
			if (pickCon)
			{
				//keep it at the same picking distance

				btVector3 newRayTo = getRayTo(x,y);
				btVector3 rayFrom;
				btVector3 oldPivotInB = pickCon->getPivotInB();
				btVector3 newPivotB;
				if (m_ortho)
				{
					newPivotB = oldPivotInB;
					newPivotB.setX(newRayTo.getX());
					newPivotB.setY(newRayTo.getY());
				} else
				{
					rayFrom = m_cameraPosition;
					btVector3 dir = newRayTo-rayFrom;
					dir.normalize();
					dir *= gOldPickingDist;

					newPivotB = rayFrom + dir;
				}
				pickCon->setPivotB(newPivotB);
			}
		}
	}

	float dx, dy;
    dx = btScalar(x) - m_mouseOldX;
    dy = btScalar(y) - m_mouseOldY;


	///only if ALT key is pressed (Maya style)
	if (m_modifierKeys& BT_ACTIVE_ALT)
	{
		if(m_mouseButtons & 2)
		{
			btVector3 hor = getRayTo(0,0)-getRayTo(1,0);
			btVector3 vert = getRayTo(0,0)-getRayTo(0,1);
			btScalar multiplierX = btScalar(0.001);
			btScalar multiplierY = btScalar(0.001);
			if (m_ortho)
			{
				multiplierX = 1;
				multiplierY = 1;
			}


			m_cameraTargetPosition += hor* dx * multiplierX;
			m_cameraTargetPosition += vert* dy * multiplierY;
		}

		if(m_mouseButtons & (2 << 2) && m_mouseButtons & 1)
		{
		}
		else if(m_mouseButtons & 1) 
		{
			m_azi += dx * btScalar(0.2);
			m_azi = fmodf(m_azi, btScalar(360.f));
			m_ele += dy * btScalar(0.2);
			m_ele = fmodf(m_ele, btScalar(180.f));
		} 
		else if(m_mouseButtons & 4) 
		{
			m_cameraDistance -= dy * btScalar(0.02f);
			if (m_cameraDistance<btScalar(0.1))
				m_cameraDistance = btScalar(0.1);

			
		} 
	}


	m_mouseOldX = x;
    m_mouseOldY = y;
	updateCamera();


}
*/


btRigidBody*	PhysicsEngine::localCreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape)
{
	btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0,0,0);
	if (isDynamic)
		shape->calculateLocalInertia(mass,localInertia);

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

#define USE_MOTIONSTATE 1
#ifdef USE_MOTIONSTATE
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);

	btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,shape,localInertia);

	btRigidBody* body = new btRigidBody(cInfo);
	body->setContactProcessingThreshold(m_defaultContactProcessingThreshold);

#else
	btRigidBody* body = new btRigidBody(mass,0,shape,localInertia);	
	body->setWorldTransform(startTransform);
#endif//

	m_dynamicsWorld->addRigidBody(body);

	return body;
}



extern CProfileIterator * m_profileIterator;



#include "BulletCollision/BroadphaseCollision/btAxisSweep3.h"


void	PhysicsEngine::clientResetScene()
{
	removePickingConstraint();

#ifdef SHOW_NUM_DEEP_PENETRATIONS
	gNumDeepPenetrationChecks = 0;
	gNumGjkChecks = 0;
#endif //SHOW_NUM_DEEP_PENETRATIONS

	gNumClampedCcdMotions = 0;
	int numObjects = 0;
	int i;

	if (m_dynamicsWorld)
	{
		int numConstraints = m_dynamicsWorld->getNumConstraints();
		for (i=0;i<numConstraints;i++)
		{
			m_dynamicsWorld->getConstraint(0)->setEnabled(true);
		}
		numObjects = m_dynamicsWorld->getNumCollisionObjects();
	
		///create a copy of the array, not a reference!
		btCollisionObjectArray copyArray = m_dynamicsWorld->getCollisionObjectArray();

		


		for (i=0;i<numObjects;i++)
		{
			btCollisionObject*	colObj=m_dynamicsWorld->getCollisionObjectArray()[i];
			//btCollisionObject* colObj = copyArray[i];
			btRigidBody* body = btRigidBody::upcast(colObj);
			if (body)
			{
				if (body->getMotionState())
				{
					btDefaultMotionState* myMotionState = (btDefaultMotionState*)body->getMotionState();
					myMotionState->m_graphicsWorldTrans = myMotionState->m_startWorldTrans;
					body->setCenterOfMassTransform( myMotionState->m_graphicsWorldTrans );
					body->setInterpolationWorldTransform( myMotionState->m_startWorldTrans );
					body->forceActivationState(ACTIVE_TAG);
					body->activate();
					body->setDeactivationTime(0);					
					colObj->setActivationState(WANTS_DEACTIVATION);
				}
				//removed cached contact points (this is not necessary if all objects have been removed from the dynamics world)
				if (m_dynamicsWorld->getBroadphase()->getOverlappingPairCache())
					m_dynamicsWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(colObj->getBroadphaseHandle(),getDynamicsWorld()->getDispatcher());

				
				body->clearForces();
				if (body && !body->isStaticObject())
				{
					body->setLinearVelocity(btVector3(0,0,0));
					body->setAngularVelocity(btVector3(0,0,0));
					
				}
			}

		}

		///reset some internal cached data in the broadphase
		m_dynamicsWorld->getBroadphase()->resetPool(getDynamicsWorld()->getDispatcher());
		m_dynamicsWorld->getConstraintSolver()->reset();
		m_dynamicsWorld->clearForces();
	}

}



int PhysicsEngine::CreateFixedCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,btVector3 &transformOrigin,btQuaternion *transformRotation )
{
	int vertStride = sizeof(btScalar) * 3;
	int indexStride = sizeof(int) * 3;
	btTriangleIndexVertexArray *_indexVertexArrays = new btTriangleIndexVertexArray(numTriangle,Indices,indexStride,numVertices,vertices,vertStride);

	btVector3 aabbMin(BT_LARGE_FLOAT,BT_LARGE_FLOAT,BT_LARGE_FLOAT);
	btVector3 aabbMax(-BT_LARGE_FLOAT,-BT_LARGE_FLOAT,-BT_LARGE_FLOAT);

	for (int i=0;i<numVertices;i++)
	{
		btVector3 vertex( vertices[i*3 + 0], vertices[i*3 + 1],vertices[i*3 + 2]);
		aabbMax.setMax( vertex);
		aabbMin.setMin( vertex);
		aabbMin.setMin( vertex);
		aabbMax.setMax( vertex);
		
	}
	
	
	btBvhTriangleMeshShape *_trimeshShape  = new btBvhTriangleMeshShape(_indexVertexArrays,true,aabbMin,aabbMax);
		
	
	

	float mass = 0.f;
	btTransform	startTransform;
	startTransform.setIdentity();
	startTransform.setOrigin(transformOrigin);

	if (transformRotation)
		startTransform.setRotation(*transformRotation);

	btRigidBody* staticBody = localCreateRigidBody(mass, startTransform,_trimeshShape);

	staticBody->setCollisionFlags( staticBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
	staticBody->setActivationState(DISABLE_DEACTIVATION);
	//staticBody->setCollisionFlags(staticBody->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);//STATIC_OBJECT);
	//staticBody->setCollisionFlags(staticBody->getCollisionFlags()  | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
		
	//delete [] Indices;
	//delete [] Vertices;
	m_collisionShapes.push_back(_trimeshShape);
	return (m_dynamicsWorld->getNumCollisionObjects() - 1);
}
int PhysicsEngine::CreateCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation )
{
	int vertStride = sizeof(btScalar) * 3;
	int indexStride = sizeof(int) * 3;
	
	btTriangleIndexVertexArray *_indexVertexArrays = new btTriangleIndexVertexArray(numTriangle,Indices,indexStride,numVertices,vertices,vertStride);

	btConvexTriangleMeshShape  *_trimeshShape  = new btConvexTriangleMeshShape (_indexVertexArrays,true);	
	


	
	btTransform	startTransform;
	startTransform.setIdentity();
	startTransform.setOrigin(transformOrigin);

	if (transformRotation)
		startTransform.setRotation(*transformRotation);
	
	btRigidBody* staticBody = localCreateRigidBody(mass, startTransform,_trimeshShape);
	m_collisionShapes.push_back(_trimeshShape);

	return (m_dynamicsWorld->getNumCollisionObjects() - 1);
}


	
int PhysicsEngine::CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&Vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation)
{
	numTriangle = polydata->GetPolys()->GetNumberOfCells();
	numVertices  = polydata->GetPoints()->GetNumberOfPoints();		
		
	polydata->GetPolys()->InitTraversal();
	Indices = new int[numTriangle*3];
	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}

	Vertices =  new btScalar[numVertices * 3];
	for  (int vi = 0 ; vi < numVertices; vi++)
	{			
		double *xyz = polydata->GetPoint(vi);
		Vertices[vi * 3 + 0]  = xyz[0];
		Vertices[vi * 3 + 1]  = xyz[1];
		Vertices[vi * 3 + 2]  = xyz[2];
	}
		

	return CreateCollisionRigidObject(numTriangle,Indices,numVertices,Vertices, mass,transformOrigin,transformRotation);
	/*
	int vertStride = sizeof(btScalar) * 3;
	int indexStride = sizeof(int) * 3;
	
	btTriangleIndexVertexArray *_indexVertexArrays = new btTriangleIndexVertexArray(numTriangle,Indices,indexStride,numVertices,Vertices,vertStride);

	btConvexTriangleMeshShape  *_trimeshShape  = new btConvexTriangleMeshShape (_indexVertexArrays,true);	
	


	
	btTransform	startTransform;
	startTransform.setIdentity();
	startTransform.setOrigin(transformOrigin);

	if (transformRotation)
		startTransform.setRotation(*transformRotation);
	
	staticBody = localCreateRigidBody(mass, startTransform,_trimeshShape);
	//staticBody->setCollisionFlags(staticBody->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);//STATIC_OBJECT);

	m_collisionShapes.push_back(trimeshShape);
	return (m_collisionShapes.size() - 1);
	*/
}


	


int PhysicsEngine::CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&Vertices,btVector3 &transformOrigin,btQuaternion *transformRotation)
{
	
	numTriangle = polydata->GetPolys()->GetNumberOfCells();
	numVertices  = polydata->GetPoints()->GetNumberOfPoints();		
		
	polydata->GetPolys()->InitTraversal();
	Indices = new int[numTriangle*3];
	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}

	Vertices =  new btScalar[numVertices * 3];
	for  (int vi = 0 ; vi < numVertices; vi++)
	{			
		double *xyz = polydata->GetPoint(vi);
		Vertices[vi * 3 + 0]  = xyz[0];
		Vertices[vi * 3 + 1]  = xyz[1];
		Vertices[vi * 3 + 2]  = xyz[2];
	}
	return CreateFixedCollisionRigidObject(numTriangle, Indices, numVertices, Vertices, transformOrigin, transformRotation);	
	/*
	int vertStride = sizeof(btScalar) * 3;
	int indexStride = sizeof(int) * 3;
	btTriangleIndexVertexArray *_indexVertexArrays = new btTriangleIndexVertexArray(numTriangles,Indices,indexStride,numVertices,Vertices,vertStride);

	
	btVector3 aabbMin(-1000,-1000,-1000),aabbMax(1000,1000,1000); 
	
	btBvhTriangleMeshShape *_trimeshShape  = new btBvhTriangleMeshShape(_indexVertexArrays,true,aabbMin,aabbMax);
		
	
	

	float mass = 0.f;
	btTransform	startTransform;
	startTransform.setIdentity();
	staticBody = localCreateRigidBody(mass, startTransform,_trimeshShape);
	staticBody->setCollisionFlags(staticBody->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);//STATIC_OBJECT);

		
	//delete [] Indices;
	//delete [] Vertices;
	m_collisionShapes.push_back(trimeshShape);
	return  (m_collisionShapes.size() - 1);
	*/
}


void PhysicsEngine::RemoveRigidObject(int bt_id)
{
	btCollisionObject*	colObj=m_dynamicsWorld->getCollisionObjectArray()[bt_id];		
	btRigidBody* body = btRigidBody::upcast(colObj);
	m_dynamicsWorld->removeRigidBody(body);
}

void PhysicsEngine::Tick(btScalar _time)
{
	if (m_dynamicsWorld)
	{
		float dt = getDeltaTimeMicroseconds() * 0.000001f;	
		//m_dynamicsWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(staticBody->getBroadphaseHandle(),getDynamicsWorld()->getDispatcher());

		if (_time > 0)
			m_dynamicsWorld->stepSimulation(btScalar(1.)/btScalar(60.),0);
		else
			m_dynamicsWorld->stepSimulation(btScalar(1.)/btScalar(60.),0);
	
	}
	
	
	//test hitting 
	/*
	int numManifolds = m_dynamicsWorld->getDispatcher()->getNumManifolds();
    for (int i=0;i<numManifolds;i++)
    {
        btPersistentManifold* contactManifold =  m_dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
        const btCollisionObject* obA = static_cast<const btCollisionObject*>(contactManifold->getBody0());
        const btCollisionObject* obB = static_cast<const btCollisionObject*>(contactManifold->getBody1());

        int numContacts = contactManifold->getNumContacts();
        for (int j=0;j<numContacts;j++)
        {
            btManifoldPoint& pt = contactManifold->getContactPoint(j);
            if (pt.getDistance()<0.f)
            {
                const btVector3& ptA = pt.getPositionWorldOnA();
                const btVector3& ptB = pt.getPositionWorldOnB();
                const btVector3& normalOnB = pt.m_normalWorldOnB;
				std::cout << "HIT !!! " << std::endl;
            }
        }
    }
	*/
}

btCollisionObject* PhysicsEngine::GetCollisionObject( int index)
{
	btCollisionObject*	colObj=m_dynamicsWorld->getCollisionObjectArray()[index];
	return colObj;
}
	
void PhysicsEngine::ResetScene()
{
	if (m_dynamicsWorld)
	{
		int numConstraints = m_dynamicsWorld->getNumConstraints();
		for (int i=0;i<numConstraints;i++)
		{
			m_dynamicsWorld->getConstraint(0)->setEnabled(true);
		}
		numObjects = m_dynamicsWorld->getNumCollisionObjects();
	
		///create a copy of the array, not a reference!
		btCollisionObjectArray copyArray = m_dynamicsWorld->getCollisionObjectArray();

		


		for (int i=0;i<numObjects;i++)
		{
			btCollisionObject* colObj = copyArray[i];
			btRigidBody* body = btRigidBody::upcast(colObj);
			
			if (body)
			{
				if (body->getMotionState())
				{
					btDefaultMotionState* myMotionState = (btDefaultMotionState*)body->getMotionState();
					myMotionState->m_graphicsWorldTrans = myMotionState->m_startWorldTrans;
					body->setCenterOfMassTransform( myMotionState->m_graphicsWorldTrans );
					colObj->setInterpolationWorldTransform( myMotionState->m_startWorldTrans );
					colObj->forceActivationState(ACTIVE_TAG);
					colObj->activate();
					colObj->setDeactivationTime(0);
					
					//colObj->setActivationState(WANTS_DEACTIVATION);
				}
				//removed cached contact points (this is not necessary if all objects have been removed from the dynamics world)
				if (m_dynamicsWorld->getBroadphase()->getOverlappingPairCache())
					m_dynamicsWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(colObj->getBroadphaseHandle(),getDynamicsWorld()->getDispatcher());

				//btRigidBody* body = btRigidBody::upcast(colObj);
				body->clearForces();

				if (body && !body->isStaticObject())
				{
					body->setLinearVelocity(btVector3(0,0,0));
					body->setAngularVelocity(btVector3(0,0,0));
					
				}
			}

		}

		///reset some internal cached data in the broadphase
		m_dynamicsWorld->getBroadphase()->resetPool(getDynamicsWorld()->getDispatcher());
		m_dynamicsWorld->getConstraintSolver()->reset();

	}

	
}

int PhysicsEngine::GetCollisionObjectTransformMatrix(int objIndex, double * m)
{
	btCollisionObject*	colObj=m_dynamicsWorld->getCollisionObjectArray()[objIndex];
	btRigidBody*		body=btRigidBody::upcast(colObj);
	
	btScalar mat[16];

	if ( body->isStaticObject())
	{
		body->getWorldTransform().getOpenGLMatrix(mat);		
	}
	else
	{
		if(body&&body->getMotionState())
		{
			btDefaultMotionState* myMotionState = (btDefaultMotionState*)body->getMotionState();		
			myMotionState->m_graphicsWorldTrans.getOpenGLMatrix(mat);
			
		}
		else
		{
			colObj->getWorldTransform().getOpenGLMatrix(mat);
			
		}
	}

	for (int i = 0 ; i < 16; i++)
		m[i] = mat[i];

	return body->getUpdateRevisionInternal();
}

void PhysicsEngine::SetCollisionObjectTransformMatrix(int objIndex, double *m)
{
	btCollisionObject*	colObj=m_dynamicsWorld->getCollisionObjectArray()[objIndex];
	btRigidBody*		body=btRigidBody::upcast(colObj);
	btScalar mat[16];
	for (int i = 0 ; i < 16; i++)
		mat[i] = m[i];

	if (body->isStaticObject())
	{	
		
		btTransform trans;
		trans.setFromOpenGLMatrix(mat);
		body->setWorldTransform(trans);		
		
	}
	else
	{
		btDefaultMotionState* myMotionState = (btDefaultMotionState*)body->getMotionState();
		myMotionState->m_graphicsWorldTrans.setFromOpenGLMatrix(mat);
	}
	/*
	
	if(body&&body->getMotionState())
	{
		btDefaultMotionState* myMotionState = (btDefaultMotionState*)body->getMotionState();
		myMotionState->m_graphicsWorldTrans.setFromOpenGLMatrix(mat);
	}
	else
	{
		colObj->getWorldTransform().setFromOpenGLMatrix(mat);
	}
	*/
}