/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef PHYSICS_ENGINE_H
#define PHYSICS_ENGINE_H


//#include "GlutStuff.h"
//#include "GL_ShapeDrawer.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#if defined(_DEBUG)
#pragma comment(lib,"BulletWorldImporter_Debug")
#pragma comment(lib,"BulletDynamics_Debug")
#pragma comment(lib,"BulletCollision_Debug")
#pragma comment(lib,"BulletFileLoader_Debug")
#pragma comment(lib,"LinearMath_Debug")
#pragma comment(lib,"BulletMultiThreaded_Debug")
#else if defined(NDEBUG)
#pragma comment(lib,"BulletWorldImporter")
#pragma comment(lib,"BulletDynamics")
#pragma comment(lib,"BulletCollision")
#pragma comment(lib,"BulletFileLoader")
#pragma comment(lib,"LinearMath")
#pragma comment(lib,"BulletMultiThreaded")
#endif


#include "LinearMath/btVector3.h"
#include "LinearMath/btMatrix3x3.h"
#include "LinearMath/btTransform.h"
#include "LinearMath/btQuickprof.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "btBulletDynamicsCommon.h"
class	btCollisionShape;
class	btDynamicsWorld;
class	btRigidBody;
class	btTypedConstraint;
class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
class btCollisionObject;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;
class btTriangleIndexVertexArray;

struct btTriMesh
{
	int numTriangles;
	int numVertices;
	int *indices;
	btScalar *vertices;
};



class PhysicsEngine
{
public:
		
	PhysicsEngine();
	
	virtual ~PhysicsEngine();
	
protected:
	
	class CProfileIterator* m_profileIterator;

	protected:
#ifdef USE_BT_CLOCK
	btClock m_clock;
#endif //USE_BT_CLOCK

	///this is the most important class
	btDynamicsWorld*		m_dynamicsWorld;

	///constraint for mouse picking
	btTypedConstraint*		m_pickConstraint;

	virtual void removePickingConstraint();

		

	
	int	m_debugMode;	
	
	bool m_idle;	

	btScalar		m_defaultContactProcessingThreshold;
	btAlignedObjectArray<btCollisionShape*>	m_collisionShapes;
	btTriangleIndexVertexArray* m_indexVertexArrays;
	btBroadphaseInterface*	m_broadphase;
	btCollisionDispatcher*	m_dispatcher;
	btConstraintSolver*	m_solver;
	btDefaultCollisionConfiguration* m_collisionConfiguration;

	class	btThreadSupportInterface*		m_threadSupportCollision;
	class	btThreadSupportInterface*		m_threadSupportSolver;
public:
		
	

	btDynamicsWorld*		getDynamicsWorld()
	{
		return m_dynamicsWorld;
	}

	virtual	void initPhysics();
	virtual void exitPhysics();
	virtual void Tick(btScalar _time = 0);
	virtual int GetCollisionObjectTransformMatrix(int objIndex, double * m);
	virtual void SetCollisionObjectTransformMatrix(int objIndex, double * m);
	virtual int CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&vertices,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL );
	virtual int CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual int CreateFixedCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual int CreateCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual void RemoveRigidObject(int bt_id);
	virtual void ResetScene();
	virtual btCollisionObject* GetCollisionObject( int bullet_id);
	int		getDebugMode()
	{
		return m_debugMode ;
	}
	
	void	setDebugMode(int mode);	

	
	void toggleIdle();	
	btScalar	getDeltaTimeMicroseconds()
	{
#ifdef USE_BT_CLOCK
		btScalar dt = (btScalar)m_clock.getTimeMicroseconds();
		m_clock.reset();
		return dt;
#else
		return btScalar(16666.);
#endif
	}

	virtual void	clientResetScene();
	
	btRigidBody*	localCreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape);

	
	bool	isIdle() const
	{
		return	m_idle;
	}

	void	setIdle(bool idle)
	{
		m_idle = idle;
	}


};

#endif //PHYSICS_ENGINE_H


