Self Movement Mesh Project
by Anuwat Dechvijankit

I would like to write in English since all researchers around the world can understand this project at most.
*This project is a part of my doctoral research.

Original code:
This code is heavily based on Ready project https://github.com/GollyGang/ready (version 0.7) 

Change from Original:
This project has multi windows;  Reaction Diffusion planes, 3D objects , Optimization (GA) control and log  windows.
All are execute with multi-thread approach mindset.
RD planes update then 3D objects move, While GA is trying to observe each system.
  

Building Environment:
This program is based on Windows OS 64 bits environment.
It should be able to build by Microsoft Visual Studio 2012 , 2013 , 2015
However, I used Intel C++ compiler but it should be able to compile with default compiler of your tools too.


Free/open source Library 
** All libraries should be compiled by /MT or /MTd flag!
- genetic algorithm from Paradiseo library, prebuild lib included.
- various physics engine: bullet, physx, ODE   (ODE is the working one)  
  ODE source code included
- Libgeometry http://www.dgp.toronto.edu/~rms/software/libgeometry/index.html   for mesh deformation library
  prebuild lib is included
- wxWidget  for GUI   (reference from Ready Code)
  source code and libs are included 
- VTK for graphics rendering (reference from Ready Code) ( please set VTK_DIR and VTK_VERSION environment variables in your OS)
  you need to build your own VTK library.  http://www.vtk.org/   tested with 7.0
  for less problems , please use built library folder (from CMAKE then MSVC) as your main VTK folder (not install folder eg. c:\Program Files\vtk).
  but please copy include folder from install folder to root folder of vtk so 
  and copy lib folder inside build folder to root folder

you should have 
  $(VTK_DIR)\include\vtk-$(VTK_VERSION)
 $(VTK_DIR)\lib\debug
  $(VTK_DIR)\lib\release
structur
- OpenMP  (Default in MSVC or intel c++)


The purpose:
This project is trying to achieve "machine learning"+"physics simulation"+"2D data mapping with 3D" style.
As you can see  
machine learning is GA , 
physics simulation is ODE and 
2D data mapping with 3D  is Reaction - Diffusion (RD) with my own 3D mesh disk-like tube.

I am trying to create system to use GA machine learning to change RD plane to control 3D surface and do some task.
Current task is move to ball out from the tube on the right side (+x).
The tube is deformed then push ball to move based on physics engine.
The deformation is based on control points (green dots) which has mapping coordinates in RD plane. 
When RD plane have changed , control points move then mesh deformed.

RD plane 's pattern is changed based on modified GA system which be changed based on 3D ball position.

How to run pre-build 
run \bin\ready.exe


