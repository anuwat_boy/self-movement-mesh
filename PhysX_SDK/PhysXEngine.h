/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef PHYSX_ENGINE_H
#define PHYSX_ENGINE_H


//#include "GlutStuff.h"
//#include "GL_ShapeDrawer.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>


#include "PhysX_Header.h"
#include "physxvisualdebuggersdk/PvdConnectionManager.h"
#pragma comment( lib, "PhysXVisualDebuggerSDKDEBUG" )

namespace PVD {
	using namespace physx::debugger;
	using namespace physx::debugger::comm;
}

using namespace physx;





class PhysXEngine
					: public PVD::PvdConnectionHandler
					
{

private:
	struct PvdParameters
	{
		char							ip[256];
		PxU32							port;
		PxU32							timeout;
		bool							useFullPvdConnection;

		PvdParameters()
		: port(5425)
		, timeout(10)
		, useFullPvdConnection(true)
		{
			PxStrcpy(ip, 256, "127.0.0.1");
		}
	};
protected:
	
	PxFoundation*							mFoundation;
	//PxProfileZoneManager*					mProfileZoneManager;
	PxPhysics*								mPhysics;

	PxCooking*								mCooking;
	PxScene*								mScene;
	PxMaterial*								mMaterial;
	PxDefaultCpuDispatcher*					mCpuDispatcher;
	PxReal									mAccumulator;
	PxReal									mStepSize;
	PvdParameters							mPvdParams;
public:
		
	PhysXEngine();
	
	virtual ~PhysXEngine();

	void togglePvdConnection();
	void createPvdConnection();	
	//virtual	void onRelease(const PxBase* observed, void* userData, PxDeletionEventFlag::Enum deletionEvent);

	virtual	void initPhysics();
	virtual PxActor* CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,const PxVec3& initialPos);
	virtual PxActor* CreateKinematicRigidObject(vtkSmartPointer<vtkPolyData> polydata,const PxVec3& initialPos);
	virtual PxActor* CreateHeightFieldRigidObject(vtkSmartPointer<vtkPolyData> polydata);
	virtual PxActor* CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,float mass, const PxVec3 &initialPos);
	virtual PxActor* CreateCollisionRigidSphereObject(float radius,float mass,const PxVec3 &initialPos);
	virtual void Tick(float _time);
	void ResetFilter(PxRigidActor *actor);
	void ResetFilter(PxRigidActor *actor	,PxShape *const * 	shapes, PxU32 	shapeCount	);
	bool fetchResult();

	PxScene* GetScene() {return mScene;};
	PxPhysics *GetPhysics() {return mPhysics;};
	PxShape* CloneShape(PxShape &src);
	/*
	virtual void exitPhysics();
	virtual void Tick(btScalar _time = 0);
	virtual int GetCollisionObjectTransformMatrix(int objIndex, double * m);
	virtual void SetCollisionObjectTransformMatrix(int objIndex, double * m);
	virtual int CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&vertices,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL );
	virtual int CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual int CreateFixedCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual int CreateCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual void RemoveRigidObject(int bt_id);
	virtual void ResetScene();
	virtual btCollisionObject* GetCollisionObject( int bullet_id);

	
	void	setDebugMode(int mode);	

	
	void toggleIdle();	
	

	virtual void	clientResetScene();
	
	btRigidBody*	localCreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape);
	*/

private:
		// Implements PvdConnectionFactoryHandler
	virtual			void									onPvdSendClassDescriptions(PVD::PvdConnection&) {}
	virtual			void									onPvdConnected(PVD::PvdConnection& inFactory);
	virtual			void									onPvdDisconnected(PVD::PvdConnection& inFactory);
};

#endif //PHYSX_ENGINE_H


