
#include "stdafx.h"
#include <algorithm>
#include "PhysXEngine.h"

PxDefaultAllocator gDefaultAllocatorCallback;



PX_FORCE_INLINE PxSimulationFilterShader getFilterShader()
{
	return PxDefaultSimulationFilterShader;
}

PxErrorCallback& getErrorCallback()
{
	static PxDefaultErrorCallback gDefaultErrorCallback;
	return gDefaultErrorCallback;
}


PhysXEngine::PhysXEngine() 

{
	mStepSize = 1.0f / 60.0f;
	mAccumulator = 0.0f;
}

PhysXEngine::~PhysXEngine()
{

}


void PhysXEngine::togglePvdConnection()
{
	if(!mPhysics->getPvdConnectionManager()) return;
	if (mPhysics->getPvdConnectionManager()->isConnected())
		mPhysics->getPvdConnectionManager()->disconnect();
	else
		createPvdConnection();
}

void PhysXEngine::createPvdConnection()
{
	PxVisualDebuggerConnectionManager* pvd = mPhysics->getPvdConnectionManager();
	if(!pvd)
		return;

	//The connection flags state overall what data is to be sent to PVD.  Currently
	//the Debug connection flag requires support from the implementation (don't send
	//the data when debug isn't set) but the other two flags, profile and memory
	//are taken care of by the PVD SDK.

	//Use these flags for a clean profile trace with minimal overhead
	PxVisualDebuggerConnectionFlags theConnectionFlags( PxVisualDebuggerConnectionFlag::eDEBUG | PxVisualDebuggerConnectionFlag::ePROFILE | PxVisualDebuggerConnectionFlag::eMEMORY );
	if (!mPvdParams.useFullPvdConnection)
	{
		theConnectionFlags = PxVisualDebuggerConnectionFlag::ePROFILE;
	}
	
	//Create a pvd connection that writes data straight to the filesystem.  This is
	//the fastest connection on windows for various reasons.  First, the transport is quite fast as
	//pvd writes data in blocks and filesystems work well with that abstraction.
	//Second, you don't have the PVD application parsing data and using CPU and memory bandwidth
	//while your application is running.
	//PxVisualDebuggerExt::createConnection(mPhysics->getPvdConnectionManager(), "c:\\temp.pxd2", theConnectionFlags);

	//The normal way to connect to pvd.  PVD needs to be running at the time this function is called.
	//We don't worry about the return value because we are already registered as a listener for connections
	//and thus our onPvdConnected call will take care of setting up our basic connection state.

	PxVisualDebuggerExt::createConnection(pvd, mPvdParams.ip, mPvdParams.port, mPvdParams.timeout, theConnectionFlags);
}


void PhysXEngine::onPvdConnected(PVD::PvdConnection& )
{
	//setup joint visualization.  This gets piped to pvd.
	mPhysics->getVisualDebugger()->setVisualizeConstraints(true);
	mPhysics->getVisualDebugger()->setVisualDebuggerFlag(PxVisualDebuggerFlag::eTRANSMIT_CONTACTS, true);
	mPhysics->getVisualDebugger()->setVisualDebuggerFlag(PxVisualDebuggerFlag::eTRANSMIT_SCENEQUERIES, true);
}

void PhysXEngine::onPvdDisconnected(PVD::PvdConnection& conn)
{
	conn.release();
}
/*
void PhysXEngine::onRelease(const PxBase* observed, void* userData, PxDeletionEventFlag::Enum deletionEvent)
{
	PX_UNUSED(userData);
	PX_UNUSED(deletionEvent);

	if(observed->is<PxRigidActor>())
	{
		const PxRigidActor* actor = static_cast<const PxRigidActor*>(observed);

		removeRenderActorsFromPhysicsActor(actor);

		std::vector<PxRigidActor*>::iterator actorIter = std::find(mPhysicsActors.begin(), mPhysicsActors.end(), actor);
		if(actorIter != mPhysicsActors.end())
		{
			mPhysicsActors.erase(actorIter);
		}

	}
}
*/

void PhysXEngine::initPhysics()
{
	bool recordMemoryAllocations = true;
#ifdef RENDERER_ANDROID
	const bool useCustomTrackingAllocator = false;
#else
	const bool useCustomTrackingAllocator = true;
#endif

	PxAllocatorCallback* allocator = &gDefaultAllocatorCallback;
	/*
	if(useCustomTrackingAllocator)		
		allocator = getSampleAllocator();		//optional override that will track memory allocations
	*/
	PxFoundation *_Foundation = PxCreateFoundation(PX_PHYSICS_VERSION, *allocator, getErrorCallback());
	

	if(!_Foundation)
	{
		_Foundation = &PxGetFoundation();
		if(!_Foundation)
			throw("PxCreateFoundation failed!");
	}
	
	mFoundation = _Foundation;
	
	

	

	PxTolerancesScale scale;
	mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *mFoundation, scale, recordMemoryAllocations, NULL);
	if(!mPhysics)
		throw("PxCreatePhysics failed!");
	
	
	if(!PxInitExtensions(*mPhysics))
		throw("PxInitExtensions failed!");

	PxCookingParams params(scale);
	params.meshWeldTolerance = 0.001f;
	params.meshPreprocessParams = PxMeshPreprocessingFlags(PxMeshPreprocessingFlag::eWELD_VERTICES | PxMeshPreprocessingFlag::eREMOVE_UNREFERENCED_VERTICES | PxMeshPreprocessingFlag::eREMOVE_DUPLICATED_TRIANGLES | PxMeshPreprocessingFlag::eFORCE_32BIT_INDICES);
	mCooking = PxCreateCooking(PX_PHYSICS_VERSION, *mFoundation, params);
	if(!mCooking)
		throw("PxCreateCooking failed!");

	
	togglePvdConnection();
	
	if(mPhysics->getPvdConnectionManager())
		mPhysics->getPvdConnectionManager()->addHandler(*this);
	
	//mPhysics->registerDeletionListener(*this, PxDeletionEventFlag::eUSER_RELEASE);
	
	// setup default material...
	mMaterial = mPhysics->createMaterial(0.5f, 0.5f, 0.1f);
	if(!mMaterial)
		throw("createMaterial failed!");

	PX_ASSERT(NULL == mScene);

	PxSceneDesc sceneDesc(mPhysics->getTolerancesScale());
	//sceneDesc.gravity = PxVec3(0.0f, -9.81, 0.0f);
	sceneDesc.gravity = PxVec3(0.0f, 0.0f, 0.0f);
	//getDefaultSceneDesc(sceneDesc);
	//customizeSceneDesc(sceneDesc);
	
	PxU32									mNbThreads = 5;
	if(!sceneDesc.cpuDispatcher)
	{
		mCpuDispatcher = PxDefaultCpuDispatcherCreate(mNbThreads);
		if(!mCpuDispatcher)
			throw("PxDefaultCpuDispatcherCreate failed!");
		sceneDesc.cpuDispatcher	= mCpuDispatcher;
	}

	if(!sceneDesc.filterShader)
		sceneDesc.filterShader	= getFilterShader();



	//sceneDesc.flags |= PxSceneFlag::eENABLE_TWO_DIRECTIONAL_FRICTION;
	//sceneDesc.flags |= PxSceneFlag::eENABLE_PCM;
	//sceneDesc.flags |= PxSceneFlag::eENABLE_ONE_DIRECTIONAL_FRICTION;  
	//sceneDesc.flags |= PxSceneFlag::eADAPTIVE_FORCE;
	sceneDesc.flags |= PxSceneFlag::eENABLE_ACTIVETRANSFORMS;
	//sceneDesc.flags |= PxSceneFlag::eDISABLE_CONTACT_CACHE;

	
#ifdef USE_MBP
	sceneDesc.broadPhaseType = PxBroadPhaseType::eMBP;
#endif
	/*
	if(mStepperType == INVERTED_FIXED_STEPPER)
		sceneDesc.simulationOrder = PxSimulationOrder::eSOLVE_COLLIDE;
	*/
	mScene = mPhysics->createScene(sceneDesc);
	if(!mScene)
		throw("createScene failed!");
   
	PxSceneWriteLock scopedLock(*mScene);

	PxSceneFlags flag = mScene->getFlags();

	PX_UNUSED(flag);
	mScene->setVisualizationParameter(PxVisualizationParameter::eSCALE, 0.0f);
	mScene->setVisualizationParameter(PxVisualizationParameter::eCOLLISION_SHAPES,	1.0f);

#ifdef USE_MBP
	setupMBP(*mScene);
#endif
	/*
	
	PxReal debugRenderScale = mSample->getDebugRenderScale();

	for(PxU32 i=0; i < mMenuVisualizations.size(); i++)
	{
		bool enabled = mMenuVisualizations[i].toggleState;
		
		PxVisualizationParameter::Enum p = static_cast<PxVisualizationParameter::Enum>(mMenuVisualizations[i].toggleCommand);
		
		if (p != PxVisualizationParameter::eSCALE)
			scene.setVisualizationParameter(p, enabled ? 1.0f : 0.0f);
		else
			scene.setVisualizationParameter(p, enabled ? debugRenderScale: 0.0f); 		
	}
	*/

	/*
	mApplication.refreshVisualizationMenuState(PxVisualizationParameter::eCOLLISION_SHAPES);
	mApplication.applyDefaultVisualizationSettings();
	mApplication.setMouseCursorHiding(false);
	mApplication.setMouseCursorRecentering(false);
	mCameraController.setMouseLookOnMouseButton(false);
	mCameraController.setMouseSensitivity(1.0f);

	if(mCreateGroundPlane)
		createGrid();
	*/
	//LOG_INFO("PhysXSample", "Init sample ok!");
}


PxActor* PhysXEngine::CreateHeightFieldRigidObject(vtkSmartPointer<vtkPolyData> polydata)
{
	/*
	const int numTriangle = polydata->GetPolys()->GetNumberOfCells();
	const int numVertices  = polydata->GetPoints()->GetNumberOfPoints();	

	PxSceneWriteLock scopedLock(*mScene);
	physx::PxVec3 *Verts = new physx::PxVec3[numVertices];
	for (int i = 0; i < numVertices; i++)
	{
		double *thispoint = polydata->GetPoint(i);
		Verts[i] = physx::PxVec3(thispoint[0],thispoint[1],thispoint[2]);
	}

	polydata->GetPolys()->InitTraversal();
	PxU32 *Indices = new PxU32[polydata->GetNumberOfPolys()*3];
	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}


	PxTriangleMeshDesc meshDesc;
	meshDesc.points.count           = numVertices;
	meshDesc.points.stride          = sizeof(PxVec3);
	meshDesc.points.data            = Verts;

	meshDesc.triangles.count        = numTriangle;
	meshDesc.triangles.stride       = 3*sizeof(PxU32);
	meshDesc.triangles.data         = Indices;
	
	


	physx::PxDefaultMemoryOutputStream writeBuffer;
	
	bool status = mCooking->cookTriangleMesh(meshDesc, writeBuffer);
	
	if(!status)
		return NULL;

	physx::PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());

	physx::PxHeightField* pxMesh = mPhysics->createHeightField(readBuffer);
	PxTransform trans = PxTransform(PxIdentity);
	PxHeightFieldGeometry hfGeom(pxMesh, PxMeshGeometryFlags(),1.0,1.0,1.0);
	PxRigidStatic *mesh = PxCreateStatic(*mPhysics,trans, PxHeightFieldGeometry(pxMesh),*mMaterial);
	mScene->addActor(*mesh);
	*/
	return NULL;
}

PxActor* PhysXEngine::CreateKinematicRigidObject(vtkSmartPointer<vtkPolyData> polydata,const PxVec3& initialPos)
{
	const int numTriangle = polydata->GetPolys()->GetNumberOfCells();
	const int numVertices  = polydata->GetPoints()->GetNumberOfPoints();	

	PxSceneWriteLock scopedLock(*mScene);
	physx::PxVec3 *Verts = new physx::PxVec3[numVertices];
	for (int i = 0; i < numVertices; i++)
	{
		double *thispoint = polydata->GetPoint(i);
		Verts[i] = physx::PxVec3(thispoint[0],thispoint[1],thispoint[2]);
	}

	polydata->GetPolys()->InitTraversal();
	PxU32 *Indices = new PxU32[polydata->GetNumberOfPolys()*3];
	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}


	PxTriangleMeshDesc meshDesc;
	meshDesc.points.count           = numVertices;
	meshDesc.points.stride          = sizeof(PxVec3);
	meshDesc.points.data            = Verts;

	meshDesc.triangles.count        = numTriangle;
	meshDesc.triangles.stride       = 3*sizeof(PxU32);
	meshDesc.triangles.data         = Indices;
	
	


	physx::PxDefaultMemoryOutputStream writeBuffer;
	
	bool status = mCooking->cookTriangleMesh(meshDesc, writeBuffer);
	
	if(!status)
		return NULL;

	physx::PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());
	physx::PxTriangleMesh* pxMesh = mPhysics->createTriangleMesh(readBuffer);
	PxTransform trans = PxTransform(PxIdentity);
	trans.p = initialPos;

	PxRigidDynamic* meshActor = mPhysics->createRigidDynamic(trans);
	PxShape* meshShape;
	if(meshActor)
	{
			meshActor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, true);

			PxTriangleMeshGeometry triGeom;
			triGeom.triangleMesh = pxMesh;
			meshShape = meshActor->createShape(triGeom, *mMaterial);
			mScene->addActor(*meshActor);
			
	}

	return meshActor;
	
}

PxActor* PhysXEngine::CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,const PxVec3& initialPos)
{
	const int numTriangle = polydata->GetPolys()->GetNumberOfCells();
	const int numVertices  = polydata->GetPoints()->GetNumberOfPoints();	

	PxSceneWriteLock scopedLock(*mScene);
	physx::PxVec3 *Verts = new physx::PxVec3[numVertices];
	for (int i = 0; i < numVertices; i++)
	{
		double *thispoint = polydata->GetPoint(i);
		Verts[i] = physx::PxVec3(thispoint[0],thispoint[1],thispoint[2]);
	}

	polydata->GetPolys()->InitTraversal();
	PxU32 *Indices = new PxU32[polydata->GetNumberOfPolys()*3];
	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}


	PxTriangleMeshDesc meshDesc;
	meshDesc.points.count           = numVertices;
	meshDesc.points.stride          = sizeof(PxVec3);
	meshDesc.points.data            = Verts;

	meshDesc.triangles.count        = numTriangle;
	meshDesc.triangles.stride       = 3*sizeof(PxU32);
	meshDesc.triangles.data         = Indices;
	
	


	physx::PxDefaultMemoryOutputStream writeBuffer;
	
	bool status = mCooking->cookTriangleMesh(meshDesc, writeBuffer);
	
	if(!status)
		return NULL;

	physx::PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());
	physx::PxTriangleMesh* pxMesh = mPhysics->createTriangleMesh(readBuffer);
	PxTransform trans = PxTransform(PxIdentity);
	trans.p = initialPos;
	PxRigidStatic *mesh = PxCreateStatic(*mPhysics,trans, physx::PxTriangleMeshGeometry(pxMesh ,PxMeshScale(),  PxMeshGeometryFlag::eDOUBLE_SIDED),*mMaterial);
	
	mScene->addActor(*mesh);
	
	
	
	return mesh;
}

PxActor* PhysXEngine::CreateCollisionRigidSphereObject(float radius,float mass,const PxVec3& initialPos)
{
	PxTransform trans = PxTransform(PxIdentity);
	trans.p = initialPos;
	PxRigidDynamic *mesh = PxCreateDynamic(*mPhysics, trans, PxSphereGeometry(radius),*mMaterial,1.0);
	PxRigidBodyExt::updateMassAndInertia(*mesh,10.0);
	mScene->addActor(*mesh);
	return mesh;
}

PxActor* PhysXEngine::CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,float mass,const  PxVec3 & initialPos)
{
	const int numTriangle = polydata->GetPolys()->GetNumberOfCells();
	const int numVertices  = polydata->GetPoints()->GetNumberOfPoints();	

	PxSceneWriteLock scopedLock(*mScene);
	physx::PxVec3 *Verts = new physx::PxVec3[numVertices];
	for (int i = 0; i < numVertices; i++)
	{
		double *thispoint = polydata->GetPoint(i);
		Verts[i] = physx::PxVec3(thispoint[0],thispoint[1],thispoint[2]);
	}

	polydata->GetPolys()->InitTraversal();
	PxU32 *Indices = new PxU32[polydata->GetNumberOfPolys()*3];
	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}


	//PxTriangleMeshDesc meshDesc;
	PxConvexMeshDesc  convexDesc;
	convexDesc.points.count           = numVertices;
	convexDesc.points.stride          = sizeof(PxVec3);
	convexDesc.points.data            = Verts;
	convexDesc.flags				   = PxConvexFlag::eCOMPUTE_CONVEX;
	convexDesc.triangles.count        = numTriangle;
	convexDesc.triangles.stride       = 3*sizeof(PxU32);
	convexDesc.triangles.data         = Indices;
	convexDesc.vertexLimit      = 256;

	physx::PxDefaultMemoryOutputStream writeBuffer;
	bool status = mCooking->cookConvexMesh(convexDesc, writeBuffer);
	if(!status)
		return NULL;

	physx::PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());
	
	//rigid dynamic is not support trimesh	
	/*
	physx::PxTriangleMesh* pxMesh = mPhysics->createTriangleMesh(readBuffer);
	PxTransform trans = PxTransform(PxIdentity);

	PxRigidDynamic *mesh2 = mPhysics->createRigidDynamic(trans);
	



	physx::PxTriangleMeshGeometry pxMeshGeo(pxMesh);
	physx::PxSphereGeometry pxSphereGeo(2.0);
	PxShape* aTriMeshShape = mesh2->createShape(pxMeshGeo, *mMaterial, PxShapeFlag::eVISUALIZATION);
	PxRigidDynamic *mesh = PxCreateDynamic(*mPhysics, trans, pxSphereGeo, *mMaterial, 1.0);
	PxRigidDynamic *mesh3 = PxCreateDynamic(*mPhysics, trans, pxMeshGeo, *mMaterial, 1.0);
	mesh->setMass(mass);
	

	mScene->addActor(*mesh);
	
	*/
	
	physx::PxConvexMesh* pxMesh = mPhysics->createConvexMesh(readBuffer);
	PxTransform trans = PxTransform(PxIdentity);
	trans.p = initialPos;
	PxRigidDynamic *mesh = PxCreateDynamic(*mPhysics,trans, physx::PxConvexMeshGeometry(pxMesh),*mMaterial,1.0);
	mesh->setMass(mass);
	mScene->addActor(*mesh);

	return mesh;
}




void PhysXEngine::Tick(float dt)
{
	PxSceneWriteLock scopedLock(*mScene);
	mScene->simulate(mStepSize);
	/*
	mAccumulator  += dt;
    if(mAccumulator < mStepSize)
        return;

    mAccumulator -= mStepSize;
	*/
//#pragma omp critical
    
	
} 
bool PhysXEngine::fetchResult()
{
	if (mScene) 
	{
		return mScene->checkResults(true);
		PxSceneWriteLock scopedLock(*mScene);
		
		return mScene->fetchResults();
	}
	else
		return false;
}

void PhysXEngine::ResetFilter(PxRigidActor *actor)
{
	PxSceneWriteLock scopedLock(*mScene);
	mScene->resetFiltering(*actor);

}

void PhysXEngine::ResetFilter(PxRigidActor *actor,PxShape *const * shapes, PxU32 shapeCount	)
{
	PxSceneWriteLock scopedLock(*mScene);
	mScene->resetFiltering(*actor,shapes,shapeCount);
}

PxShape* PhysXEngine::CloneShape(PxShape &src)
{
//	mPhysics->createShape(src.getTriangleMeshGeometry(),
	return NULL;
}
