
#include "stdafx.h"
#include <algorithm>
#include "ODEEngine.h"
#define MAX_CONTACTS 64




ODEEngine::~ODEEngine()
{


  dJointGroupDestroy (m_contactgroup);
  dSpaceDestroy (m_space);
  dWorldDestroy (m_world);
  //dCloseODE();
}
void ODEEngine::initPhysics()
{
	int ret_init = dInitODE2(0);
	m_world = dWorldCreate();
	m_totalStep = 0;
	m_space = dSimpleSpaceCreate(0);
	m_contactgroup = dJointGroupCreate(0);
	dWorldSetGravity (m_world,0.0,0.0,0.0);
	dWorldSetAutoDisableAverageSamplesCount(m_world,0);
	//dWorldSetCFM (m_world,1e-5);

	m_threading = dThreadingAllocateMultiThreadedImplementation();
	m_pool = dThreadingAllocateThreadPool(4, 0, dAllocateFlagBasicData, NULL);
	dThreadingThreadPoolServeMultiThreadedImplementation(m_pool, m_threading);
	dWorldSetStepThreadingImplementation(m_world, dThreadingImplementationGetFunctions(m_threading), m_threading);
}

dGeomID ODEEngine::CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,double initPos[3],int &op_numTriangle, dTriIndex *&op_Indices, int &op_numVertices, double *&op_vertices )
{
	dTriMeshDataID Data = dGeomTriMeshDataCreate();

	const int numTriangle = polydata->GetPolys()->GetNumberOfCells();
	const int numVertices  = polydata->GetPoints()->GetNumberOfPoints();	
	double *vertices = new double[numVertices*3];
	
	for (int i = 0; i < numVertices; i++)
	{
		double *thispoint = polydata->GetPoint(i);
		memcpy_s(&vertices[i*3 + 0], sizeof(double)*3, thispoint, sizeof(double)*3);		
	}

	polydata->GetPolys()->InitTraversal();
	dTriIndex * Indices = new dTriIndex[numTriangle*3];

	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}


	dGeomTriMeshDataBuildDouble(Data, vertices, 3 * sizeof(double), numVertices, &Indices[0], numTriangle*3, 3 * sizeof(dTriIndex));
	dGeomID geom = dCreateTriMesh(m_space, Data, 0, 0, 0);
	if (geom)
	{
		op_Indices = Indices;
		op_vertices = vertices;
		op_numTriangle = numTriangle;
		op_numVertices = numVertices;
	}
	
	return geom;
}

dGeomID ODEEngine::CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,double mass, double initPos[3],int &op_numTriangle, dTriIndex *&op_Indices, int &op_numVertices, double *&op_vertices)
{
	dTriMeshDataID Data = dGeomTriMeshDataCreate();

	const int numTriangle = polydata->GetPolys()->GetNumberOfCells();
	const int numVertices  = polydata->GetPoints()->GetNumberOfPoints();	
	double *vertices = new double[numVertices*3];
	
	for (int i = 0; i < numVertices; i++)
	{
		double *thispoint = polydata->GetPoint(i);
		memcpy_s(&vertices[i*3 + 0], sizeof(double)*3, thispoint, sizeof(double)*3);		
	}

	polydata->GetPolys()->InitTraversal();
	dTriIndex * Indices = new dTriIndex[numTriangle*3];

	for  (int ci = 0 ; ci < numTriangle; ci++)
	{
		vtkIdType npts;
		vtkIdType *pts(NULL);
		polydata->GetPolys()->GetNextCell(npts, pts);
		Indices[ci * 3 + 0]  = pts[0];
		Indices[ci * 3 + 1]  = pts[1];
		Indices[ci * 3 + 2]  = pts[2];
	}

	dGeomTriMeshDataBuildDouble(Data, vertices, 3 * sizeof(double), numVertices, &Indices[0], numTriangle*3, 3 * sizeof(dTriIndex));
	dGeomID geom = dCreateTriMesh(m_space, Data, 0, 0, 0);
	dBodyID body = dBodyCreate (m_world);
	dMass m;
	dMassSetZero(&m);
	dMassSetTrimeshTotal (&m, mass, geom);
	

	
    //dGeomSetPosition(geom, -initPos[0], -initPos[1], -initPos[2]);
    //dMassTranslate(&m, -initPos[0], -initPos[1], -initPos[2]);


	//dGeomSetPosition(geom, initPos[0],initPos[1],initPos[2]);
	dBodySetPosition(body,initPos[0],initPos[1],initPos[2]);
	dGeomSetBody (geom,body);
	dBodySetMass(body,&m);
	
	
	
	return geom;
}

void ODEEngine::nearCallback (void *data, dGeomID o1, dGeomID o2)
{
	ODEEngine *_this = reinterpret_cast<ODEEngine*>(data);

	 int i;
  // if (o1->body && o2->body) return;

  // exit without doing anything if the two bodies are connected by a joint
  dBodyID b1 = dGeomGetBody(o1);
  dBodyID b2 = dGeomGetBody(o2);  
  if (b1 && b2 && dAreConnectedExcluding (b1,b2,dJointTypeContact)) return;




  dContact contact[MAX_CONTACTS];   // up to MAX_CONTACTS contacts per box-box
  for (i=0; i<MAX_CONTACTS; i++) {
    contact[i].surface.mode = dContactBounce;
    contact[i].surface.mu = dInfinity;
	
    //contact[i].surface.mu2 = 0;
    contact[i].surface.bounce = 0.0; //0.1
    contact[i].surface.bounce_vel = 0.0; //0.1
    contact[i].surface.soft_cfm = 0.0;
  }
  if (int numc = dCollide (o1,o2,MAX_CONTACTS,&contact[0].geom,
			   sizeof(dContact))) {
    dMatrix3 RI;
    dRSetIdentity (RI);
    const dReal ss[3] = {0.02,0.02,0.02};
    for (i=0; i<numc; i++) {
		
		
      dJointID c = dJointCreateContact (_this->m_world,_this->m_contactgroup,contact+i);
      dJointAttach (c,b1,b2);
      
    }
  }


}
void ODEEngine::Tick(double _time)
{
	dSpaceCollide (m_space,this,nearCallback);
	
	dReal step_time = 1.0/60.0;
	dWorldStep (m_world,step_time);
	
	m_totalStep += step_time;
	dJointGroupEmpty (m_contactgroup);
	
		// pos = dGeomGetPosition (geom);
   // R = dGeomGetRotation (geom);
}


int ODEEngine::GetCollisionObjectTransform(dGeomID object, double op_pos[3],double op_rotQuaternion[4])
{
	const dReal* position = dGeomGetPosition(object);
	dQuaternion quat;
	dGeomGetQuaternion(object, quat);
	
	op_pos[0] = position[0];op_pos[1] = position[1];op_pos[2] = position[2];
	op_rotQuaternion[0] = quat[0];op_rotQuaternion[1] = quat[1];op_rotQuaternion[2] = quat[2];op_rotQuaternion[3] = quat[3];
	return 0;
}



