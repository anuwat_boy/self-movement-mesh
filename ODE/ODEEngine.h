/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef ODE_ENGINE_H
#define ODE_ENGINE_H


//#include "GlutStuff.h"
//#include "GL_ShapeDrawer.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define dDOUBLE
#include "ODE_Header.h"










class ODEEngine				
{

private:
	
protected:
	dWorldID					m_world;
	dSpaceID					m_space;
	dJointGroupID				m_contactgroup;
	dThreadingImplementationID	m_threading;
	dThreadingThreadPoolID		m_pool;

	double						m_totalStep;
public:
		
	ODEEngine(){};
	
	virtual ~ODEEngine();

	

	virtual	void initPhysics();
	virtual dGeomID CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,double initPos[3],int &op_numTriangle, dTriIndex *&op_Indices, int &op_numVertices, double *&op_vertices );
	virtual dGeomID CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,double mass, double initPos[3],int &op_numTriangle, dTriIndex *&op_Indices, int &op_numVertices, double *&op_vertices);
	
	virtual void Tick(double _time);
	static void nearCallback (void *data, dGeomID o1, dGeomID o2);

	int GetCollisionObjectTransform(dGeomID object, double op_pos[3],double op_rotQuaternion[4]);
	double GetTotalStep() {return m_totalStep;};
	


	/*
	virtual void exitPhysics();
	virtual void Tick(btScalar _time = 0);
	virtual int GetCollisionObjectTransformMatrix(int objIndex, double * m);
	virtual void SetCollisionObjectTransformMatrix(int objIndex, double * m);
	virtual int CreateFixedCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&vertices,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL );
	virtual int CreateCollisionRigidObject(vtkSmartPointer<vtkPolyData> polydata,int &numTriangle, int *&Indices, int &numVertices, btScalar *&vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual int CreateFixedCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual int CreateCollisionRigidObject(int numTriangle, int *Indices, int numVertices, btScalar * vertices,float mass,btVector3 &transformOrigin,btQuaternion *transformRotation = NULL);
	virtual void RemoveRigidObject(int bt_id);
	virtual void ResetScene();
	virtual btCollisionObject* GetCollisionObject( int bullet_id);

	
	void	setDebugMode(int mode);	

	
	void toggleIdle();	
	

	virtual void	clientResetScene();
	
	btRigidBody*	localCreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape);
	*/

private:

};

#endif //ODE_ENGINE_H


